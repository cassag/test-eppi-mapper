# HTML test EPPI-Mapper

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>EPPI-Mapper</title>
  <style>
    *, *:before, *:after {
  box-sizing: border-box;
}
html {
  box-sizing: border-box;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  text-rendering: optimizeLegibility;
  -moz-osx-font-smoothing: grayscale;
  line-height: 1.5;
}
body {
  background-color: #ffffff;
  margin: 0;
  overflow: hidden;
}
a {
  color: #0275d8;
}
a:active, a:hover {
  outline-width: 0;
}
.clearfix:after {
  visibility: hidden;
  display: block;
  font-size: 0;
  content: " ";
  clear: both;
  height: 0;
}
* html .clearfix {
  zoom: 1;
}
*:first-child + html .clearfix {
  zoom: 1;
}
.loader {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: #0275d8;
  z-index: 1000;
  transition: all 2s ease;
}
.loader span.spinner {
  animation: spin 1.2s linear infinite;
  border: 5px solid #ffffff;
  border-bottom-color: #0275d8;
  border-top-color: #0275d8;
  border-radius: 100%;
  display: inline-block;
  width: 40px;
  height: 40px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
.header {
  padding: 0 18px;
}
.wrapper {
  margin: 8px;
}
.pivot-table {
  font-size: 11px;
  font-weight: normal;
  width: 100%;
}
.pivot-table .top-head, .pivot-table .side-head, .pivot-table .body {
  display: inline-block;
  overflow: hidden;
}
.pivot-table .side-head, .pivot-table .body {
  float: left;
}
.pivot-table table {
  border: 0;
  border-collapse: collapse;
}
.pivot-table table thead th {
  border: 1px solid #ffffff;
  color: #ffffff;
  font-weight: normal;
  max-width: 103px;
  min-width: 103px;
  width: 103px;
  padding: 4px;
  overflow: hidden;
  text-overflow: ellipsis;
  vertical-align: top;
  text-align: left;
}
.pivot-table table tbody th, .pivot-table table tbody td {
  border: 1px solid #fff;
}
.pivot-table table tbody th {
  color: #ffffff;
  font-weight: normal;
  min-height: 103px;
  height: 103px;
  max-width: 103px;
  min-width: 0;
  width: 0;
  padding: 4px;
  overflow: hidden;
  text-overflow: ellipsis;
  vertical-align: top;
  text-align: left;
}
.pivot-table table tbody td {
  min-width: 103px;
  width: 103px;
  min-height: 103px;
  height: 103px;
}
.body table tbody td.cell div.data-wrapper {
  position: relative;
  background-color: #eeeeee;
  cursor: pointer;
  width: 100%;
  height: 100%;
  overflow: hidden;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
  -moz-transition: all 0.5s;
  -ms-transition: all 0.5s;
  -o-transition: all 0.5s;
  transition: all 0.5s;
}
.body table tbody td.cell div.pie-wrapper {
  position: relative;
  background-color: #eeeeee;
  cursor: pointer;
  display: none;
  width: 100%;
  height: 100%;
  overflow: hidden;
}
.body table tbody td.cell div.mosaic-wrapper {
  position: relative;
  background-color: #eeeeee;
  cursor: pointer;
  display: none;
  width: 100px;
  height: 100px;
  max-width: 100px;
  max-height: 100px;
  overflow: hidden;
}
.body table tbody td.cell div.pie-wrapper div.pie,
.body table tbody td.cell div.pie-wrapper div.pie-hole{
  position: absolute;
  top: 50%;
  left: 50%;
  background-size: cover;
  border-radius: 100%;
  transform: translate(-50%, -50%);
}
.body table tbody td.cell div.pie-wrapper div.pie-hole{
  background-color: #dddddd;
}
.body table tbody td.cell.none div.data-wrapper,
.body table tbody td.cell.none div.pie-wrapper,
.body table tbody td.cell.none div.mosaic-wrapper {
  cursor: not-allowed !important;
}
.controls {
  display: inline-block;
  position: absolute;
}
.ui-segment {
  background-color: #ffffff;
  color: #0275d8;
  border: 1px solid #0275d8;
  border-radius: 4px;
  display: inline-block;
}
.ui-segment span.option.active {
  background-color: #0275d8;
  color: #ffffff;
}
.ui-segment span.option {
  font-size: 13px;
  padding-left: 23px;
  padding-right: 23px;
  height: 25px;
  text-align: center;
  display: inline-block;
  line-height: 25px;
  margin: 0;
  float: left;
  cursor: pointer;
  border-right: 1px solid #0275d8;
  transition: all 0.5s ease;
}
.ui-segment span.option:last-child {
  border-right: none;
}
.segment-select{
  display: none;
}
.footer {
  position: absolute;
  bottom: 0;
  background-color: #ffffff;
  color: rgba(0, 0, 0, 0.87);
  height: 36px;
  min-height: 36px;
  padding: 16px;
  width: 100%;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 0 !important;
  -ms-flex: 0 1 auto !important;
  flex: 0 1 auto !important;
}
.footer .inner svg {
  display: inline-block;
  vertical-align: middle;
}
.footer .legend .dot {
  border-radius: 100%;
  display: inline-block;
  height: 10px;
  width: 10px;
  margin: 2px 3px 0 6px;
}
.footer .legend .label {
  font-size: 1.0em;
}
.legend-tooltip {
  border: 1px solid #cccccc;
  display: block;
  position: absolute;
  width: 300px;
  background: #ffffff;
  color: #000000;
  padding: 6px 8px;
  border-radius: 4px;
  -webkit-box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  z-index: 2;
}
.spacer {
  -webkit-box-flex: 1 !important;
  -ms-flex-positive: 1 !important;
  flex-grow: 1 !important;
}
.hide {
  display: none;
}
.tooltip {
  display: none;
  position: absolute;
  border: 1px solid #cccccc;
  background-color: #ffffff;
  border-radius: 3px;
  padding: 3px 6px 2px;
  -webkit-box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  z-index: 2;
}
.tooltip .count {
  color: #0275d8;
  margin: 2px 3px;
}
.tooltip .count span {
  background-color: #0275d8;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  color: #ffffff;
  padding: 2px 4px;
}
.refs {
  font-size:  12px;
  color:  lightgrey;
  margin:  0;
  padding: 0 0 0 20px;
}
.veil {
  background: rgba(0, 0, 0, 0.7);
  display: none;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 99;
}
.veil.open {
  display: block;
}
.settings {
  position: absolute;
  top: 0;
  left: -600px;
  bottom: 0;
  background: #fff;
  color: #888;
  padding: 0;
  transition: all 0.5s ease;
  width: 600px;
  z-index: 100;
}
.settings.open {
  left: 0;
}
.settings > div.title {
  background-color: #0275d8;
  color: #ffffff;
  font-size: 22px;
  padding: 20px;
  margin: 0 0 10px 0;
}
.settings > div.title > a.btnSettings {
  -webkit-border-radius: 36px;
  -moz-border-radius: 36px;
  border-radius: 36px;
  background-color: #0275d8;
  color: #ffffff;
  display: inline-block;
  float: right;
  height: 36px;
  transition: all 0.25s ease;
  text-align: center;
  line-height: 36px;
  padding: 0 12px;
}
.settings > div.title > a.btnSettings:hover {
  background-color: #005cab;
  cursor: pointer;
}
.settings > div.title > a.btnSettings.busy{
  border: 3px solid #005cab !important;
  border-top-color: #ffffff !important;
  border-bottom-color: #ffffff !important;
  text-indent: -99999px;
  width: 36px;
  border-radius: 36px;
  animation: spin 1.2s linear infinite;
}
.settings > div.title > a.disabled {
  background-color: #005cab;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}
.settings > div.title > a.right {
  border-bottom-left-radius: 0;
  border-top-left-radius: 0;
  margin-left: 2px;
}
.settings > div.title > a.left {
  border-bottom-right-radius: 0;
  border-top-right-radius: 0;
}
.settings div.filter-type-wrapper {
  background-color: #efefef;
  position: absolute;
  top: 76px;
  overflow: auto;
  padding: 0 10px 0 0;
  width: 60%;
}
.settings div.filter-wrapper {
  background-color: #efefef;
  position: absolute;
  top: 226px;
  bottom: 0;
  overflow: auto;
  padding: 0 10px 0 0;
  width: 60%;
}
.settings div.filter-type-wrapper h2,
.settings div.filter-wrapper h2 {
  margin-left: 20px;
  margin-bottom: 10px;
}
.settings div.filter-type-wrapper ul,
.settings div.filter-wrapper ul {
  list-style: none;
  margin: 0 0 0 16px;
  padding: 0;
}
.settings div.filter-type-wrapper ul li,
.settings div.filter-wrapper ul li {
  margin: 0;
  padding: 3px 5px;
}
.settings div.filter-type-wrapper ul li:hover,
.settings div.filter-wrapper ul li:hover {
  cursor: pointer;
}
.settings div.filter-type-wrapper ul li span,
.settings div.filter-type-wrapper ul li svg,
.settings div.filter-wrapper ul li span,
.settings div.filter-wrapper ul li svg {
  display: inline-block;
  vertical-align: middle;
}
.settings div.filter-type-wrapper ul li svg,
.settings div.filter-wrapper ul li svg {
  display: none;
}
.settings div.filter-type-wrapper ul li.checked svg#checked,
.settings div.filter-wrapper ul li.checked svg#checked {
  display: inline-block;
}
.settings div.filter-type-wrapper ul li.unchecked svg#unchecked,
.settings div.filter-wrapper ul li.unchecked svg#unchecked {
  display: inline-block;
}
.settings div.filter-type-wrapper ul li.indeterminate svg#indeterminate,
.settings div.filter-wrapper ul li.indeterminate svg#indeterminate {
  display: inline-block;
}
.settings div.style-wrapper {
  position: absolute;
  top: 76px;
  right: 0;
  bottom: 0;
  overflow: auto;
  padding: 0 20px 0 10px;
  width: 40%;
}
.settings div.style-wrapper h2 {
  margin-bottom: 10px;
}
.settings div.style-wrapper ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
.settings div.style-wrapper ul li {
  margin: 0;
  padding: 3px 5px;
}
.settings div.style-wrapper ul li:hover {
  cursor: pointer;
}
.settings div.style-wrapper ul li span,
.settings div.style-wrapper ul li svg {
  display: inline-block;
  vertical-align: middle;
}
.settings div.style-wrapper ul li svg {
  display: none;
}
.settings div.style-wrapper ul li.checked svg#checked {
  display: inline-block;
}
.settings div.style-wrapper ul li.unchecked svg#unchecked {
  display: inline-block;
}
.reader {
  position: absolute;
  top: 0;
  right: -1000px;
  bottom: 0;
  background: #fff;
  color: #000;
  padding: 0;
  transition: all 0.5s ease;
  width: 1000px;
  z-index: 100;
}
.reader.open {
  right: 0;
}
.reader > div.title {
  background-color: #0275d8;
  color: #ffffff;
  font-size: 22px;
  padding: 20px;
}
.reader > div.title > a.close {
  -webkit-border-radius: 100%;
  -moz-border-radius: 100%;
  border-radius: 100%;
  color: #ffffff;
  display: inline-block;
  width: 36px;
  height: 36px;
  transition: all 0.25s ease;
  text-align: center;
  line-height: 36px;
  margin-right: 20px;
}
.reader > div.title > a.close:hover {
  background-color: #0275d8;
  cursor: pointer;
}
.reader > div.title > input {
  border: 1px solid #96c9fb;
  background: #0275d8;
  padding: 6px;
  color: #96c9fb;
  float: right;
  font-size: 16px;
  transition: all 0.5s ease;
}
.reader > div.title > input:focus {
  background: #96c9fb;
  color: #1f5286;
}
.reader > div.title > input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #96c9fb;
}
.reader > div.title > input::-moz-placeholder { /* Firefox 19+ */
  color: #96c9fb;
}
.reader > div.title > input:-ms-input-placeholder { /* IE 10+ */
  color: #96c9fb;
}
.reader > div.title > input:-moz-placeholder { /* Firefox 18- */
  color: #96c9fb;
}
.reader > div.title > select {
  border: 1px solid #96c9fb;
  background: #0275d8;
  padding: 6px;
  color: #96c9fb;
  float: right;
  font-size: 15px;
  transition: all 0.5s ease;
}
.reader > div.title > button {
  float: right;
  color: #96c9fb;
  border: 1px solid #96c9fb;
  margin-left: 5px;
}

.reader > div.content {
  background: #ffffff;
  display: flex;
  align-content: stretch;
  align-items: stretch;
  height: 92.4%;
}
.reader > div.content > div.reader-filter {
  background-color: #efefef;
  border-right: 1px solid #ffffff;
  overflow: auto;
  width: 200px;
  min-width: 200px;
  max-width: 200px;
}
.reader > div.content > div.filter-opts {
  background-color: #efefef;
  border-right: 1px solid #ffffff;
  overflow: auto;
  width: 200px;
  min-width: 200px;
  max-width: 200px;
  float: right;
}
.settings > div.filter-wrapper > div.controlTainer {
  align-content: center;
  padding: 10px;
}
.reader > div.content > div.reader-filter ul {
  list-style: none;
  margin: 0;
  padding: 0;
  width: 100%;
  overflow: hidden;
}
.reader > div.content > div.reader-filter > ul > ul {
  padding: 0 0 0 12px;
 }
.reader > div.content > div.reader-filter ul li:hover {
  cursor: pointer;
}
.reader > div.content > div.reader-filter ul li {
  padding: 3px 6px 2px;
  display: inline-flex;
  align-items: end;
  width: 100%;
}
.reader > div.content > div.reader-filter ul li span,
.reader > div.content > div.reader-filter ul li svg {
  display: inline-block;
  vertical-align: middle;
}
.reader > div.content > div.reader-filter ul li svg {
  display: none;
  width: 24px;
  min-width: 24px;
}
.reader > div.content > div.reader-filter ul li span {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.reader > div.content > div.reader-filter ul li.checked svg#checked {
  display: inline-block;
}
.reader > div.content > div.reader-filter ul li.unchecked svg#unchecked{
  display: inline-block;
}
.reader > div.content > div.reader-filter ul li.indeterminate svg#indeterminate {
  display: inline-block;
}
.reader > div.content > div.nav {
  background-color: #efefef;
  border-right: 1px solid #ffffff;
  overflow: auto;
  width: 280px;
  min-width: 280px;
  max-width: 280px;
}
.reader > div.content > div.navTainer {
  background-color: #efefef;
  border-right: 1px solid #ffffff;
  overflow: auto;
  width: 280px;
  min-width: 280px;
  max-width: 280px;
}
.reader > div.content > div.navTainer .navGroupSelect {
  color: #0275d8;
  border-top: 1px solid #0275d8;
  padding: 6px 10px 4px;
}
.reader > div.content > div.navTainer .ref-sort-order {
  color: #0275d8;
  border-top: 1px solid #0275d8;
  border-bottom: 1px solid #0275d8;
  padding: 6px 10px 4px;
}
.reader > div.content > div.navTainer > div > ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
.reader > div.content > div.nav > ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
.reader > div.content > div.nav > ul > li {
  border-bottom: 1px solid #ffffff;
  padding: 6px 10px 8px;
  transition: all 0.5s ease;
}
.reader > div.content div.nav > ul li,
.reader > div.content > div.navTainer > div > ul > li > ul > li {
  padding: 6px 10px 8px;
}
.reader > div.content div.nav > ul.segmented > li {
  padding: 0;
}
.reader > div.content div.nav > ul.segmented > li > ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
.reader > div.content div.nav > ul.segmented > li > ul > li {
  padding: 6px 10px 8px;
}
.reader > div.content div.nav > ul.segmented > li > div.segment-title {
  color: #000000;
  font-weight: bold;
  padding: 6px 10px 8px;
  width: 100%;
}
.reader > div.content > div.navTainer > div > ul > li > ul > li.selected:hover {
  background-color: silver;
  cursor: pointer;
}
.reader > div.content > div.navTainer > div > ul > li[segmented=no]:hover {
  background-color: #ffffff;
  cursor: pointer;
}
.reader > div.content > div.navTainer > div > ul > li[segmented=no].selected:hover {
  background-color: silver;
  cursor: pointer;
}
.reader > div.content div.nav > ul li.selected {
  background-color: #aaaaaa;
  color: #ffffff;
}
.reader > div.content div.nav > ul li > div {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.reader > div.content div.nav > ul li > div.title {
  font-weight: bold;
}
.reader > div.content div.nav > ul li > div.auth,
.reader > div.content div.nav > ul li > div.date {
  color: #666666;
}
.reader > div.content div.nav > ul li.selected > div.auth,
.reader > div.content div.nav > ul li.selected > div.date {
  color: #ffffff;
}
.reader > div.content div.nav > ul li > div.auth {
  font-style: italic;
}
.reader > div.content > div.read {
  flex-grow: 1;
  overflow: auto;
  padding: 10px 18px;
}
.reader > div.content > div.read > h2 {
  margin-top: 0;
}
.reader > div.content > div.read > hr {
  background-color: #efefef;
  border: 0;
  height: 1px;
}
.reader > div.content > div.read > p {
  font-size: 1.3rem;
}
.reader > div.content > div.read > div.meta-data {
  margin-top: 16px;
}
.reader > div.content > div.read > div.meta-data > div.meta-data-item > label {
  color: #aaaaaa;
  display: block;
  float: left;
  width: 140px;
  margin: 0 0 3px 0;
  padding: 3px 6px;
}
.reader > div.content > div.read > div.meta-data > div.meta-data-item > span {
  border: 1px solid #eeeeee;
  display: block;
  float: left;
  margin: 0 0 3px -140px;
  padding: 2px 4px 2px 144px;
  width: 100%;
}
.ref-sort-order {
  padding: 4px 8px;
}
.menu {
  background-color: #ffffff;
  border-bottom: 1px solid #eeeeee;
  display: block;
  list-style: none;
  margin: 0;
  padding: 0;
}
.menu:after {
  content: "";
  display: table;
  clear: both;
}
.menu .menu-item {
  background-color: #ffffff;
  color: #555555;
  display: flex;
  padding: 8px 12px;
  transition: all 0.25s ease-in-out;
  cursor: pointer;
  float: left;
}
.menu .menu-item:hover {
  background-color: #f1f1f1;
  color: #000000;
}
.menu .menu-item:hover svg {
  fill: #000000;
}
.menu .menu-item span {
  margin-left: 4px;
  overflow: hidden;
  word-break: keep-all;
  transition: all 500ms ease-in-out;
}
.menu .menu-item svg {
  fill: #555555;
  display: inline-block;
  margin: 0 0 0 4px;
}
.menu .menu-item span.active-text,
.menu .menu-item svg.active-svg {
  display: none;
}
.menu .menu-item.active span.inactive-text,
.menu .menu-item.active svg.inactive-svg {
  display: none;
}
.menu .menu-item.active span.active-text,
.menu .menu-item.active svg.active-svg {
  display: block;
}
.refMenuItemLegend {
  -webkit-border-radius: 100%;
  -moz-border-radius: 100%;
  border: 1px solid #ffffff;
  border-radius: 100%;
  display: inline-block;
  width: 10px;
  height: 10px;
  margin: 6px 3px 0;
  float: right;
}
.clickable-row,
.clickable-col {
  cursor: pointer;
}
.top-head th div,
.side-head th div {
  position: relative;
  width: 100%;
  height: 100%;
}
.top-head th div svg,
.side-head th div svg {
  display: none;
  background: rgba(255, 255, 255, 0.8);
}
.top-head th div svg,
.side-head th div svg {
  position: absolute;
  top: -4px;
  right: -4px;
}
.top-head th:not(.collapsed) div svg#arrowLeft,
.side-head th:not(.collapsed) div svg#arrowUp {
  cursor: pointer;
  display: inline-block;
}
.top-head th.collapsed div svg#arrowRight,
.side-head th.collapsed div svg#arrowDown {
  cursor: pointer;
  display: inline-block;
}
.top-head th.busy div svg#refresh,
.side-head th.busy div svg#refresh {
  border-radius: 100%;
  cursor: pointer;
  display: inline-block;
  animation: spin 1s linear infinite;
}
.btnColCollapse,
.btnRowCollapse {
  opacity: 40%;
}
.top-head th.collapsed {
  min-width: 0;
  max-width: 0;
  width: 0;
  padding: 4px 0;
}
.top-head th.collapsed.level-1 {
  min-width: 103px;
  width: 103px;
  padding: 4px;
}
.side-head th.collapsed {
  min-height: 0;
  max-height: 0;
  height: 0;
  padding: 0 4px;
}
.side-head th.collapsed.level-1 {
  min-height: 103px;
  height: 103px;
  padding: 4px;
}
.body td.collapsed-col {
  min-width: 18px;
  width: 18px;
}
.body td.collapsed-row {
  min-height: 18px;
  height: 18px !important;
}
.body td.collapsed-col,
.body td.collapsed-row {
  background: rgba(0, 0, 0, 0.2);
}
.body td.collapsed-col .mosaic-wrapper,
.body td.collapsed-row .mosaic-wrapper {
  width: auto !important;
  height: auto !important;
}
.body td.collapsed-col svg,
.body td.collapsed-row svg {
  display: none;
}
.body td.collapsed-col .pie-wrapper,
.body td.collapsed-col .mosaic-wrapper,
.body td.collapsed-col .data-wrapper,
.body td.collapsed-row .pie-wrapper,
.body td.collapsed-row .mosaic-wrapper,
.body td.collapsed-row .data-wrapper {
  opacity: 0 !important;
}
.top-head th.collapsed:not(.level-1) span,
.side-head th.collapsed:not(.level-1) span {
  display: none;
}
.top-head th.collapsed:not(.level-1),
.side-head th.collapsed:not(.level-1) {
  border-width: 0;
}
.top-head th.collapsed.first:not(.level-1) {
  border-left: 1px solid #ffffff !important;
}
.top-head th.collapsed.last:not(.level-1) {
  border-right: 1px solid #ffffff !important;
}
.side-head th.collapsed.first:not(.level-1) {
  border-top: 1px solid #ffffff !important;
}
.side-head th.collapsed.last:not(.level-1) {
  border-bottom: 1px solid #ffffff !important;
}
@keyframes spin {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
.attribute-tooltip {
  position: absolute;
  border: 1px solid #cccccc;
  background-color: #ffffff;
  border-radius: 3px;
  display: none;
  padding: 3px 6px 2px;
  text-overflow: ellipsis;
  max-width: 350px !important;
  min-width: 200px !important;
  -webkit-box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  box-shadow: 0 2px 10px 2px rgba(0, 0, 0, 0.3);
  z-index: 2;
}
.attribute-tooltip.show {
  display: block;
}
.attribute-tooltip .close-tooltip {
  cursor: pointer;
  float: right;
}
.attribute-tooltip .content {
  margin: 0 0 12px 0;
}
.attribute-tooltip h4 {
  margin: 0 0 8px 0;
}
.text-center {
  text-align: center;
}
.text-muted {
  color: #6c757d !important;
  font-size: 11px;
  margin: 10px 0 0 0;
}
#filterClearButton,
#codeFilterClearButton {
  float: right;
}
#codeFilterClearButton {
  width: 100%;
}
.btn {
  background-color: transparent;
  border: 1px solid #0275d8;
  color: #0275d8;
  display: block;
  font-weight: 400;
  padding: .375rem .75rem;
  font-size: 1rem;
  line-height: 1.5;
  transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.btn:hover {
  color: #fff;
  background-color: #0275d8;
  border-color: #0275d8;
  cursor: pointer;
}

@keyframes spin {
  from {transform:rotate(0deg);}
  to {transform:rotate(360deg);}
}

  </style>
  <script type="text/javascript">
    var isIE = false || !!document.documentMode;

    if (isIE === true) {
      alert("Unfortunately this map cannot be used in Internet Explorer. You will need to use Microsoft Edge, Firefox or Google Chrome.");
    }
  </script>
</head>
<body>
  <div class="attribute-tooltip">
    <span class="close-tooltip">X</span>
    <div class="content"></div>
  </div>
  <div class="loader">
    <span class="spinner"></span>
  </div>
  <div class="veil"></div>
  <div class="settings"></div>
  <div class="reader"></div>
  <ul class="menu">
    <li class="menu-item menu-settings">
      <svg xmlns="http://www.w3.org/2000/svg" fill="#eeeeee" width="18" height="18" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="none"/>
        <path d="M19.43 12.98c.04-.32.07-.64.07-.98s-.03-.66-.07-.98l2.11-1.65c.19-.15.24-.42.12-.64l-2-3.46c-.12-.22-.39-.3-.61-.22l-2.49 1c-.52-.4-1.08-.73-1.69-.98l-.38-2.65C14.46 2.18 14.25 2 14 2h-4c-.25 0-.46.18-.49.42l-.38 2.65c-.61.25-1.17.59-1.69.98l-2.49-1c-.23-.09-.49 0-.61.22l-2 3.46c-.13.22-.07.49.12.64l2.11 1.65c-.04.32-.07.65-.07.98s.03.66.07.98l-2.11 1.65c-.19.15-.24.42-.12.64l2 3.46c.12.22.39.3.61.22l2.49-1c.52.4 1.08.73 1.69.98l.38 2.65c.03.24.24.42.49.42h4c.25 0 .46-.18.49-.42l.38-2.65c.61-.25 1.17-.59 1.69-.98l2.49 1c.23.09.49 0 .61-.22l2-3.46c.12-.22.07-.49-.12-.64l-2.11-1.65zM12 15.5c-1.93 0-3.5-1.57-3.5-3.5s1.57-3.5 3.5-3.5 3.5 1.57 3.5 3.5-1.57 3.5-3.5 3.5z"/>
      </svg>
      <span>Filters</span>
    </li>
    <li class="menu-item menu-expand">
      <svg class="inactive-svg" xmlns="http://www.w3.org/2000/svg" fill="#eeeeee" width="18" height="18" viewBox="0 0 24 24">
        <path d="M15 21h2v-2h-2v2zm4 0h2v-2h-2v2zM7 21h2v-2H7v2zm4 0h2v-2h-2v2zm8-4h2v-2h-2v2zm0-4h2v-2h-2v2zM3 3v18h2V5h16V3H3zm16 6h2V7h-2v2z"/>
        <path d="M0 0h24v24H0z" fill="none"/>
      </svg>
      <svg class="active-svg" xmlns="http://www.w3.org/2000/svg" fill="#eeeeee" width="18" height="18" viewBox="0 0 24 24">
        <path d="M13 7h-2v2h2V7zm0 4h-2v2h2v-2zm4 0h-2v2h2v-2zM3 3v18h18V3H3zm16 16H5V5h14v14zm-6-4h-2v2h2v-2zm-4-4H7v2h2v-2z"/>
        <path d="M0 0h24v24H0z" fill="none"/>
      </svg>
      <span class="inactive-text">Hide Headers</span>
      <span class="active-text">Show Headers</span>
    </li>
    <li class="menu-item menu-fullscreen">
      <svg class="inactive-svg" xmlns="http://www.w3.org/2000/svg" fill="#eeeeee" width="18" height="18" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="none"/><path d="M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z"/>
      </svg>
      <svg class="active-svg" xmlns="http://www.w3.org/2000/svg" fill="#eeeeee" width="18" height="18" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="none"/><path d="M5 16h3v3h2v-5H5v2zm3-8H5v2h5V5H8v3zm6 11h2v-3h3v-2h-5v5zm2-11V5h-2v5h5V8h-3z"/>
      </svg>
      <span class="inactive-text">Fullscreen</span>
      <span class="active-text">Exit Fullscreen</span>
    </li>
    <li class="menu-item menu-about">
      <svg xmlns="http://www.w3.org/2000/svg" fill="#eeeeee" width="18" height="18" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="none"/>
        <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z"/>
      </svg>
      <span>About</span>
    </li>
    <li class="menu-item menu-studysubmit">
      <svg xlmns="http://www.w3.org/2000/svg" fill="#eeeeee" width="18" height="18" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="none"/>
        <path d="M8.016 15l3.984-3.984 3.984 3.984-1.406 1.453-1.594-1.594v4.125h-1.969v-4.125l-1.594 1.547zM18 20.016v-11.016h-5.016v-5.016h-6.984v16.031h12zM14.016 2.016l6 6v12q0 0.797-0.609 1.383t-1.406 0.586h-12q-0.797 0-1.406-0.586t-0.609-1.383l0.047-16.031q0-0.797 0.586-1.383t1.383-0.586h8.016z"></path>
      </svg>
      <span>Submit a Study</span>
    </li>
    <li class="menu-item menu-reader">
      <svg xmlns="http://www.w3.org/2000/svg" fill="#eeeeee" width="18" height="18" viewBox="0 0 24 24">
        <path fill="none" d="M-74 29h48v48h-48V29zM0 0h24v24H0V0zm0 0h24v24H0V0z"/>
        <path d="M13 12h7v1.5h-7zm0-2.5h7V11h-7zm0 5h7V16h-7zM21 4H3c-1.1 0-2 .9-2 2v13c0 1.1.9 2 2 2h18c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 15h-9V6h9v13z"/>
      </svg>
      <span>View Records</span>
    </li>
  </ul>
  <div class="header clearfix">
    <p style="text-align: left;"><em></em><strong><em>Location and study design of primary empirical research on immunization programs during times of crisis</em></strong></p>
  </div>
  <div class="wrapper">
    <div class="pivot-table clearfix">
      <div class="top-head-wrapper">
        <div class="top-head">
          <table>
            <thead>
            </thead>
          </table>
        </div>
      </div>
      <div class="side-head">
        <table>
          <tbody>
          </tbody>
        </table>
      </div>
      <div class="body">
        <table>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="footer">
    <div class="legend"></div>
    <div class="spacer"></div>
    <div class="inner">
      Generated using v.2.1.0 of the EPPI-Mapper
      powered by <a href="https://eppi.ioe.ac.uk/cms/Default.aspx?tabid=2914" target="_blank">EPPI Reviewer</a>
      and created with
      <svg fill="#c62828" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0h24v24H0z" fill="none"/>
        <path d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z"/>
      </svg>
      by the
      <a href="http://www.digitalsolutionfoundry.co.za/" target="_blank">Digital Solution Foundry</a> team.
    </div>
  </div>
  <script type="text/javascript">/*! jQuery v3.3.1 | (c) JS Foundation and other contributors | jquery.org/license */
!function(e,t){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=e.document?t(e,!0):function(e){if(!e.document)throw new Error("jQuery requires a window with a document");return t(e)}:t(e)}("undefined"!=typeof window?window:this,function(e,t){"use strict";var n=[],r=e.document,i=Object.getPrototypeOf,o=n.slice,a=n.concat,s=n.push,u=n.indexOf,l={},c=l.toString,f=l.hasOwnProperty,p=f.toString,d=p.call(Object),h={},g=function e(t){return"function"==typeof t&&"number"!=typeof t.nodeType},y=function e(t){return null!=t&&t===t.window},v={type:!0,src:!0,noModule:!0};function m(e,t,n){var i,o=(t=t||r).createElement("script");if(o.text=e,n)for(i in v)n[i]&&(o[i]=n[i]);t.head.appendChild(o).parentNode.removeChild(o)}function x(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?l[c.call(e)]||"object":typeof e}var b="3.3.1",w=function(e,t){return new w.fn.init(e,t)},T=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;w.fn=w.prototype={jquery:"3.3.1",constructor:w,length:0,toArray:function(){return o.call(this)},get:function(e){return null==e?o.call(this):e<0?this[e+this.length]:this[e]},pushStack:function(e){var t=w.merge(this.constructor(),e);return t.prevObject=this,t},each:function(e){return w.each(this,e)},map:function(e){return this.pushStack(w.map(this,function(t,n){return e.call(t,n,t)}))},slice:function(){return this.pushStack(o.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(e){var t=this.length,n=+e+(e<0?t:0);return this.pushStack(n>=0&&n<t?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:s,sort:n.sort,splice:n.splice},w.extend=w.fn.extend=function(){var e,t,n,r,i,o,a=arguments[0]||{},s=1,u=arguments.length,l=!1;for("boolean"==typeof a&&(l=a,a=arguments[s]||{},s++),"object"==typeof a||g(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in e)n=a[t],a!==(r=e[t])&&(l&&r&&(w.isPlainObject(r)||(i=Array.isArray(r)))?(i?(i=!1,o=n&&Array.isArray(n)?n:[]):o=n&&w.isPlainObject(n)?n:{},a[t]=w.extend(l,o,r)):void 0!==r&&(a[t]=r));return a},w.extend({expando:"jQuery"+("3.3.1"+Math.random()).replace(/\D/g,""),isReady:!0,error:function(e){throw new Error(e)},noop:function(){},isPlainObject:function(e){var t,n;return!(!e||"[object Object]"!==c.call(e))&&(!(t=i(e))||"function"==typeof(n=f.call(t,"constructor")&&t.constructor)&&p.call(n)===d)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},globalEval:function(e){m(e)},each:function(e,t){var n,r=0;if(C(e)){for(n=e.length;r<n;r++)if(!1===t.call(e[r],r,e[r]))break}else for(r in e)if(!1===t.call(e[r],r,e[r]))break;return e},trim:function(e){return null==e?"":(e+"").replace(T,"")},makeArray:function(e,t){var n=t||[];return null!=e&&(C(Object(e))?w.merge(n,"string"==typeof e?[e]:e):s.call(n,e)),n},inArray:function(e,t,n){return null==t?-1:u.call(t,e,n)},merge:function(e,t){for(var n=+t.length,r=0,i=e.length;r<n;r++)e[i++]=t[r];return e.length=i,e},grep:function(e,t,n){for(var r,i=[],o=0,a=e.length,s=!n;o<a;o++)(r=!t(e[o],o))!==s&&i.push(e[o]);return i},map:function(e,t,n){var r,i,o=0,s=[];if(C(e))for(r=e.length;o<r;o++)null!=(i=t(e[o],o,n))&&s.push(i);else for(o in e)null!=(i=t(e[o],o,n))&&s.push(i);return a.apply([],s)},guid:1,support:h}),"function"==typeof Symbol&&(w.fn[Symbol.iterator]=n[Symbol.iterator]),w.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(e,t){l["[object "+t+"]"]=t.toLowerCase()});function C(e){var t=!!e&&"length"in e&&e.length,n=x(e);return!g(e)&&!y(e)&&("array"===n||0===t||"number"==typeof t&&t>0&&t-1 in e)}var E=function(e){var t,n,r,i,o,a,s,u,l,c,f,p,d,h,g,y,v,m,x,b="sizzle"+1*new Date,w=e.document,T=0,C=0,E=ae(),k=ae(),S=ae(),D=function(e,t){return e===t&&(f=!0),0},N={}.hasOwnProperty,A=[],j=A.pop,q=A.push,L=A.push,H=A.slice,O=function(e,t){for(var n=0,r=e.length;n<r;n++)if(e[n]===t)return n;return-1},P="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",M="[\\x20\\t\\r\\n\\f]",R="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",I="\\["+M+"*("+R+")(?:"+M+"*([*^$|!~]?=)"+M+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+R+"))|)"+M+"*\\]",W=":("+R+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+I+")*)|.*)\\)|)",$=new RegExp(M+"+","g"),B=new RegExp("^"+M+"+|((?:^|[^\\\\])(?:\\\\.)*)"+M+"+$","g"),F=new RegExp("^"+M+"*,"+M+"*"),_=new RegExp("^"+M+"*([>+~]|"+M+")"+M+"*"),z=new RegExp("="+M+"*([^\\]'\"]*?)"+M+"*\\]","g"),X=new RegExp(W),U=new RegExp("^"+R+"$"),V={ID:new RegExp("^#("+R+")"),CLASS:new RegExp("^\\.("+R+")"),TAG:new RegExp("^("+R+"|[*])"),ATTR:new RegExp("^"+I),PSEUDO:new RegExp("^"+W),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+M+"*(even|odd|(([+-]|)(\\d*)n|)"+M+"*(?:([+-]|)"+M+"*(\\d+)|))"+M+"*\\)|)","i"),bool:new RegExp("^(?:"+P+")$","i"),needsContext:new RegExp("^"+M+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+M+"*((?:-\\d)?\\d*)"+M+"*\\)|)(?=[^-]|$)","i")},G=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Q=/^[^{]+\{\s*\[native \w/,J=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,K=/[+~]/,Z=new RegExp("\\\\([\\da-f]{1,6}"+M+"?|("+M+")|.)","ig"),ee=function(e,t,n){var r="0x"+t-65536;return r!==r||n?t:r<0?String.fromCharCode(r+65536):String.fromCharCode(r>>10|55296,1023&r|56320)},te=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,ne=function(e,t){return t?"\0"===e?"\ufffd":e.slice(0,-1)+"\\"+e.charCodeAt(e.length-1).toString(16)+" ":"\\"+e},re=function(){p()},ie=me(function(e){return!0===e.disabled&&("form"in e||"label"in e)},{dir:"parentNode",next:"legend"});try{L.apply(A=H.call(w.childNodes),w.childNodes),A[w.childNodes.length].nodeType}catch(e){L={apply:A.length?function(e,t){q.apply(e,H.call(t))}:function(e,t){var n=e.length,r=0;while(e[n++]=t[r++]);e.length=n-1}}}function oe(e,t,r,i){var o,s,l,c,f,h,v,m=t&&t.ownerDocument,T=t?t.nodeType:9;if(r=r||[],"string"!=typeof e||!e||1!==T&&9!==T&&11!==T)return r;if(!i&&((t?t.ownerDocument||t:w)!==d&&p(t),t=t||d,g)){if(11!==T&&(f=J.exec(e)))if(o=f[1]){if(9===T){if(!(l=t.getElementById(o)))return r;if(l.id===o)return r.push(l),r}else if(m&&(l=m.getElementById(o))&&x(t,l)&&l.id===o)return r.push(l),r}else{if(f[2])return L.apply(r,t.getElementsByTagName(e)),r;if((o=f[3])&&n.getElementsByClassName&&t.getElementsByClassName)return L.apply(r,t.getElementsByClassName(o)),r}if(n.qsa&&!S[e+" "]&&(!y||!y.test(e))){if(1!==T)m=t,v=e;else if("object"!==t.nodeName.toLowerCase()){(c=t.getAttribute("id"))?c=c.replace(te,ne):t.setAttribute("id",c=b),s=(h=a(e)).length;while(s--)h[s]="#"+c+" "+ve(h[s]);v=h.join(","),m=K.test(e)&&ge(t.parentNode)||t}if(v)try{return L.apply(r,m.querySelectorAll(v)),r}catch(e){}finally{c===b&&t.removeAttribute("id")}}}return u(e.replace(B,"$1"),t,r,i)}function ae(){var e=[];function t(n,i){return e.push(n+" ")>r.cacheLength&&delete t[e.shift()],t[n+" "]=i}return t}function se(e){return e[b]=!0,e}function ue(e){var t=d.createElement("fieldset");try{return!!e(t)}catch(e){return!1}finally{t.parentNode&&t.parentNode.removeChild(t),t=null}}function le(e,t){var n=e.split("|"),i=n.length;while(i--)r.attrHandle[n[i]]=t}function ce(e,t){var n=t&&e,r=n&&1===e.nodeType&&1===t.nodeType&&e.sourceIndex-t.sourceIndex;if(r)return r;if(n)while(n=n.nextSibling)if(n===t)return-1;return e?1:-1}function fe(e){return function(t){return"input"===t.nodeName.toLowerCase()&&t.type===e}}function pe(e){return function(t){var n=t.nodeName.toLowerCase();return("input"===n||"button"===n)&&t.type===e}}function de(e){return function(t){return"form"in t?t.parentNode&&!1===t.disabled?"label"in t?"label"in t.parentNode?t.parentNode.disabled===e:t.disabled===e:t.isDisabled===e||t.isDisabled!==!e&&ie(t)===e:t.disabled===e:"label"in t&&t.disabled===e}}function he(e){return se(function(t){return t=+t,se(function(n,r){var i,o=e([],n.length,t),a=o.length;while(a--)n[i=o[a]]&&(n[i]=!(r[i]=n[i]))})})}function ge(e){return e&&"undefined"!=typeof e.getElementsByTagName&&e}n=oe.support={},o=oe.isXML=function(e){var t=e&&(e.ownerDocument||e).documentElement;return!!t&&"HTML"!==t.nodeName},p=oe.setDocument=function(e){var t,i,a=e?e.ownerDocument||e:w;return a!==d&&9===a.nodeType&&a.documentElement?(d=a,h=d.documentElement,g=!o(d),w!==d&&(i=d.defaultView)&&i.top!==i&&(i.addEventListener?i.addEventListener("unload",re,!1):i.attachEvent&&i.attachEvent("onunload",re)),n.attributes=ue(function(e){return e.className="i",!e.getAttribute("className")}),n.getElementsByTagName=ue(function(e){return e.appendChild(d.createComment("")),!e.getElementsByTagName("*").length}),n.getElementsByClassName=Q.test(d.getElementsByClassName),n.getById=ue(function(e){return h.appendChild(e).id=b,!d.getElementsByName||!d.getElementsByName(b).length}),n.getById?(r.filter.ID=function(e){var t=e.replace(Z,ee);return function(e){return e.getAttribute("id")===t}},r.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&g){var n=t.getElementById(e);return n?[n]:[]}}):(r.filter.ID=function(e){var t=e.replace(Z,ee);return function(e){var n="undefined"!=typeof e.getAttributeNode&&e.getAttributeNode("id");return n&&n.value===t}},r.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&g){var n,r,i,o=t.getElementById(e);if(o){if((n=o.getAttributeNode("id"))&&n.value===e)return[o];i=t.getElementsByName(e),r=0;while(o=i[r++])if((n=o.getAttributeNode("id"))&&n.value===e)return[o]}return[]}}),r.find.TAG=n.getElementsByTagName?function(e,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e):n.qsa?t.querySelectorAll(e):void 0}:function(e,t){var n,r=[],i=0,o=t.getElementsByTagName(e);if("*"===e){while(n=o[i++])1===n.nodeType&&r.push(n);return r}return o},r.find.CLASS=n.getElementsByClassName&&function(e,t){if("undefined"!=typeof t.getElementsByClassName&&g)return t.getElementsByClassName(e)},v=[],y=[],(n.qsa=Q.test(d.querySelectorAll))&&(ue(function(e){h.appendChild(e).innerHTML="<a id='"+b+"'></a><select id='"+b+"-\r\\' msallowcapture=''><option selected=''></option></select>",e.querySelectorAll("[msallowcapture^='']").length&&y.push("[*^$]="+M+"*(?:''|\"\")"),e.querySelectorAll("[selected]").length||y.push("\\["+M+"*(?:value|"+P+")"),e.querySelectorAll("[id~="+b+"-]").length||y.push("~="),e.querySelectorAll(":checked").length||y.push(":checked"),e.querySelectorAll("a#"+b+"+*").length||y.push(".#.+[+~]")}),ue(function(e){e.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var t=d.createElement("input");t.setAttribute("type","hidden"),e.appendChild(t).setAttribute("name","D"),e.querySelectorAll("[name=d]").length&&y.push("name"+M+"*[*^$|!~]?="),2!==e.querySelectorAll(":enabled").length&&y.push(":enabled",":disabled"),h.appendChild(e).disabled=!0,2!==e.querySelectorAll(":disabled").length&&y.push(":enabled",":disabled"),e.querySelectorAll("*,:x"),y.push(",.*:")})),(n.matchesSelector=Q.test(m=h.matches||h.webkitMatchesSelector||h.mozMatchesSelector||h.oMatchesSelector||h.msMatchesSelector))&&ue(function(e){n.disconnectedMatch=m.call(e,"*"),m.call(e,"[s!='']:x"),v.push("!=",W)}),y=y.length&&new RegExp(y.join("|")),v=v.length&&new RegExp(v.join("|")),t=Q.test(h.compareDocumentPosition),x=t||Q.test(h.contains)?function(e,t){var n=9===e.nodeType?e.documentElement:e,r=t&&t.parentNode;return e===r||!(!r||1!==r.nodeType||!(n.contains?n.contains(r):e.compareDocumentPosition&&16&e.compareDocumentPosition(r)))}:function(e,t){if(t)while(t=t.parentNode)if(t===e)return!0;return!1},D=t?function(e,t){if(e===t)return f=!0,0;var r=!e.compareDocumentPosition-!t.compareDocumentPosition;return r||(1&(r=(e.ownerDocument||e)===(t.ownerDocument||t)?e.compareDocumentPosition(t):1)||!n.sortDetached&&t.compareDocumentPosition(e)===r?e===d||e.ownerDocument===w&&x(w,e)?-1:t===d||t.ownerDocument===w&&x(w,t)?1:c?O(c,e)-O(c,t):0:4&r?-1:1)}:function(e,t){if(e===t)return f=!0,0;var n,r=0,i=e.parentNode,o=t.parentNode,a=[e],s=[t];if(!i||!o)return e===d?-1:t===d?1:i?-1:o?1:c?O(c,e)-O(c,t):0;if(i===o)return ce(e,t);n=e;while(n=n.parentNode)a.unshift(n);n=t;while(n=n.parentNode)s.unshift(n);while(a[r]===s[r])r++;return r?ce(a[r],s[r]):a[r]===w?-1:s[r]===w?1:0},d):d},oe.matches=function(e,t){return oe(e,null,null,t)},oe.matchesSelector=function(e,t){if((e.ownerDocument||e)!==d&&p(e),t=t.replace(z,"='$1']"),n.matchesSelector&&g&&!S[t+" "]&&(!v||!v.test(t))&&(!y||!y.test(t)))try{var r=m.call(e,t);if(r||n.disconnectedMatch||e.document&&11!==e.document.nodeType)return r}catch(e){}return oe(t,d,null,[e]).length>0},oe.contains=function(e,t){return(e.ownerDocument||e)!==d&&p(e),x(e,t)},oe.attr=function(e,t){(e.ownerDocument||e)!==d&&p(e);var i=r.attrHandle[t.toLowerCase()],o=i&&N.call(r.attrHandle,t.toLowerCase())?i(e,t,!g):void 0;return void 0!==o?o:n.attributes||!g?e.getAttribute(t):(o=e.getAttributeNode(t))&&o.specified?o.value:null},oe.escape=function(e){return(e+"").replace(te,ne)},oe.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)},oe.uniqueSort=function(e){var t,r=[],i=0,o=0;if(f=!n.detectDuplicates,c=!n.sortStable&&e.slice(0),e.sort(D),f){while(t=e[o++])t===e[o]&&(i=r.push(o));while(i--)e.splice(r[i],1)}return c=null,e},i=oe.getText=function(e){var t,n="",r=0,o=e.nodeType;if(o){if(1===o||9===o||11===o){if("string"==typeof e.textContent)return e.textContent;for(e=e.firstChild;e;e=e.nextSibling)n+=i(e)}else if(3===o||4===o)return e.nodeValue}else while(t=e[r++])n+=i(t);return n},(r=oe.selectors={cacheLength:50,createPseudo:se,match:V,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(Z,ee),e[3]=(e[3]||e[4]||e[5]||"").replace(Z,ee),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||oe.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&oe.error(e[0]),e},PSEUDO:function(e){var t,n=!e[6]&&e[2];return V.CHILD.test(e[0])?null:(e[3]?e[2]=e[4]||e[5]||"":n&&X.test(n)&&(t=a(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){var t=e.replace(Z,ee).toLowerCase();return"*"===e?function(){return!0}:function(e){return e.nodeName&&e.nodeName.toLowerCase()===t}},CLASS:function(e){var t=E[e+" "];return t||(t=new RegExp("(^|"+M+")"+e+"("+M+"|$)"))&&E(e,function(e){return t.test("string"==typeof e.className&&e.className||"undefined"!=typeof e.getAttribute&&e.getAttribute("class")||"")})},ATTR:function(e,t,n){return function(r){var i=oe.attr(r,e);return null==i?"!="===t:!t||(i+="","="===t?i===n:"!="===t?i!==n:"^="===t?n&&0===i.indexOf(n):"*="===t?n&&i.indexOf(n)>-1:"$="===t?n&&i.slice(-n.length)===n:"~="===t?(" "+i.replace($," ")+" ").indexOf(n)>-1:"|="===t&&(i===n||i.slice(0,n.length+1)===n+"-"))}},CHILD:function(e,t,n,r,i){var o="nth"!==e.slice(0,3),a="last"!==e.slice(-4),s="of-type"===t;return 1===r&&0===i?function(e){return!!e.parentNode}:function(t,n,u){var l,c,f,p,d,h,g=o!==a?"nextSibling":"previousSibling",y=t.parentNode,v=s&&t.nodeName.toLowerCase(),m=!u&&!s,x=!1;if(y){if(o){while(g){p=t;while(p=p[g])if(s?p.nodeName.toLowerCase()===v:1===p.nodeType)return!1;h=g="only"===e&&!h&&"nextSibling"}return!0}if(h=[a?y.firstChild:y.lastChild],a&&m){x=(d=(l=(c=(f=(p=y)[b]||(p[b]={}))[p.uniqueID]||(f[p.uniqueID]={}))[e]||[])[0]===T&&l[1])&&l[2],p=d&&y.childNodes[d];while(p=++d&&p&&p[g]||(x=d=0)||h.pop())if(1===p.nodeType&&++x&&p===t){c[e]=[T,d,x];break}}else if(m&&(x=d=(l=(c=(f=(p=t)[b]||(p[b]={}))[p.uniqueID]||(f[p.uniqueID]={}))[e]||[])[0]===T&&l[1]),!1===x)while(p=++d&&p&&p[g]||(x=d=0)||h.pop())if((s?p.nodeName.toLowerCase()===v:1===p.nodeType)&&++x&&(m&&((c=(f=p[b]||(p[b]={}))[p.uniqueID]||(f[p.uniqueID]={}))[e]=[T,x]),p===t))break;return(x-=i)===r||x%r==0&&x/r>=0}}},PSEUDO:function(e,t){var n,i=r.pseudos[e]||r.setFilters[e.toLowerCase()]||oe.error("unsupported pseudo: "+e);return i[b]?i(t):i.length>1?(n=[e,e,"",t],r.setFilters.hasOwnProperty(e.toLowerCase())?se(function(e,n){var r,o=i(e,t),a=o.length;while(a--)e[r=O(e,o[a])]=!(n[r]=o[a])}):function(e){return i(e,0,n)}):i}},pseudos:{not:se(function(e){var t=[],n=[],r=s(e.replace(B,"$1"));return r[b]?se(function(e,t,n,i){var o,a=r(e,null,i,[]),s=e.length;while(s--)(o=a[s])&&(e[s]=!(t[s]=o))}):function(e,i,o){return t[0]=e,r(t,null,o,n),t[0]=null,!n.pop()}}),has:se(function(e){return function(t){return oe(e,t).length>0}}),contains:se(function(e){return e=e.replace(Z,ee),function(t){return(t.textContent||t.innerText||i(t)).indexOf(e)>-1}}),lang:se(function(e){return U.test(e||"")||oe.error("unsupported lang: "+e),e=e.replace(Z,ee).toLowerCase(),function(t){var n;do{if(n=g?t.lang:t.getAttribute("xml:lang")||t.getAttribute("lang"))return(n=n.toLowerCase())===e||0===n.indexOf(e+"-")}while((t=t.parentNode)&&1===t.nodeType);return!1}}),target:function(t){var n=e.location&&e.location.hash;return n&&n.slice(1)===t.id},root:function(e){return e===h},focus:function(e){return e===d.activeElement&&(!d.hasFocus||d.hasFocus())&&!!(e.type||e.href||~e.tabIndex)},enabled:de(!1),disabled:de(!0),checked:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&!!e.checked||"option"===t&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,!0===e.selected},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeType<6)return!1;return!0},parent:function(e){return!r.pseudos.empty(e)},header:function(e){return Y.test(e.nodeName)},input:function(e){return G.test(e.nodeName)},button:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&"button"===e.type||"button"===t},text:function(e){var t;return"input"===e.nodeName.toLowerCase()&&"text"===e.type&&(null==(t=e.getAttribute("type"))||"text"===t.toLowerCase())},first:he(function(){return[0]}),last:he(function(e,t){return[t-1]}),eq:he(function(e,t,n){return[n<0?n+t:n]}),even:he(function(e,t){for(var n=0;n<t;n+=2)e.push(n);return e}),odd:he(function(e,t){for(var n=1;n<t;n+=2)e.push(n);return e}),lt:he(function(e,t,n){for(var r=n<0?n+t:n;--r>=0;)e.push(r);return e}),gt:he(function(e,t,n){for(var r=n<0?n+t:n;++r<t;)e.push(r);return e})}}).pseudos.nth=r.pseudos.eq;for(t in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})r.pseudos[t]=fe(t);for(t in{submit:!0,reset:!0})r.pseudos[t]=pe(t);function ye(){}ye.prototype=r.filters=r.pseudos,r.setFilters=new ye,a=oe.tokenize=function(e,t){var n,i,o,a,s,u,l,c=k[e+" "];if(c)return t?0:c.slice(0);s=e,u=[],l=r.preFilter;while(s){n&&!(i=F.exec(s))||(i&&(s=s.slice(i[0].length)||s),u.push(o=[])),n=!1,(i=_.exec(s))&&(n=i.shift(),o.push({value:n,type:i[0].replace(B," ")}),s=s.slice(n.length));for(a in r.filter)!(i=V[a].exec(s))||l[a]&&!(i=l[a](i))||(n=i.shift(),o.push({value:n,type:a,matches:i}),s=s.slice(n.length));if(!n)break}return t?s.length:s?oe.error(e):k(e,u).slice(0)};function ve(e){for(var t=0,n=e.length,r="";t<n;t++)r+=e[t].value;return r}function me(e,t,n){var r=t.dir,i=t.next,o=i||r,a=n&&"parentNode"===o,s=C++;return t.first?function(t,n,i){while(t=t[r])if(1===t.nodeType||a)return e(t,n,i);return!1}:function(t,n,u){var l,c,f,p=[T,s];if(u){while(t=t[r])if((1===t.nodeType||a)&&e(t,n,u))return!0}else while(t=t[r])if(1===t.nodeType||a)if(f=t[b]||(t[b]={}),c=f[t.uniqueID]||(f[t.uniqueID]={}),i&&i===t.nodeName.toLowerCase())t=t[r]||t;else{if((l=c[o])&&l[0]===T&&l[1]===s)return p[2]=l[2];if(c[o]=p,p[2]=e(t,n,u))return!0}return!1}}function xe(e){return e.length>1?function(t,n,r){var i=e.length;while(i--)if(!e[i](t,n,r))return!1;return!0}:e[0]}function be(e,t,n){for(var r=0,i=t.length;r<i;r++)oe(e,t[r],n);return n}function we(e,t,n,r,i){for(var o,a=[],s=0,u=e.length,l=null!=t;s<u;s++)(o=e[s])&&(n&&!n(o,r,i)||(a.push(o),l&&t.push(s)));return a}function Te(e,t,n,r,i,o){return r&&!r[b]&&(r=Te(r)),i&&!i[b]&&(i=Te(i,o)),se(function(o,a,s,u){var l,c,f,p=[],d=[],h=a.length,g=o||be(t||"*",s.nodeType?[s]:s,[]),y=!e||!o&&t?g:we(g,p,e,s,u),v=n?i||(o?e:h||r)?[]:a:y;if(n&&n(y,v,s,u),r){l=we(v,d),r(l,[],s,u),c=l.length;while(c--)(f=l[c])&&(v[d[c]]=!(y[d[c]]=f))}if(o){if(i||e){if(i){l=[],c=v.length;while(c--)(f=v[c])&&l.push(y[c]=f);i(null,v=[],l,u)}c=v.length;while(c--)(f=v[c])&&(l=i?O(o,f):p[c])>-1&&(o[l]=!(a[l]=f))}}else v=we(v===a?v.splice(h,v.length):v),i?i(null,a,v,u):L.apply(a,v)})}function Ce(e){for(var t,n,i,o=e.length,a=r.relative[e[0].type],s=a||r.relative[" "],u=a?1:0,c=me(function(e){return e===t},s,!0),f=me(function(e){return O(t,e)>-1},s,!0),p=[function(e,n,r){var i=!a&&(r||n!==l)||((t=n).nodeType?c(e,n,r):f(e,n,r));return t=null,i}];u<o;u++)if(n=r.relative[e[u].type])p=[me(xe(p),n)];else{if((n=r.filter[e[u].type].apply(null,e[u].matches))[b]){for(i=++u;i<o;i++)if(r.relative[e[i].type])break;return Te(u>1&&xe(p),u>1&&ve(e.slice(0,u-1).concat({value:" "===e[u-2].type?"*":""})).replace(B,"$1"),n,u<i&&Ce(e.slice(u,i)),i<o&&Ce(e=e.slice(i)),i<o&&ve(e))}p.push(n)}return xe(p)}function Ee(e,t){var n=t.length>0,i=e.length>0,o=function(o,a,s,u,c){var f,h,y,v=0,m="0",x=o&&[],b=[],w=l,C=o||i&&r.find.TAG("*",c),E=T+=null==w?1:Math.random()||.1,k=C.length;for(c&&(l=a===d||a||c);m!==k&&null!=(f=C[m]);m++){if(i&&f){h=0,a||f.ownerDocument===d||(p(f),s=!g);while(y=e[h++])if(y(f,a||d,s)){u.push(f);break}c&&(T=E)}n&&((f=!y&&f)&&v--,o&&x.push(f))}if(v+=m,n&&m!==v){h=0;while(y=t[h++])y(x,b,a,s);if(o){if(v>0)while(m--)x[m]||b[m]||(b[m]=j.call(u));b=we(b)}L.apply(u,b),c&&!o&&b.length>0&&v+t.length>1&&oe.uniqueSort(u)}return c&&(T=E,l=w),x};return n?se(o):o}return s=oe.compile=function(e,t){var n,r=[],i=[],o=S[e+" "];if(!o){t||(t=a(e)),n=t.length;while(n--)(o=Ce(t[n]))[b]?r.push(o):i.push(o);(o=S(e,Ee(i,r))).selector=e}return o},u=oe.select=function(e,t,n,i){var o,u,l,c,f,p="function"==typeof e&&e,d=!i&&a(e=p.selector||e);if(n=n||[],1===d.length){if((u=d[0]=d[0].slice(0)).length>2&&"ID"===(l=u[0]).type&&9===t.nodeType&&g&&r.relative[u[1].type]){if(!(t=(r.find.ID(l.matches[0].replace(Z,ee),t)||[])[0]))return n;p&&(t=t.parentNode),e=e.slice(u.shift().value.length)}o=V.needsContext.test(e)?0:u.length;while(o--){if(l=u[o],r.relative[c=l.type])break;if((f=r.find[c])&&(i=f(l.matches[0].replace(Z,ee),K.test(u[0].type)&&ge(t.parentNode)||t))){if(u.splice(o,1),!(e=i.length&&ve(u)))return L.apply(n,i),n;break}}}return(p||s(e,d))(i,t,!g,n,!t||K.test(e)&&ge(t.parentNode)||t),n},n.sortStable=b.split("").sort(D).join("")===b,n.detectDuplicates=!!f,p(),n.sortDetached=ue(function(e){return 1&e.compareDocumentPosition(d.createElement("fieldset"))}),ue(function(e){return e.innerHTML="<a href='#'></a>","#"===e.firstChild.getAttribute("href")})||le("type|href|height|width",function(e,t,n){if(!n)return e.getAttribute(t,"type"===t.toLowerCase()?1:2)}),n.attributes&&ue(function(e){return e.innerHTML="<input/>",e.firstChild.setAttribute("value",""),""===e.firstChild.getAttribute("value")})||le("value",function(e,t,n){if(!n&&"input"===e.nodeName.toLowerCase())return e.defaultValue}),ue(function(e){return null==e.getAttribute("disabled")})||le(P,function(e,t,n){var r;if(!n)return!0===e[t]?t.toLowerCase():(r=e.getAttributeNode(t))&&r.specified?r.value:null}),oe}(e);w.find=E,w.expr=E.selectors,w.expr[":"]=w.expr.pseudos,w.uniqueSort=w.unique=E.uniqueSort,w.text=E.getText,w.isXMLDoc=E.isXML,w.contains=E.contains,w.escapeSelector=E.escape;var k=function(e,t,n){var r=[],i=void 0!==n;while((e=e[t])&&9!==e.nodeType)if(1===e.nodeType){if(i&&w(e).is(n))break;r.push(e)}return r},S=function(e,t){for(var n=[];e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n},D=w.expr.match.needsContext;function N(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()}var A=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;function j(e,t,n){return g(t)?w.grep(e,function(e,r){return!!t.call(e,r,e)!==n}):t.nodeType?w.grep(e,function(e){return e===t!==n}):"string"!=typeof t?w.grep(e,function(e){return u.call(t,e)>-1!==n}):w.filter(t,e,n)}w.filter=function(e,t,n){var r=t[0];return n&&(e=":not("+e+")"),1===t.length&&1===r.nodeType?w.find.matchesSelector(r,e)?[r]:[]:w.find.matches(e,w.grep(t,function(e){return 1===e.nodeType}))},w.fn.extend({find:function(e){var t,n,r=this.length,i=this;if("string"!=typeof e)return this.pushStack(w(e).filter(function(){for(t=0;t<r;t++)if(w.contains(i[t],this))return!0}));for(n=this.pushStack([]),t=0;t<r;t++)w.find(e,i[t],n);return r>1?w.uniqueSort(n):n},filter:function(e){return this.pushStack(j(this,e||[],!1))},not:function(e){return this.pushStack(j(this,e||[],!0))},is:function(e){return!!j(this,"string"==typeof e&&D.test(e)?w(e):e||[],!1).length}});var q,L=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;(w.fn.init=function(e,t,n){var i,o;if(!e)return this;if(n=n||q,"string"==typeof e){if(!(i="<"===e[0]&&">"===e[e.length-1]&&e.length>=3?[null,e,null]:L.exec(e))||!i[1]&&t)return!t||t.jquery?(t||n).find(e):this.constructor(t).find(e);if(i[1]){if(t=t instanceof w?t[0]:t,w.merge(this,w.parseHTML(i[1],t&&t.nodeType?t.ownerDocument||t:r,!0)),A.test(i[1])&&w.isPlainObject(t))for(i in t)g(this[i])?this[i](t[i]):this.attr(i,t[i]);return this}return(o=r.getElementById(i[2]))&&(this[0]=o,this.length=1),this}return e.nodeType?(this[0]=e,this.length=1,this):g(e)?void 0!==n.ready?n.ready(e):e(w):w.makeArray(e,this)}).prototype=w.fn,q=w(r);var H=/^(?:parents|prev(?:Until|All))/,O={children:!0,contents:!0,next:!0,prev:!0};w.fn.extend({has:function(e){var t=w(e,this),n=t.length;return this.filter(function(){for(var e=0;e<n;e++)if(w.contains(this,t[e]))return!0})},closest:function(e,t){var n,r=0,i=this.length,o=[],a="string"!=typeof e&&w(e);if(!D.test(e))for(;r<i;r++)for(n=this[r];n&&n!==t;n=n.parentNode)if(n.nodeType<11&&(a?a.index(n)>-1:1===n.nodeType&&w.find.matchesSelector(n,e))){o.push(n);break}return this.pushStack(o.length>1?w.uniqueSort(o):o)},index:function(e){return e?"string"==typeof e?u.call(w(e),this[0]):u.call(this,e.jquery?e[0]:e):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){return this.pushStack(w.uniqueSort(w.merge(this.get(),w(e,t))))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}});function P(e,t){while((e=e[t])&&1!==e.nodeType);return e}w.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return k(e,"parentNode")},parentsUntil:function(e,t,n){return k(e,"parentNode",n)},next:function(e){return P(e,"nextSibling")},prev:function(e){return P(e,"previousSibling")},nextAll:function(e){return k(e,"nextSibling")},prevAll:function(e){return k(e,"previousSibling")},nextUntil:function(e,t,n){return k(e,"nextSibling",n)},prevUntil:function(e,t,n){return k(e,"previousSibling",n)},siblings:function(e){return S((e.parentNode||{}).firstChild,e)},children:function(e){return S(e.firstChild)},contents:function(e){return N(e,"iframe")?e.contentDocument:(N(e,"template")&&(e=e.content||e),w.merge([],e.childNodes))}},function(e,t){w.fn[e]=function(n,r){var i=w.map(this,t,n);return"Until"!==e.slice(-5)&&(r=n),r&&"string"==typeof r&&(i=w.filter(r,i)),this.length>1&&(O[e]||w.uniqueSort(i),H.test(e)&&i.reverse()),this.pushStack(i)}});var M=/[^\x20\t\r\n\f]+/g;function R(e){var t={};return w.each(e.match(M)||[],function(e,n){t[n]=!0}),t}w.Callbacks=function(e){e="string"==typeof e?R(e):w.extend({},e);var t,n,r,i,o=[],a=[],s=-1,u=function(){for(i=i||e.once,r=t=!0;a.length;s=-1){n=a.shift();while(++s<o.length)!1===o[s].apply(n[0],n[1])&&e.stopOnFalse&&(s=o.length,n=!1)}e.memory||(n=!1),t=!1,i&&(o=n?[]:"")},l={add:function(){return o&&(n&&!t&&(s=o.length-1,a.push(n)),function t(n){w.each(n,function(n,r){g(r)?e.unique&&l.has(r)||o.push(r):r&&r.length&&"string"!==x(r)&&t(r)})}(arguments),n&&!t&&u()),this},remove:function(){return w.each(arguments,function(e,t){var n;while((n=w.inArray(t,o,n))>-1)o.splice(n,1),n<=s&&s--}),this},has:function(e){return e?w.inArray(e,o)>-1:o.length>0},empty:function(){return o&&(o=[]),this},disable:function(){return i=a=[],o=n="",this},disabled:function(){return!o},lock:function(){return i=a=[],n||t||(o=n=""),this},locked:function(){return!!i},fireWith:function(e,n){return i||(n=[e,(n=n||[]).slice?n.slice():n],a.push(n),t||u()),this},fire:function(){return l.fireWith(this,arguments),this},fired:function(){return!!r}};return l};function I(e){return e}function W(e){throw e}function $(e,t,n,r){var i;try{e&&g(i=e.promise)?i.call(e).done(t).fail(n):e&&g(i=e.then)?i.call(e,t,n):t.apply(void 0,[e].slice(r))}catch(e){n.apply(void 0,[e])}}w.extend({Deferred:function(t){var n=[["notify","progress",w.Callbacks("memory"),w.Callbacks("memory"),2],["resolve","done",w.Callbacks("once memory"),w.Callbacks("once memory"),0,"resolved"],["reject","fail",w.Callbacks("once memory"),w.Callbacks("once memory"),1,"rejected"]],r="pending",i={state:function(){return r},always:function(){return o.done(arguments).fail(arguments),this},"catch":function(e){return i.then(null,e)},pipe:function(){var e=arguments;return w.Deferred(function(t){w.each(n,function(n,r){var i=g(e[r[4]])&&e[r[4]];o[r[1]](function(){var e=i&&i.apply(this,arguments);e&&g(e.promise)?e.promise().progress(t.notify).done(t.resolve).fail(t.reject):t[r[0]+"With"](this,i?[e]:arguments)})}),e=null}).promise()},then:function(t,r,i){var o=0;function a(t,n,r,i){return function(){var s=this,u=arguments,l=function(){var e,l;if(!(t<o)){if((e=r.apply(s,u))===n.promise())throw new TypeError("Thenable self-resolution");l=e&&("object"==typeof e||"function"==typeof e)&&e.then,g(l)?i?l.call(e,a(o,n,I,i),a(o,n,W,i)):(o++,l.call(e,a(o,n,I,i),a(o,n,W,i),a(o,n,I,n.notifyWith))):(r!==I&&(s=void 0,u=[e]),(i||n.resolveWith)(s,u))}},c=i?l:function(){try{l()}catch(e){w.Deferred.exceptionHook&&w.Deferred.exceptionHook(e,c.stackTrace),t+1>=o&&(r!==W&&(s=void 0,u=[e]),n.rejectWith(s,u))}};t?c():(w.Deferred.getStackHook&&(c.stackTrace=w.Deferred.getStackHook()),e.setTimeout(c))}}return w.Deferred(function(e){n[0][3].add(a(0,e,g(i)?i:I,e.notifyWith)),n[1][3].add(a(0,e,g(t)?t:I)),n[2][3].add(a(0,e,g(r)?r:W))}).promise()},promise:function(e){return null!=e?w.extend(e,i):i}},o={};return w.each(n,function(e,t){var a=t[2],s=t[5];i[t[1]]=a.add,s&&a.add(function(){r=s},n[3-e][2].disable,n[3-e][3].disable,n[0][2].lock,n[0][3].lock),a.add(t[3].fire),o[t[0]]=function(){return o[t[0]+"With"](this===o?void 0:this,arguments),this},o[t[0]+"With"]=a.fireWith}),i.promise(o),t&&t.call(o,o),o},when:function(e){var t=arguments.length,n=t,r=Array(n),i=o.call(arguments),a=w.Deferred(),s=function(e){return function(n){r[e]=this,i[e]=arguments.length>1?o.call(arguments):n,--t||a.resolveWith(r,i)}};if(t<=1&&($(e,a.done(s(n)).resolve,a.reject,!t),"pending"===a.state()||g(i[n]&&i[n].then)))return a.then();while(n--)$(i[n],s(n),a.reject);return a.promise()}});var B=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;w.Deferred.exceptionHook=function(t,n){e.console&&e.console.warn&&t&&B.test(t.name)&&e.console.warn("jQuery.Deferred exception: "+t.message,t.stack,n)},w.readyException=function(t){e.setTimeout(function(){throw t})};var F=w.Deferred();w.fn.ready=function(e){return F.then(e)["catch"](function(e){w.readyException(e)}),this},w.extend({isReady:!1,readyWait:1,ready:function(e){(!0===e?--w.readyWait:w.isReady)||(w.isReady=!0,!0!==e&&--w.readyWait>0||F.resolveWith(r,[w]))}}),w.ready.then=F.then;function _(){r.removeEventListener("DOMContentLoaded",_),e.removeEventListener("load",_),w.ready()}"complete"===r.readyState||"loading"!==r.readyState&&!r.documentElement.doScroll?e.setTimeout(w.ready):(r.addEventListener("DOMContentLoaded",_),e.addEventListener("load",_));var z=function(e,t,n,r,i,o,a){var s=0,u=e.length,l=null==n;if("object"===x(n)){i=!0;for(s in n)z(e,t,s,n[s],!0,o,a)}else if(void 0!==r&&(i=!0,g(r)||(a=!0),l&&(a?(t.call(e,r),t=null):(l=t,t=function(e,t,n){return l.call(w(e),n)})),t))for(;s<u;s++)t(e[s],n,a?r:r.call(e[s],s,t(e[s],n)));return i?e:l?t.call(e):u?t(e[0],n):o},X=/^-ms-/,U=/-([a-z])/g;function V(e,t){return t.toUpperCase()}function G(e){return e.replace(X,"ms-").replace(U,V)}var Y=function(e){return 1===e.nodeType||9===e.nodeType||!+e.nodeType};function Q(){this.expando=w.expando+Q.uid++}Q.uid=1,Q.prototype={cache:function(e){var t=e[this.expando];return t||(t={},Y(e)&&(e.nodeType?e[this.expando]=t:Object.defineProperty(e,this.expando,{value:t,configurable:!0}))),t},set:function(e,t,n){var r,i=this.cache(e);if("string"==typeof t)i[G(t)]=n;else for(r in t)i[G(r)]=t[r];return i},get:function(e,t){return void 0===t?this.cache(e):e[this.expando]&&e[this.expando][G(t)]},access:function(e,t,n){return void 0===t||t&&"string"==typeof t&&void 0===n?this.get(e,t):(this.set(e,t,n),void 0!==n?n:t)},remove:function(e,t){var n,r=e[this.expando];if(void 0!==r){if(void 0!==t){n=(t=Array.isArray(t)?t.map(G):(t=G(t))in r?[t]:t.match(M)||[]).length;while(n--)delete r[t[n]]}(void 0===t||w.isEmptyObject(r))&&(e.nodeType?e[this.expando]=void 0:delete e[this.expando])}},hasData:function(e){var t=e[this.expando];return void 0!==t&&!w.isEmptyObject(t)}};var J=new Q,K=new Q,Z=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,ee=/[A-Z]/g;function te(e){return"true"===e||"false"!==e&&("null"===e?null:e===+e+""?+e:Z.test(e)?JSON.parse(e):e)}function ne(e,t,n){var r;if(void 0===n&&1===e.nodeType)if(r="data-"+t.replace(ee,"-{{ jquery }}").toLowerCase(),"string"==typeof(n=e.getAttribute(r))){try{n=te(n)}catch(e){}K.set(e,t,n)}else n=void 0;return n}w.extend({hasData:function(e){return K.hasData(e)||J.hasData(e)},data:function(e,t,n){return K.access(e,t,n)},removeData:function(e,t){K.remove(e,t)},_data:function(e,t,n){return J.access(e,t,n)},_removeData:function(e,t){J.remove(e,t)}}),w.fn.extend({data:function(e,t){var n,r,i,o=this[0],a=o&&o.attributes;if(void 0===e){if(this.length&&(i=K.get(o),1===o.nodeType&&!J.get(o,"hasDataAttrs"))){n=a.length;while(n--)a[n]&&0===(r=a[n].name).indexOf("data-")&&(r=G(r.slice(5)),ne(o,r,i[r]));J.set(o,"hasDataAttrs",!0)}return i}return"object"==typeof e?this.each(function(){K.set(this,e)}):z(this,function(t){var n;if(o&&void 0===t){if(void 0!==(n=K.get(o,e)))return n;if(void 0!==(n=ne(o,e)))return n}else this.each(function(){K.set(this,e,t)})},null,t,arguments.length>1,null,!0)},removeData:function(e){return this.each(function(){K.remove(this,e)})}}),w.extend({queue:function(e,t,n){var r;if(e)return t=(t||"fx")+"queue",r=J.get(e,t),n&&(!r||Array.isArray(n)?r=J.access(e,t,w.makeArray(n)):r.push(n)),r||[]},dequeue:function(e,t){t=t||"fx";var n=w.queue(e,t),r=n.length,i=n.shift(),o=w._queueHooks(e,t),a=function(){w.dequeue(e,t)};"inprogress"===i&&(i=n.shift(),r--),i&&("fx"===t&&n.unshift("inprogress"),delete o.stop,i.call(e,a,o)),!r&&o&&o.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return J.get(e,n)||J.access(e,n,{empty:w.Callbacks("once memory").add(function(){J.remove(e,[t+"queue",n])})})}}),w.fn.extend({queue:function(e,t){var n=2;return"string"!=typeof e&&(t=e,e="fx",n--),arguments.length<n?w.queue(this[0],e):void 0===t?this:this.each(function(){var n=w.queue(this,e,t);w._queueHooks(this,e),"fx"===e&&"inprogress"!==n[0]&&w.dequeue(this,e)})},dequeue:function(e){return this.each(function(){w.dequeue(this,e)})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,t){var n,r=1,i=w.Deferred(),o=this,a=this.length,s=function(){--r||i.resolveWith(o,[o])};"string"!=typeof e&&(t=e,e=void 0),e=e||"fx";while(a--)(n=J.get(o[a],e+"queueHooks"))&&n.empty&&(r++,n.empty.add(s));return s(),i.promise(t)}});var re=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,ie=new RegExp("^(?:([+-])=|)("+re+")([a-z%]*)$","i"),oe=["Top","Right","Bottom","Left"],ae=function(e,t){return"none"===(e=t||e).style.display||""===e.style.display&&w.contains(e.ownerDocument,e)&&"none"===w.css(e,"display")},se=function(e,t,n,r){var i,o,a={};for(o in t)a[o]=e.style[o],e.style[o]=t[o];i=n.apply(e,r||[]);for(o in t)e.style[o]=a[o];return i};function ue(e,t,n,r){var i,o,a=20,s=r?function(){return r.cur()}:function(){return w.css(e,t,"")},u=s(),l=n&&n[3]||(w.cssNumber[t]?"":"px"),c=(w.cssNumber[t]||"px"!==l&&+u)&&ie.exec(w.css(e,t));if(c&&c[3]!==l){u/=2,l=l||c[3],c=+u||1;while(a--)w.style(e,t,c+l),(1-o)*(1-(o=s()/u||.5))<=0&&(a=0),c/=o;c*=2,w.style(e,t,c+l),n=n||[]}return n&&(c=+c||+u||0,i=n[1]?c+(n[1]+1)*n[2]:+n[2],r&&(r.unit=l,r.start=c,r.end=i)),i}var le={};function ce(e){var t,n=e.ownerDocument,r=e.nodeName,i=le[r];return i||(t=n.body.appendChild(n.createElement(r)),i=w.css(t,"display"),t.parentNode.removeChild(t),"none"===i&&(i="block"),le[r]=i,i)}function fe(e,t){for(var n,r,i=[],o=0,a=e.length;o<a;o++)(r=e[o]).style&&(n=r.style.display,t?("none"===n&&(i[o]=J.get(r,"display")||null,i[o]||(r.style.display="")),""===r.style.display&&ae(r)&&(i[o]=ce(r))):"none"!==n&&(i[o]="none",J.set(r,"display",n)));for(o=0;o<a;o++)null!=i[o]&&(e[o].style.display=i[o]);return e}w.fn.extend({show:function(){return fe(this,!0)},hide:function(){return fe(this)},toggle:function(e){return"boolean"==typeof e?e?this.show():this.hide():this.each(function(){ae(this)?w(this).show():w(this).hide()})}});var pe=/^(?:checkbox|radio)$/i,de=/<([a-z][^\/\0>\x20\t\r\n\f]+)/i,he=/^$|^module$|\/(?:java|ecma)script/i,ge={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};ge.optgroup=ge.option,ge.tbody=ge.tfoot=ge.colgroup=ge.caption=ge.thead,ge.th=ge.td;function ye(e,t){var n;return n="undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t||"*"):"undefined"!=typeof e.querySelectorAll?e.querySelectorAll(t||"*"):[],void 0===t||t&&N(e,t)?w.merge([e],n):n}function ve(e,t){for(var n=0,r=e.length;n<r;n++)J.set(e[n],"globalEval",!t||J.get(t[n],"globalEval"))}var me=/<|&#?\w+;/;function xe(e,t,n,r,i){for(var o,a,s,u,l,c,f=t.createDocumentFragment(),p=[],d=0,h=e.length;d<h;d++)if((o=e[d])||0===o)if("object"===x(o))w.merge(p,o.nodeType?[o]:o);else if(me.test(o)){a=a||f.appendChild(t.createElement("div")),s=(de.exec(o)||["",""])[1].toLowerCase(),u=ge[s]||ge._default,a.innerHTML=u[1]+w.htmlPrefilter(o)+u[2],c=u[0];while(c--)a=a.lastChild;w.merge(p,a.childNodes),(a=f.firstChild).textContent=""}else p.push(t.createTextNode(o));f.textContent="",d=0;while(o=p[d++])if(r&&w.inArray(o,r)>-1)i&&i.push(o);else if(l=w.contains(o.ownerDocument,o),a=ye(f.appendChild(o),"script"),l&&ve(a),n){c=0;while(o=a[c++])he.test(o.type||"")&&n.push(o)}return f}!function(){var e=r.createDocumentFragment().appendChild(r.createElement("div")),t=r.createElement("input");t.setAttribute("type","radio"),t.setAttribute("checked","checked"),t.setAttribute("name","t"),e.appendChild(t),h.checkClone=e.cloneNode(!0).cloneNode(!0).lastChild.checked,e.innerHTML="<textarea>x</textarea>",h.noCloneChecked=!!e.cloneNode(!0).lastChild.defaultValue}();var be=r.documentElement,we=/^key/,Te=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,Ce=/^([^.]*)(?:\.(.+)|)/;function Ee(){return!0}function ke(){return!1}function Se(){try{return r.activeElement}catch(e){}}function De(e,t,n,r,i,o){var a,s;if("object"==typeof t){"string"!=typeof n&&(r=r||n,n=void 0);for(s in t)De(e,s,n,r,t[s],o);return e}if(null==r&&null==i?(i=n,r=n=void 0):null==i&&("string"==typeof n?(i=r,r=void 0):(i=r,r=n,n=void 0)),!1===i)i=ke;else if(!i)return e;return 1===o&&(a=i,(i=function(e){return w().off(e),a.apply(this,arguments)}).guid=a.guid||(a.guid=w.guid++)),e.each(function(){w.event.add(this,t,i,r,n)})}w.event={global:{},add:function(e,t,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,y=J.get(e);if(y){n.handler&&(n=(o=n).handler,i=o.selector),i&&w.find.matchesSelector(be,i),n.guid||(n.guid=w.guid++),(u=y.events)||(u=y.events={}),(a=y.handle)||(a=y.handle=function(t){return"undefined"!=typeof w&&w.event.triggered!==t.type?w.event.dispatch.apply(e,arguments):void 0}),l=(t=(t||"").match(M)||[""]).length;while(l--)d=g=(s=Ce.exec(t[l])||[])[1],h=(s[2]||"").split(".").sort(),d&&(f=w.event.special[d]||{},d=(i?f.delegateType:f.bindType)||d,f=w.event.special[d]||{},c=w.extend({type:d,origType:g,data:r,handler:n,guid:n.guid,selector:i,needsContext:i&&w.expr.match.needsContext.test(i),namespace:h.join(".")},o),(p=u[d])||((p=u[d]=[]).delegateCount=0,f.setup&&!1!==f.setup.call(e,r,h,a)||e.addEventListener&&e.addEventListener(d,a)),f.add&&(f.add.call(e,c),c.handler.guid||(c.handler.guid=n.guid)),i?p.splice(p.delegateCount++,0,c):p.push(c),w.event.global[d]=!0)}},remove:function(e,t,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,y=J.hasData(e)&&J.get(e);if(y&&(u=y.events)){l=(t=(t||"").match(M)||[""]).length;while(l--)if(s=Ce.exec(t[l])||[],d=g=s[1],h=(s[2]||"").split(".").sort(),d){f=w.event.special[d]||{},p=u[d=(r?f.delegateType:f.bindType)||d]||[],s=s[2]&&new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"),a=o=p.length;while(o--)c=p[o],!i&&g!==c.origType||n&&n.guid!==c.guid||s&&!s.test(c.namespace)||r&&r!==c.selector&&("**"!==r||!c.selector)||(p.splice(o,1),c.selector&&p.delegateCount--,f.remove&&f.remove.call(e,c));a&&!p.length&&(f.teardown&&!1!==f.teardown.call(e,h,y.handle)||w.removeEvent(e,d,y.handle),delete u[d])}else for(d in u)w.event.remove(e,d+t[l],n,r,!0);w.isEmptyObject(u)&&J.remove(e,"handle events")}},dispatch:function(e){var t=w.event.fix(e),n,r,i,o,a,s,u=new Array(arguments.length),l=(J.get(this,"events")||{})[t.type]||[],c=w.event.special[t.type]||{};for(u[0]=t,n=1;n<arguments.length;n++)u[n]=arguments[n];if(t.delegateTarget=this,!c.preDispatch||!1!==c.preDispatch.call(this,t)){s=w.event.handlers.call(this,t,l),n=0;while((o=s[n++])&&!t.isPropagationStopped()){t.currentTarget=o.elem,r=0;while((a=o.handlers[r++])&&!t.isImmediatePropagationStopped())t.rnamespace&&!t.rnamespace.test(a.namespace)||(t.handleObj=a,t.data=a.data,void 0!==(i=((w.event.special[a.origType]||{}).handle||a.handler).apply(o.elem,u))&&!1===(t.result=i)&&(t.preventDefault(),t.stopPropagation()))}return c.postDispatch&&c.postDispatch.call(this,t),t.result}},handlers:function(e,t){var n,r,i,o,a,s=[],u=t.delegateCount,l=e.target;if(u&&l.nodeType&&!("click"===e.type&&e.button>=1))for(;l!==this;l=l.parentNode||this)if(1===l.nodeType&&("click"!==e.type||!0!==l.disabled)){for(o=[],a={},n=0;n<u;n++)void 0===a[i=(r=t[n]).selector+" "]&&(a[i]=r.needsContext?w(i,this).index(l)>-1:w.find(i,this,null,[l]).length),a[i]&&o.push(r);o.length&&s.push({elem:l,handlers:o})}return l=this,u<t.length&&s.push({elem:l,handlers:t.slice(u)}),s},addProp:function(e,t){Object.defineProperty(w.Event.prototype,e,{enumerable:!0,configurable:!0,get:g(t)?function(){if(this.originalEvent)return t(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[e]},set:function(t){Object.defineProperty(this,e,{enumerable:!0,configurable:!0,writable:!0,value:t})}})},fix:function(e){return e[w.expando]?e:new w.Event(e)},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==Se()&&this.focus)return this.focus(),!1},delegateType:"focusin"},blur:{trigger:function(){if(this===Se()&&this.blur)return this.blur(),!1},delegateType:"focusout"},click:{trigger:function(){if("checkbox"===this.type&&this.click&&N(this,"input"))return this.click(),!1},_default:function(e){return N(e.target,"a")}},beforeunload:{postDispatch:function(e){void 0!==e.result&&e.originalEvent&&(e.originalEvent.returnValue=e.result)}}}},w.removeEvent=function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n)},w.Event=function(e,t){if(!(this instanceof w.Event))return new w.Event(e,t);e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||void 0===e.defaultPrevented&&!1===e.returnValue?Ee:ke,this.target=e.target&&3===e.target.nodeType?e.target.parentNode:e.target,this.currentTarget=e.currentTarget,this.relatedTarget=e.relatedTarget):this.type=e,t&&w.extend(this,t),this.timeStamp=e&&e.timeStamp||Date.now(),this[w.expando]=!0},w.Event.prototype={constructor:w.Event,isDefaultPrevented:ke,isPropagationStopped:ke,isImmediatePropagationStopped:ke,isSimulated:!1,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=Ee,e&&!this.isSimulated&&e.preventDefault()},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=Ee,e&&!this.isSimulated&&e.stopPropagation()},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=Ee,e&&!this.isSimulated&&e.stopImmediatePropagation(),this.stopPropagation()}},w.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(e){var t=e.button;return null==e.which&&we.test(e.type)?null!=e.charCode?e.charCode:e.keyCode:!e.which&&void 0!==t&&Te.test(e.type)?1&t?1:2&t?3:4&t?2:0:e.which}},w.event.addProp),w.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(e,t){w.event.special[e]={delegateType:t,bindType:t,handle:function(e){var n,r=this,i=e.relatedTarget,o=e.handleObj;return i&&(i===r||w.contains(r,i))||(e.type=o.origType,n=o.handler.apply(this,arguments),e.type=t),n}}}),w.fn.extend({on:function(e,t,n,r){return De(this,e,t,n,r)},one:function(e,t,n,r){return De(this,e,t,n,r,1)},off:function(e,t,n){var r,i;if(e&&e.preventDefault&&e.handleObj)return r=e.handleObj,w(e.delegateTarget).off(r.namespace?r.origType+"."+r.namespace:r.origType,r.selector,r.handler),this;if("object"==typeof e){for(i in e)this.off(i,t,e[i]);return this}return!1!==t&&"function"!=typeof t||(n=t,t=void 0),!1===n&&(n=ke),this.each(function(){w.event.remove(this,e,n,t)})}});var Ne=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,Ae=/<script|<style|<link/i,je=/checked\s*(?:[^=]|=\s*.checked.)/i,qe=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function Le(e,t){return N(e,"table")&&N(11!==t.nodeType?t:t.firstChild,"tr")?w(e).children("tbody")[0]||e:e}function He(e){return e.type=(null!==e.getAttribute("type"))+"/"+e.type,e}function Oe(e){return"true/"===(e.type||"").slice(0,5)?e.type=e.type.slice(5):e.removeAttribute("type"),e}function Pe(e,t){var n,r,i,o,a,s,u,l;if(1===t.nodeType){if(J.hasData(e)&&(o=J.access(e),a=J.set(t,o),l=o.events)){delete a.handle,a.events={};for(i in l)for(n=0,r=l[i].length;n<r;n++)w.event.add(t,i,l[i][n])}K.hasData(e)&&(s=K.access(e),u=w.extend({},s),K.set(t,u))}}function Me(e,t){var n=t.nodeName.toLowerCase();"input"===n&&pe.test(e.type)?t.checked=e.checked:"input"!==n&&"textarea"!==n||(t.defaultValue=e.defaultValue)}function Re(e,t,n,r){t=a.apply([],t);var i,o,s,u,l,c,f=0,p=e.length,d=p-1,y=t[0],v=g(y);if(v||p>1&&"string"==typeof y&&!h.checkClone&&je.test(y))return e.each(function(i){var o=e.eq(i);v&&(t[0]=y.call(this,i,o.html())),Re(o,t,n,r)});if(p&&(i=xe(t,e[0].ownerDocument,!1,e,r),o=i.firstChild,1===i.childNodes.length&&(i=o),o||r)){for(u=(s=w.map(ye(i,"script"),He)).length;f<p;f++)l=i,f!==d&&(l=w.clone(l,!0,!0),u&&w.merge(s,ye(l,"script"))),n.call(e[f],l,f);if(u)for(c=s[s.length-1].ownerDocument,w.map(s,Oe),f=0;f<u;f++)l=s[f],he.test(l.type||"")&&!J.access(l,"globalEval")&&w.contains(c,l)&&(l.src&&"module"!==(l.type||"").toLowerCase()?w._evalUrl&&w._evalUrl(l.src):m(l.textContent.replace(qe,""),c,l))}return e}function Ie(e,t,n){for(var r,i=t?w.filter(t,e):e,o=0;null!=(r=i[o]);o++)n||1!==r.nodeType||w.cleanData(ye(r)),r.parentNode&&(n&&w.contains(r.ownerDocument,r)&&ve(ye(r,"script")),r.parentNode.removeChild(r));return e}w.extend({htmlPrefilter:function(e){return e.replace(Ne,"<$1></$2>")},clone:function(e,t,n){var r,i,o,a,s=e.cloneNode(!0),u=w.contains(e.ownerDocument,e);if(!(h.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||w.isXMLDoc(e)))for(a=ye(s),r=0,i=(o=ye(e)).length;r<i;r++)Me(o[r],a[r]);if(t)if(n)for(o=o||ye(e),a=a||ye(s),r=0,i=o.length;r<i;r++)Pe(o[r],a[r]);else Pe(e,s);return(a=ye(s,"script")).length>0&&ve(a,!u&&ye(e,"script")),s},cleanData:function(e){for(var t,n,r,i=w.event.special,o=0;void 0!==(n=e[o]);o++)if(Y(n)){if(t=n[J.expando]){if(t.events)for(r in t.events)i[r]?w.event.remove(n,r):w.removeEvent(n,r,t.handle);n[J.expando]=void 0}n[K.expando]&&(n[K.expando]=void 0)}}}),w.fn.extend({detach:function(e){return Ie(this,e,!0)},remove:function(e){return Ie(this,e)},text:function(e){return z(this,function(e){return void 0===e?w.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=e)})},null,e,arguments.length)},append:function(){return Re(this,arguments,function(e){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||Le(this,e).appendChild(e)})},prepend:function(){return Re(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=Le(this,e);t.insertBefore(e,t.firstChild)}})},before:function(){return Re(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return Re(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},empty:function(){for(var e,t=0;null!=(e=this[t]);t++)1===e.nodeType&&(w.cleanData(ye(e,!1)),e.textContent="");return this},clone:function(e,t){return e=null!=e&&e,t=null==t?e:t,this.map(function(){return w.clone(this,e,t)})},html:function(e){return z(this,function(e){var t=this[0]||{},n=0,r=this.length;if(void 0===e&&1===t.nodeType)return t.innerHTML;if("string"==typeof e&&!Ae.test(e)&&!ge[(de.exec(e)||["",""])[1].toLowerCase()]){e=w.htmlPrefilter(e);try{for(;n<r;n++)1===(t=this[n]||{}).nodeType&&(w.cleanData(ye(t,!1)),t.innerHTML=e);t=0}catch(e){}}t&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(){var e=[];return Re(this,arguments,function(t){var n=this.parentNode;w.inArray(this,e)<0&&(w.cleanData(ye(this)),n&&n.replaceChild(t,this))},e)}}),w.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,t){w.fn[e]=function(e){for(var n,r=[],i=w(e),o=i.length-1,a=0;a<=o;a++)n=a===o?this:this.clone(!0),w(i[a])[t](n),s.apply(r,n.get());return this.pushStack(r)}});var We=new RegExp("^("+re+")(?!px)[a-z%]+$","i"),$e=function(t){var n=t.ownerDocument.defaultView;return n&&n.opener||(n=e),n.getComputedStyle(t)},Be=new RegExp(oe.join("|"),"i");!function(){function t(){if(c){l.style.cssText="position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",c.style.cssText="position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",be.appendChild(l).appendChild(c);var t=e.getComputedStyle(c);i="1%"!==t.top,u=12===n(t.marginLeft),c.style.right="60%",s=36===n(t.right),o=36===n(t.width),c.style.position="absolute",a=36===c.offsetWidth||"absolute",be.removeChild(l),c=null}}function n(e){return Math.round(parseFloat(e))}var i,o,a,s,u,l=r.createElement("div"),c=r.createElement("div");c.style&&(c.style.backgroundClip="content-box",c.cloneNode(!0).style.backgroundClip="",h.clearCloneStyle="content-box"===c.style.backgroundClip,w.extend(h,{boxSizingReliable:function(){return t(),o},pixelBoxStyles:function(){return t(),s},pixelPosition:function(){return t(),i},reliableMarginLeft:function(){return t(),u},scrollboxSize:function(){return t(),a}}))}();function Fe(e,t,n){var r,i,o,a,s=e.style;return(n=n||$e(e))&&(""!==(a=n.getPropertyValue(t)||n[t])||w.contains(e.ownerDocument,e)||(a=w.style(e,t)),!h.pixelBoxStyles()&&We.test(a)&&Be.test(t)&&(r=s.width,i=s.minWidth,o=s.maxWidth,s.minWidth=s.maxWidth=s.width=a,a=n.width,s.width=r,s.minWidth=i,s.maxWidth=o)),void 0!==a?a+"":a}function _e(e,t){return{get:function(){if(!e())return(this.get=t).apply(this,arguments);delete this.get}}}var ze=/^(none|table(?!-c[ea]).+)/,Xe=/^--/,Ue={position:"absolute",visibility:"hidden",display:"block"},Ve={letterSpacing:"0",fontWeight:"400"},Ge=["Webkit","Moz","ms"],Ye=r.createElement("div").style;function Qe(e){if(e in Ye)return e;var t=e[0].toUpperCase()+e.slice(1),n=Ge.length;while(n--)if((e=Ge[n]+t)in Ye)return e}function Je(e){var t=w.cssProps[e];return t||(t=w.cssProps[e]=Qe(e)||e),t}function Ke(e,t,n){var r=ie.exec(t);return r?Math.max(0,r[2]-(n||0))+(r[3]||"px"):t}function Ze(e,t,n,r,i,o){var a="width"===t?1:0,s=0,u=0;if(n===(r?"border":"content"))return 0;for(;a<4;a+=2)"margin"===n&&(u+=w.css(e,n+oe[a],!0,i)),r?("content"===n&&(u-=w.css(e,"padding"+oe[a],!0,i)),"margin"!==n&&(u-=w.css(e,"border"+oe[a]+"Width",!0,i))):(u+=w.css(e,"padding"+oe[a],!0,i),"padding"!==n?u+=w.css(e,"border"+oe[a]+"Width",!0,i):s+=w.css(e,"border"+oe[a]+"Width",!0,i));return!r&&o>=0&&(u+=Math.max(0,Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-o-u-s-.5))),u}function et(e,t,n){var r=$e(e),i=Fe(e,t,r),o="border-box"===w.css(e,"boxSizing",!1,r),a=o;if(We.test(i)){if(!n)return i;i="auto"}return a=a&&(h.boxSizingReliable()||i===e.style[t]),("auto"===i||!parseFloat(i)&&"inline"===w.css(e,"display",!1,r))&&(i=e["offset"+t[0].toUpperCase()+t.slice(1)],a=!0),(i=parseFloat(i)||0)+Ze(e,t,n||(o?"border":"content"),a,r,i)+"px"}w.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=Fe(e,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{},style:function(e,t,n,r){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var i,o,a,s=G(t),u=Xe.test(t),l=e.style;if(u||(t=Je(s)),a=w.cssHooks[t]||w.cssHooks[s],void 0===n)return a&&"get"in a&&void 0!==(i=a.get(e,!1,r))?i:l[t];"string"==(o=typeof n)&&(i=ie.exec(n))&&i[1]&&(n=ue(e,t,i),o="number"),null!=n&&n===n&&("number"===o&&(n+=i&&i[3]||(w.cssNumber[s]?"":"px")),h.clearCloneStyle||""!==n||0!==t.indexOf("background")||(l[t]="inherit"),a&&"set"in a&&void 0===(n=a.set(e,n,r))||(u?l.setProperty(t,n):l[t]=n))}},css:function(e,t,n,r){var i,o,a,s=G(t);return Xe.test(t)||(t=Je(s)),(a=w.cssHooks[t]||w.cssHooks[s])&&"get"in a&&(i=a.get(e,!0,n)),void 0===i&&(i=Fe(e,t,r)),"normal"===i&&t in Ve&&(i=Ve[t]),""===n||n?(o=parseFloat(i),!0===n||isFinite(o)?o||0:i):i}}),w.each(["height","width"],function(e,t){w.cssHooks[t]={get:function(e,n,r){if(n)return!ze.test(w.css(e,"display"))||e.getClientRects().length&&e.getBoundingClientRect().width?et(e,t,r):se(e,Ue,function(){return et(e,t,r)})},set:function(e,n,r){var i,o=$e(e),a="border-box"===w.css(e,"boxSizing",!1,o),s=r&&Ze(e,t,r,a,o);return a&&h.scrollboxSize()===o.position&&(s-=Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-parseFloat(o[t])-Ze(e,t,"border",!1,o)-.5)),s&&(i=ie.exec(n))&&"px"!==(i[3]||"px")&&(e.style[t]=n,n=w.css(e,t)),Ke(e,n,s)}}}),w.cssHooks.marginLeft=_e(h.reliableMarginLeft,function(e,t){if(t)return(parseFloat(Fe(e,"marginLeft"))||e.getBoundingClientRect().left-se(e,{marginLeft:0},function(){return e.getBoundingClientRect().left}))+"px"}),w.each({margin:"",padding:"",border:"Width"},function(e,t){w.cssHooks[e+t]={expand:function(n){for(var r=0,i={},o="string"==typeof n?n.split(" "):[n];r<4;r++)i[e+oe[r]+t]=o[r]||o[r-2]||o[0];return i}},"margin"!==e&&(w.cssHooks[e+t].set=Ke)}),w.fn.extend({css:function(e,t){return z(this,function(e,t,n){var r,i,o={},a=0;if(Array.isArray(t)){for(r=$e(e),i=t.length;a<i;a++)o[t[a]]=w.css(e,t[a],!1,r);return o}return void 0!==n?w.style(e,t,n):w.css(e,t)},e,t,arguments.length>1)}});function tt(e,t,n,r,i){return new tt.prototype.init(e,t,n,r,i)}w.Tween=tt,tt.prototype={constructor:tt,init:function(e,t,n,r,i,o){this.elem=e,this.prop=n,this.easing=i||w.easing._default,this.options=t,this.start=this.now=this.cur(),this.end=r,this.unit=o||(w.cssNumber[n]?"":"px")},cur:function(){var e=tt.propHooks[this.prop];return e&&e.get?e.get(this):tt.propHooks._default.get(this)},run:function(e){var t,n=tt.propHooks[this.prop];return this.options.duration?this.pos=t=w.easing[this.easing](e,this.options.duration*e,0,1,this.options.duration):this.pos=t=e,this.now=(this.end-this.start)*t+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),n&&n.set?n.set(this):tt.propHooks._default.set(this),this}},tt.prototype.init.prototype=tt.prototype,tt.propHooks={_default:{get:function(e){var t;return 1!==e.elem.nodeType||null!=e.elem[e.prop]&&null==e.elem.style[e.prop]?e.elem[e.prop]:(t=w.css(e.elem,e.prop,""))&&"auto"!==t?t:0},set:function(e){w.fx.step[e.prop]?w.fx.step[e.prop](e):1!==e.elem.nodeType||null==e.elem.style[w.cssProps[e.prop]]&&!w.cssHooks[e.prop]?e.elem[e.prop]=e.now:w.style(e.elem,e.prop,e.now+e.unit)}}},tt.propHooks.scrollTop=tt.propHooks.scrollLeft={set:function(e){e.elem.nodeType&&e.elem.parentNode&&(e.elem[e.prop]=e.now)}},w.easing={linear:function(e){return e},swing:function(e){return.5-Math.cos(e*Math.PI)/2},_default:"swing"},w.fx=tt.prototype.init,w.fx.step={};var nt,rt,it=/^(?:toggle|show|hide)$/,ot=/queueHooks$/;function at(){rt&&(!1===r.hidden&&e.requestAnimationFrame?e.requestAnimationFrame(at):e.setTimeout(at,w.fx.interval),w.fx.tick())}function st(){return e.setTimeout(function(){nt=void 0}),nt=Date.now()}function ut(e,t){var n,r=0,i={height:e};for(t=t?1:0;r<4;r+=2-t)i["margin"+(n=oe[r])]=i["padding"+n]=e;return t&&(i.opacity=i.width=e),i}function lt(e,t,n){for(var r,i=(pt.tweeners[t]||[]).concat(pt.tweeners["*"]),o=0,a=i.length;o<a;o++)if(r=i[o].call(n,t,e))return r}function ct(e,t,n){var r,i,o,a,s,u,l,c,f="width"in t||"height"in t,p=this,d={},h=e.style,g=e.nodeType&&ae(e),y=J.get(e,"fxshow");n.queue||(null==(a=w._queueHooks(e,"fx")).unqueued&&(a.unqueued=0,s=a.empty.fire,a.empty.fire=function(){a.unqueued||s()}),a.unqueued++,p.always(function(){p.always(function(){a.unqueued--,w.queue(e,"fx").length||a.empty.fire()})}));for(r in t)if(i=t[r],it.test(i)){if(delete t[r],o=o||"toggle"===i,i===(g?"hide":"show")){if("show"!==i||!y||void 0===y[r])continue;g=!0}d[r]=y&&y[r]||w.style(e,r)}if((u=!w.isEmptyObject(t))||!w.isEmptyObject(d)){f&&1===e.nodeType&&(n.overflow=[h.overflow,h.overflowX,h.overflowY],null==(l=y&&y.display)&&(l=J.get(e,"display")),"none"===(c=w.css(e,"display"))&&(l?c=l:(fe([e],!0),l=e.style.display||l,c=w.css(e,"display"),fe([e]))),("inline"===c||"inline-block"===c&&null!=l)&&"none"===w.css(e,"float")&&(u||(p.done(function(){h.display=l}),null==l&&(c=h.display,l="none"===c?"":c)),h.display="inline-block")),n.overflow&&(h.overflow="hidden",p.always(function(){h.overflow=n.overflow[0],h.overflowX=n.overflow[1],h.overflowY=n.overflow[2]})),u=!1;for(r in d)u||(y?"hidden"in y&&(g=y.hidden):y=J.access(e,"fxshow",{display:l}),o&&(y.hidden=!g),g&&fe([e],!0),p.done(function(){g||fe([e]),J.remove(e,"fxshow");for(r in d)w.style(e,r,d[r])})),u=lt(g?y[r]:0,r,p),r in y||(y[r]=u.start,g&&(u.end=u.start,u.start=0))}}function ft(e,t){var n,r,i,o,a;for(n in e)if(r=G(n),i=t[r],o=e[n],Array.isArray(o)&&(i=o[1],o=e[n]=o[0]),n!==r&&(e[r]=o,delete e[n]),(a=w.cssHooks[r])&&"expand"in a){o=a.expand(o),delete e[r];for(n in o)n in e||(e[n]=o[n],t[n]=i)}else t[r]=i}function pt(e,t,n){var r,i,o=0,a=pt.prefilters.length,s=w.Deferred().always(function(){delete u.elem}),u=function(){if(i)return!1;for(var t=nt||st(),n=Math.max(0,l.startTime+l.duration-t),r=1-(n/l.duration||0),o=0,a=l.tweens.length;o<a;o++)l.tweens[o].run(r);return s.notifyWith(e,[l,r,n]),r<1&&a?n:(a||s.notifyWith(e,[l,1,0]),s.resolveWith(e,[l]),!1)},l=s.promise({elem:e,props:w.extend({},t),opts:w.extend(!0,{specialEasing:{},easing:w.easing._default},n),originalProperties:t,originalOptions:n,startTime:nt||st(),duration:n.duration,tweens:[],createTween:function(t,n){var r=w.Tween(e,l.opts,t,n,l.opts.specialEasing[t]||l.opts.easing);return l.tweens.push(r),r},stop:function(t){var n=0,r=t?l.tweens.length:0;if(i)return this;for(i=!0;n<r;n++)l.tweens[n].run(1);return t?(s.notifyWith(e,[l,1,0]),s.resolveWith(e,[l,t])):s.rejectWith(e,[l,t]),this}}),c=l.props;for(ft(c,l.opts.specialEasing);o<a;o++)if(r=pt.prefilters[o].call(l,e,c,l.opts))return g(r.stop)&&(w._queueHooks(l.elem,l.opts.queue).stop=r.stop.bind(r)),r;return w.map(c,lt,l),g(l.opts.start)&&l.opts.start.call(e,l),l.progress(l.opts.progress).done(l.opts.done,l.opts.complete).fail(l.opts.fail).always(l.opts.always),w.fx.timer(w.extend(u,{elem:e,anim:l,queue:l.opts.queue})),l}w.Animation=w.extend(pt,{tweeners:{"*":[function(e,t){var n=this.createTween(e,t);return ue(n.elem,e,ie.exec(t),n),n}]},tweener:function(e,t){g(e)?(t=e,e=["*"]):e=e.match(M);for(var n,r=0,i=e.length;r<i;r++)n=e[r],pt.tweeners[n]=pt.tweeners[n]||[],pt.tweeners[n].unshift(t)},prefilters:[ct],prefilter:function(e,t){t?pt.prefilters.unshift(e):pt.prefilters.push(e)}}),w.speed=function(e,t,n){var r=e&&"object"==typeof e?w.extend({},e):{complete:n||!n&&t||g(e)&&e,duration:e,easing:n&&t||t&&!g(t)&&t};return w.fx.off?r.duration=0:"number"!=typeof r.duration&&(r.duration in w.fx.speeds?r.duration=w.fx.speeds[r.duration]:r.duration=w.fx.speeds._default),null!=r.queue&&!0!==r.queue||(r.queue="fx"),r.old=r.complete,r.complete=function(){g(r.old)&&r.old.call(this),r.queue&&w.dequeue(this,r.queue)},r},w.fn.extend({fadeTo:function(e,t,n,r){return this.filter(ae).css("opacity",0).show().end().animate({opacity:t},e,n,r)},animate:function(e,t,n,r){var i=w.isEmptyObject(e),o=w.speed(t,n,r),a=function(){var t=pt(this,w.extend({},e),o);(i||J.get(this,"finish"))&&t.stop(!0)};return a.finish=a,i||!1===o.queue?this.each(a):this.queue(o.queue,a)},stop:function(e,t,n){var r=function(e){var t=e.stop;delete e.stop,t(n)};return"string"!=typeof e&&(n=t,t=e,e=void 0),t&&!1!==e&&this.queue(e||"fx",[]),this.each(function(){var t=!0,i=null!=e&&e+"queueHooks",o=w.timers,a=J.get(this);if(i)a[i]&&a[i].stop&&r(a[i]);else for(i in a)a[i]&&a[i].stop&&ot.test(i)&&r(a[i]);for(i=o.length;i--;)o[i].elem!==this||null!=e&&o[i].queue!==e||(o[i].anim.stop(n),t=!1,o.splice(i,1));!t&&n||w.dequeue(this,e)})},finish:function(e){return!1!==e&&(e=e||"fx"),this.each(function(){var t,n=J.get(this),r=n[e+"queue"],i=n[e+"queueHooks"],o=w.timers,a=r?r.length:0;for(n.finish=!0,w.queue(this,e,[]),i&&i.stop&&i.stop.call(this,!0),t=o.length;t--;)o[t].elem===this&&o[t].queue===e&&(o[t].anim.stop(!0),o.splice(t,1));for(t=0;t<a;t++)r[t]&&r[t].finish&&r[t].finish.call(this);delete n.finish})}}),w.each(["toggle","show","hide"],function(e,t){var n=w.fn[t];w.fn[t]=function(e,r,i){return null==e||"boolean"==typeof e?n.apply(this,arguments):this.animate(ut(t,!0),e,r,i)}}),w.each({slideDown:ut("show"),slideUp:ut("hide"),slideToggle:ut("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(e,t){w.fn[e]=function(e,n,r){return this.animate(t,e,n,r)}}),w.timers=[],w.fx.tick=function(){var e,t=0,n=w.timers;for(nt=Date.now();t<n.length;t++)(e=n[t])()||n[t]!==e||n.splice(t--,1);n.length||w.fx.stop(),nt=void 0},w.fx.timer=function(e){w.timers.push(e),w.fx.start()},w.fx.interval=13,w.fx.start=function(){rt||(rt=!0,at())},w.fx.stop=function(){rt=null},w.fx.speeds={slow:600,fast:200,_default:400},w.fn.delay=function(t,n){return t=w.fx?w.fx.speeds[t]||t:t,n=n||"fx",this.queue(n,function(n,r){var i=e.setTimeout(n,t);r.stop=function(){e.clearTimeout(i)}})},function(){var e=r.createElement("input"),t=r.createElement("select").appendChild(r.createElement("option"));e.type="checkbox",h.checkOn=""!==e.value,h.optSelected=t.selected,(e=r.createElement("input")).value="t",e.type="radio",h.radioValue="t"===e.value}();var dt,ht=w.expr.attrHandle;w.fn.extend({attr:function(e,t){return z(this,w.attr,e,t,arguments.length>1)},removeAttr:function(e){return this.each(function(){w.removeAttr(this,e)})}}),w.extend({attr:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return"undefined"==typeof e.getAttribute?w.prop(e,t,n):(1===o&&w.isXMLDoc(e)||(i=w.attrHooks[t.toLowerCase()]||(w.expr.match.bool.test(t)?dt:void 0)),void 0!==n?null===n?void w.removeAttr(e,t):i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:(e.setAttribute(t,n+""),n):i&&"get"in i&&null!==(r=i.get(e,t))?r:null==(r=w.find.attr(e,t))?void 0:r)},attrHooks:{type:{set:function(e,t){if(!h.radioValue&&"radio"===t&&N(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},removeAttr:function(e,t){var n,r=0,i=t&&t.match(M);if(i&&1===e.nodeType)while(n=i[r++])e.removeAttribute(n)}}),dt={set:function(e,t,n){return!1===t?w.removeAttr(e,n):e.setAttribute(n,n),n}},w.each(w.expr.match.bool.source.match(/\w+/g),function(e,t){var n=ht[t]||w.find.attr;ht[t]=function(e,t,r){var i,o,a=t.toLowerCase();return r||(o=ht[a],ht[a]=i,i=null!=n(e,t,r)?a:null,ht[a]=o),i}});var gt=/^(?:input|select|textarea|button)$/i,yt=/^(?:a|area)$/i;w.fn.extend({prop:function(e,t){return z(this,w.prop,e,t,arguments.length>1)},removeProp:function(e){return this.each(function(){delete this[w.propFix[e]||e]})}}),w.extend({prop:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return 1===o&&w.isXMLDoc(e)||(t=w.propFix[t]||t,i=w.propHooks[t]),void 0!==n?i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:e[t]=n:i&&"get"in i&&null!==(r=i.get(e,t))?r:e[t]},propHooks:{tabIndex:{get:function(e){var t=w.find.attr(e,"tabindex");return t?parseInt(t,10):gt.test(e.nodeName)||yt.test(e.nodeName)&&e.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),h.optSelected||(w.propHooks.selected={get:function(e){var t=e.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(e){var t=e.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}}),w.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){w.propFix[this.toLowerCase()]=this});function vt(e){return(e.match(M)||[]).join(" ")}function mt(e){return e.getAttribute&&e.getAttribute("class")||""}function xt(e){return Array.isArray(e)?e:"string"==typeof e?e.match(M)||[]:[]}w.fn.extend({addClass:function(e){var t,n,r,i,o,a,s,u=0;if(g(e))return this.each(function(t){w(this).addClass(e.call(this,t,mt(this)))});if((t=xt(e)).length)while(n=this[u++])if(i=mt(n),r=1===n.nodeType&&" "+vt(i)+" "){a=0;while(o=t[a++])r.indexOf(" "+o+" ")<0&&(r+=o+" ");i!==(s=vt(r))&&n.setAttribute("class",s)}return this},removeClass:function(e){var t,n,r,i,o,a,s,u=0;if(g(e))return this.each(function(t){w(this).removeClass(e.call(this,t,mt(this)))});if(!arguments.length)return this.attr("class","");if((t=xt(e)).length)while(n=this[u++])if(i=mt(n),r=1===n.nodeType&&" "+vt(i)+" "){a=0;while(o=t[a++])while(r.indexOf(" "+o+" ")>-1)r=r.replace(" "+o+" "," ");i!==(s=vt(r))&&n.setAttribute("class",s)}return this},toggleClass:function(e,t){var n=typeof e,r="string"===n||Array.isArray(e);return"boolean"==typeof t&&r?t?this.addClass(e):this.removeClass(e):g(e)?this.each(function(n){w(this).toggleClass(e.call(this,n,mt(this),t),t)}):this.each(function(){var t,i,o,a;if(r){i=0,o=w(this),a=xt(e);while(t=a[i++])o.hasClass(t)?o.removeClass(t):o.addClass(t)}else void 0!==e&&"boolean"!==n||((t=mt(this))&&J.set(this,"__className__",t),this.setAttribute&&this.setAttribute("class",t||!1===e?"":J.get(this,"__className__")||""))})},hasClass:function(e){var t,n,r=0;t=" "+e+" ";while(n=this[r++])if(1===n.nodeType&&(" "+vt(mt(n))+" ").indexOf(t)>-1)return!0;return!1}});var bt=/\r/g;w.fn.extend({val:function(e){var t,n,r,i=this[0];{if(arguments.length)return r=g(e),this.each(function(n){var i;1===this.nodeType&&(null==(i=r?e.call(this,n,w(this).val()):e)?i="":"number"==typeof i?i+="":Array.isArray(i)&&(i=w.map(i,function(e){return null==e?"":e+""})),(t=w.valHooks[this.type]||w.valHooks[this.nodeName.toLowerCase()])&&"set"in t&&void 0!==t.set(this,i,"value")||(this.value=i))});if(i)return(t=w.valHooks[i.type]||w.valHooks[i.nodeName.toLowerCase()])&&"get"in t&&void 0!==(n=t.get(i,"value"))?n:"string"==typeof(n=i.value)?n.replace(bt,""):null==n?"":n}}}),w.extend({valHooks:{option:{get:function(e){var t=w.find.attr(e,"value");return null!=t?t:vt(w.text(e))}},select:{get:function(e){var t,n,r,i=e.options,o=e.selectedIndex,a="select-one"===e.type,s=a?null:[],u=a?o+1:i.length;for(r=o<0?u:a?o:0;r<u;r++)if(((n=i[r]).selected||r===o)&&!n.disabled&&(!n.parentNode.disabled||!N(n.parentNode,"optgroup"))){if(t=w(n).val(),a)return t;s.push(t)}return s},set:function(e,t){var n,r,i=e.options,o=w.makeArray(t),a=i.length;while(a--)((r=i[a]).selected=w.inArray(w.valHooks.option.get(r),o)>-1)&&(n=!0);return n||(e.selectedIndex=-1),o}}}}),w.each(["radio","checkbox"],function(){w.valHooks[this]={set:function(e,t){if(Array.isArray(t))return e.checked=w.inArray(w(e).val(),t)>-1}},h.checkOn||(w.valHooks[this].get=function(e){return null===e.getAttribute("value")?"on":e.value})}),h.focusin="onfocusin"in e;var wt=/^(?:focusinfocus|focusoutblur)$/,Tt=function(e){e.stopPropagation()};w.extend(w.event,{trigger:function(t,n,i,o){var a,s,u,l,c,p,d,h,v=[i||r],m=f.call(t,"type")?t.type:t,x=f.call(t,"namespace")?t.namespace.split("."):[];if(s=h=u=i=i||r,3!==i.nodeType&&8!==i.nodeType&&!wt.test(m+w.event.triggered)&&(m.indexOf(".")>-1&&(m=(x=m.split(".")).shift(),x.sort()),c=m.indexOf(":")<0&&"on"+m,t=t[w.expando]?t:new w.Event(m,"object"==typeof t&&t),t.isTrigger=o?2:3,t.namespace=x.join("."),t.rnamespace=t.namespace?new RegExp("(^|\\.)"+x.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,t.result=void 0,t.target||(t.target=i),n=null==n?[t]:w.makeArray(n,[t]),d=w.event.special[m]||{},o||!d.trigger||!1!==d.trigger.apply(i,n))){if(!o&&!d.noBubble&&!y(i)){for(l=d.delegateType||m,wt.test(l+m)||(s=s.parentNode);s;s=s.parentNode)v.push(s),u=s;u===(i.ownerDocument||r)&&v.push(u.defaultView||u.parentWindow||e)}a=0;while((s=v[a++])&&!t.isPropagationStopped())h=s,t.type=a>1?l:d.bindType||m,(p=(J.get(s,"events")||{})[t.type]&&J.get(s,"handle"))&&p.apply(s,n),(p=c&&s[c])&&p.apply&&Y(s)&&(t.result=p.apply(s,n),!1===t.result&&t.preventDefault());return t.type=m,o||t.isDefaultPrevented()||d._default&&!1!==d._default.apply(v.pop(),n)||!Y(i)||c&&g(i[m])&&!y(i)&&((u=i[c])&&(i[c]=null),w.event.triggered=m,t.isPropagationStopped()&&h.addEventListener(m,Tt),i[m](),t.isPropagationStopped()&&h.removeEventListener(m,Tt),w.event.triggered=void 0,u&&(i[c]=u)),t.result}},simulate:function(e,t,n){var r=w.extend(new w.Event,n,{type:e,isSimulated:!0});w.event.trigger(r,null,t)}}),w.fn.extend({trigger:function(e,t){return this.each(function(){w.event.trigger(e,t,this)})},triggerHandler:function(e,t){var n=this[0];if(n)return w.event.trigger(e,t,n,!0)}}),h.focusin||w.each({focus:"focusin",blur:"focusout"},function(e,t){var n=function(e){w.event.simulate(t,e.target,w.event.fix(e))};w.event.special[t]={setup:function(){var r=this.ownerDocument||this,i=J.access(r,t);i||r.addEventListener(e,n,!0),J.access(r,t,(i||0)+1)},teardown:function(){var r=this.ownerDocument||this,i=J.access(r,t)-1;i?J.access(r,t,i):(r.removeEventListener(e,n,!0),J.remove(r,t))}}});var Ct=e.location,Et=Date.now(),kt=/\?/;w.parseXML=function(t){var n;if(!t||"string"!=typeof t)return null;try{n=(new e.DOMParser).parseFromString(t,"text/xml")}catch(e){n=void 0}return n&&!n.getElementsByTagName("parsererror").length||w.error("Invalid XML: "+t),n};var St=/\[\]$/,Dt=/\r?\n/g,Nt=/^(?:submit|button|image|reset|file)$/i,At=/^(?:input|select|textarea|keygen)/i;function jt(e,t,n,r){var i;if(Array.isArray(t))w.each(t,function(t,i){n||St.test(e)?r(e,i):jt(e+"["+("object"==typeof i&&null!=i?t:"")+"]",i,n,r)});else if(n||"object"!==x(t))r(e,t);else for(i in t)jt(e+"["+i+"]",t[i],n,r)}w.param=function(e,t){var n,r=[],i=function(e,t){var n=g(t)?t():t;r[r.length]=encodeURIComponent(e)+"="+encodeURIComponent(null==n?"":n)};if(Array.isArray(e)||e.jquery&&!w.isPlainObject(e))w.each(e,function(){i(this.name,this.value)});else for(n in e)jt(n,e[n],t,i);return r.join("&")},w.fn.extend({serialize:function(){return w.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=w.prop(this,"elements");return e?w.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!w(this).is(":disabled")&&At.test(this.nodeName)&&!Nt.test(e)&&(this.checked||!pe.test(e))}).map(function(e,t){var n=w(this).val();return null==n?null:Array.isArray(n)?w.map(n,function(e){return{name:t.name,value:e.replace(Dt,"\r\n")}}):{name:t.name,value:n.replace(Dt,"\r\n")}}).get()}});var qt=/%20/g,Lt=/#.*$/,Ht=/([?&])_=[^&]*/,Ot=/^(.*?):[ \t]*([^\r\n]*)$/gm,Pt=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Mt=/^(?:GET|HEAD)$/,Rt=/^\/\//,It={},Wt={},$t="*/".concat("*"),Bt=r.createElement("a");Bt.href=Ct.href;function Ft(e){return function(t,n){"string"!=typeof t&&(n=t,t="*");var r,i=0,o=t.toLowerCase().match(M)||[];if(g(n))while(r=o[i++])"+"===r[0]?(r=r.slice(1)||"*",(e[r]=e[r]||[]).unshift(n)):(e[r]=e[r]||[]).push(n)}}function _t(e,t,n,r){var i={},o=e===Wt;function a(s){var u;return i[s]=!0,w.each(e[s]||[],function(e,s){var l=s(t,n,r);return"string"!=typeof l||o||i[l]?o?!(u=l):void 0:(t.dataTypes.unshift(l),a(l),!1)}),u}return a(t.dataTypes[0])||!i["*"]&&a("*")}function zt(e,t){var n,r,i=w.ajaxSettings.flatOptions||{};for(n in t)void 0!==t[n]&&((i[n]?e:r||(r={}))[n]=t[n]);return r&&w.extend(!0,e,r),e}function Xt(e,t,n){var r,i,o,a,s=e.contents,u=e.dataTypes;while("*"===u[0])u.shift(),void 0===r&&(r=e.mimeType||t.getResponseHeader("Content-Type"));if(r)for(i in s)if(s[i]&&s[i].test(r)){u.unshift(i);break}if(u[0]in n)o=u[0];else{for(i in n){if(!u[0]||e.converters[i+" "+u[0]]){o=i;break}a||(a=i)}o=o||a}if(o)return o!==u[0]&&u.unshift(o),n[o]}function Ut(e,t,n,r){var i,o,a,s,u,l={},c=e.dataTypes.slice();if(c[1])for(a in e.converters)l[a.toLowerCase()]=e.converters[a];o=c.shift();while(o)if(e.responseFields[o]&&(n[e.responseFields[o]]=t),!u&&r&&e.dataFilter&&(t=e.dataFilter(t,e.dataType)),u=o,o=c.shift())if("*"===o)o=u;else if("*"!==u&&u!==o){if(!(a=l[u+" "+o]||l["* "+o]))for(i in l)if((s=i.split(" "))[1]===o&&(a=l[u+" "+s[0]]||l["* "+s[0]])){!0===a?a=l[i]:!0!==l[i]&&(o=s[0],c.unshift(s[1]));break}if(!0!==a)if(a&&e["throws"])t=a(t);else try{t=a(t)}catch(e){return{state:"parsererror",error:a?e:"No conversion from "+u+" to "+o}}}return{state:"success",data:t}}w.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Ct.href,type:"GET",isLocal:Pt.test(Ct.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":$t,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":w.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(e,t){return t?zt(zt(e,w.ajaxSettings),t):zt(w.ajaxSettings,e)},ajaxPrefilter:Ft(It),ajaxTransport:Ft(Wt),ajax:function(t,n){"object"==typeof t&&(n=t,t=void 0),n=n||{};var i,o,a,s,u,l,c,f,p,d,h=w.ajaxSetup({},n),g=h.context||h,y=h.context&&(g.nodeType||g.jquery)?w(g):w.event,v=w.Deferred(),m=w.Callbacks("once memory"),x=h.statusCode||{},b={},T={},C="canceled",E={readyState:0,getResponseHeader:function(e){var t;if(c){if(!s){s={};while(t=Ot.exec(a))s[t[1].toLowerCase()]=t[2]}t=s[e.toLowerCase()]}return null==t?null:t},getAllResponseHeaders:function(){return c?a:null},setRequestHeader:function(e,t){return null==c&&(e=T[e.toLowerCase()]=T[e.toLowerCase()]||e,b[e]=t),this},overrideMimeType:function(e){return null==c&&(h.mimeType=e),this},statusCode:function(e){var t;if(e)if(c)E.always(e[E.status]);else for(t in e)x[t]=[x[t],e[t]];return this},abort:function(e){var t=e||C;return i&&i.abort(t),k(0,t),this}};if(v.promise(E),h.url=((t||h.url||Ct.href)+"").replace(Rt,Ct.protocol+"//"),h.type=n.method||n.type||h.method||h.type,h.dataTypes=(h.dataType||"*").toLowerCase().match(M)||[""],null==h.crossDomain){l=r.createElement("a");try{l.href=h.url,l.href=l.href,h.crossDomain=Bt.protocol+"//"+Bt.host!=l.protocol+"//"+l.host}catch(e){h.crossDomain=!0}}if(h.data&&h.processData&&"string"!=typeof h.data&&(h.data=w.param(h.data,h.traditional)),_t(It,h,n,E),c)return E;(f=w.event&&h.global)&&0==w.active++&&w.event.trigger("ajaxStart"),h.type=h.type.toUpperCase(),h.hasContent=!Mt.test(h.type),o=h.url.replace(Lt,""),h.hasContent?h.data&&h.processData&&0===(h.contentType||"").indexOf("application/x-www-form-urlencoded")&&(h.data=h.data.replace(qt,"+")):(d=h.url.slice(o.length),h.data&&(h.processData||"string"==typeof h.data)&&(o+=(kt.test(o)?"&":"?")+h.data,delete h.data),!1===h.cache&&(o=o.replace(Ht,"$1"),d=(kt.test(o)?"&":"?")+"_="+Et+++d),h.url=o+d),h.ifModified&&(w.lastModified[o]&&E.setRequestHeader("If-Modified-Since",w.lastModified[o]),w.etag[o]&&E.setRequestHeader("If-None-Match",w.etag[o])),(h.data&&h.hasContent&&!1!==h.contentType||n.contentType)&&E.setRequestHeader("Content-Type",h.contentType),E.setRequestHeader("Accept",h.dataTypes[0]&&h.accepts[h.dataTypes[0]]?h.accepts[h.dataTypes[0]]+("*"!==h.dataTypes[0]?", "+$t+"; q=0.01":""):h.accepts["*"]);for(p in h.headers)E.setRequestHeader(p,h.headers[p]);if(h.beforeSend&&(!1===h.beforeSend.call(g,E,h)||c))return E.abort();if(C="abort",m.add(h.complete),E.done(h.success),E.fail(h.error),i=_t(Wt,h,n,E)){if(E.readyState=1,f&&y.trigger("ajaxSend",[E,h]),c)return E;h.async&&h.timeout>0&&(u=e.setTimeout(function(){E.abort("timeout")},h.timeout));try{c=!1,i.send(b,k)}catch(e){if(c)throw e;k(-1,e)}}else k(-1,"No Transport");function k(t,n,r,s){var l,p,d,b,T,C=n;c||(c=!0,u&&e.clearTimeout(u),i=void 0,a=s||"",E.readyState=t>0?4:0,l=t>=200&&t<300||304===t,r&&(b=Xt(h,E,r)),b=Ut(h,b,E,l),l?(h.ifModified&&((T=E.getResponseHeader("Last-Modified"))&&(w.lastModified[o]=T),(T=E.getResponseHeader("etag"))&&(w.etag[o]=T)),204===t||"HEAD"===h.type?C="nocontent":304===t?C="notmodified":(C=b.state,p=b.data,l=!(d=b.error))):(d=C,!t&&C||(C="error",t<0&&(t=0))),E.status=t,E.statusText=(n||C)+"",l?v.resolveWith(g,[p,C,E]):v.rejectWith(g,[E,C,d]),E.statusCode(x),x=void 0,f&&y.trigger(l?"ajaxSuccess":"ajaxError",[E,h,l?p:d]),m.fireWith(g,[E,C]),f&&(y.trigger("ajaxComplete",[E,h]),--w.active||w.event.trigger("ajaxStop")))}return E},getJSON:function(e,t,n){return w.get(e,t,n,"json")},getScript:function(e,t){return w.get(e,void 0,t,"script")}}),w.each(["get","post"],function(e,t){w[t]=function(e,n,r,i){return g(n)&&(i=i||r,r=n,n=void 0),w.ajax(w.extend({url:e,type:t,dataType:i,data:n,success:r},w.isPlainObject(e)&&e))}}),w._evalUrl=function(e){return w.ajax({url:e,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},w.fn.extend({wrapAll:function(e){var t;return this[0]&&(g(e)&&(e=e.call(this[0])),t=w(e,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){var e=this;while(e.firstElementChild)e=e.firstElementChild;return e}).append(this)),this},wrapInner:function(e){return g(e)?this.each(function(t){w(this).wrapInner(e.call(this,t))}):this.each(function(){var t=w(this),n=t.contents();n.length?n.wrapAll(e):t.append(e)})},wrap:function(e){var t=g(e);return this.each(function(n){w(this).wrapAll(t?e.call(this,n):e)})},unwrap:function(e){return this.parent(e).not("body").each(function(){w(this).replaceWith(this.childNodes)}),this}}),w.expr.pseudos.hidden=function(e){return!w.expr.pseudos.visible(e)},w.expr.pseudos.visible=function(e){return!!(e.offsetWidth||e.offsetHeight||e.getClientRects().length)},w.ajaxSettings.xhr=function(){try{return new e.XMLHttpRequest}catch(e){}};var Vt={0:200,1223:204},Gt=w.ajaxSettings.xhr();h.cors=!!Gt&&"withCredentials"in Gt,h.ajax=Gt=!!Gt,w.ajaxTransport(function(t){var n,r;if(h.cors||Gt&&!t.crossDomain)return{send:function(i,o){var a,s=t.xhr();if(s.open(t.type,t.url,t.async,t.username,t.password),t.xhrFields)for(a in t.xhrFields)s[a]=t.xhrFields[a];t.mimeType&&s.overrideMimeType&&s.overrideMimeType(t.mimeType),t.crossDomain||i["X-Requested-With"]||(i["X-Requested-With"]="XMLHttpRequest");for(a in i)s.setRequestHeader(a,i[a]);n=function(e){return function(){n&&(n=r=s.onload=s.onerror=s.onabort=s.ontimeout=s.onreadystatechange=null,"abort"===e?s.abort():"error"===e?"number"!=typeof s.status?o(0,"error"):o(s.status,s.statusText):o(Vt[s.status]||s.status,s.statusText,"text"!==(s.responseType||"text")||"string"!=typeof s.responseText?{binary:s.response}:{text:s.responseText},s.getAllResponseHeaders()))}},s.onload=n(),r=s.onerror=s.ontimeout=n("error"),void 0!==s.onabort?s.onabort=r:s.onreadystatechange=function(){4===s.readyState&&e.setTimeout(function(){n&&r()})},n=n("abort");try{s.send(t.hasContent&&t.data||null)}catch(e){if(n)throw e}},abort:function(){n&&n()}}}),w.ajaxPrefilter(function(e){e.crossDomain&&(e.contents.script=!1)}),w.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(e){return w.globalEval(e),e}}}),w.ajaxPrefilter("script",function(e){void 0===e.cache&&(e.cache=!1),e.crossDomain&&(e.type="GET")}),w.ajaxTransport("script",function(e){if(e.crossDomain){var t,n;return{send:function(i,o){t=w("<script>").prop({charset:e.scriptCharset,src:e.url}).on("load error",n=function(e){t.remove(),n=null,e&&o("error"===e.type?404:200,e.type)}),r.head.appendChild(t[0])},abort:function(){n&&n()}}}});var Yt=[],Qt=/(=)\?(?=&|$)|\?\?/;w.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var e=Yt.pop()||w.expando+"_"+Et++;return this[e]=!0,e}}),w.ajaxPrefilter("json jsonp",function(t,n,r){var i,o,a,s=!1!==t.jsonp&&(Qt.test(t.url)?"url":"string"==typeof t.data&&0===(t.contentType||"").indexOf("application/x-www-form-urlencoded")&&Qt.test(t.data)&&"data");if(s||"jsonp"===t.dataTypes[0])return i=t.jsonpCallback=g(t.jsonpCallback)?t.jsonpCallback():t.jsonpCallback,s?t[s]=t[s].replace(Qt,"$1"+i):!1!==t.jsonp&&(t.url+=(kt.test(t.url)?"&":"?")+t.jsonp+"="+i),t.converters["script json"]=function(){return a||w.error(i+" was not called"),a[0]},t.dataTypes[0]="json",o=e[i],e[i]=function(){a=arguments},r.always(function(){void 0===o?w(e).removeProp(i):e[i]=o,t[i]&&(t.jsonpCallback=n.jsonpCallback,Yt.push(i)),a&&g(o)&&o(a[0]),a=o=void 0}),"script"}),h.createHTMLDocument=function(){var e=r.implementation.createHTMLDocument("").body;return e.innerHTML="<form></form><form></form>",2===e.childNodes.length}(),w.parseHTML=function(e,t,n){if("string"!=typeof e)return[];"boolean"==typeof t&&(n=t,t=!1);var i,o,a;return t||(h.createHTMLDocument?((i=(t=r.implementation.createHTMLDocument("")).createElement("base")).href=r.location.href,t.head.appendChild(i)):t=r),o=A.exec(e),a=!n&&[],o?[t.createElement(o[1])]:(o=xe([e],t,a),a&&a.length&&w(a).remove(),w.merge([],o.childNodes))},w.fn.load=function(e,t,n){var r,i,o,a=this,s=e.indexOf(" ");return s>-1&&(r=vt(e.slice(s)),e=e.slice(0,s)),g(t)?(n=t,t=void 0):t&&"object"==typeof t&&(i="POST"),a.length>0&&w.ajax({url:e,type:i||"GET",dataType:"html",data:t}).done(function(e){o=arguments,a.html(r?w("<div>").append(w.parseHTML(e)).find(r):e)}).always(n&&function(e,t){a.each(function(){n.apply(this,o||[e.responseText,t,e])})}),this},w.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(e,t){w.fn[t]=function(e){return this.on(t,e)}}),w.expr.pseudos.animated=function(e){return w.grep(w.timers,function(t){return e===t.elem}).length},w.offset={setOffset:function(e,t,n){var r,i,o,a,s,u,l,c=w.css(e,"position"),f=w(e),p={};"static"===c&&(e.style.position="relative"),s=f.offset(),o=w.css(e,"top"),u=w.css(e,"left"),(l=("absolute"===c||"fixed"===c)&&(o+u).indexOf("auto")>-1)?(a=(r=f.position()).top,i=r.left):(a=parseFloat(o)||0,i=parseFloat(u)||0),g(t)&&(t=t.call(e,n,w.extend({},s))),null!=t.top&&(p.top=t.top-s.top+a),null!=t.left&&(p.left=t.left-s.left+i),"using"in t?t.using.call(e,p):f.css(p)}},w.fn.extend({offset:function(e){if(arguments.length)return void 0===e?this:this.each(function(t){w.offset.setOffset(this,e,t)});var t,n,r=this[0];if(r)return r.getClientRects().length?(t=r.getBoundingClientRect(),n=r.ownerDocument.defaultView,{top:t.top+n.pageYOffset,left:t.left+n.pageXOffset}):{top:0,left:0}},position:function(){if(this[0]){var e,t,n,r=this[0],i={top:0,left:0};if("fixed"===w.css(r,"position"))t=r.getBoundingClientRect();else{t=this.offset(),n=r.ownerDocument,e=r.offsetParent||n.documentElement;while(e&&(e===n.body||e===n.documentElement)&&"static"===w.css(e,"position"))e=e.parentNode;e&&e!==r&&1===e.nodeType&&((i=w(e).offset()).top+=w.css(e,"borderTopWidth",!0),i.left+=w.css(e,"borderLeftWidth",!0))}return{top:t.top-i.top-w.css(r,"marginTop",!0),left:t.left-i.left-w.css(r,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var e=this.offsetParent;while(e&&"static"===w.css(e,"position"))e=e.offsetParent;return e||be})}}),w.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(e,t){var n="pageYOffset"===t;w.fn[e]=function(r){return z(this,function(e,r,i){var o;if(y(e)?o=e:9===e.nodeType&&(o=e.defaultView),void 0===i)return o?o[t]:e[r];o?o.scrollTo(n?o.pageXOffset:i,n?i:o.pageYOffset):e[r]=i},e,r,arguments.length)}}),w.each(["top","left"],function(e,t){w.cssHooks[t]=_e(h.pixelPosition,function(e,n){if(n)return n=Fe(e,t),We.test(n)?w(e).position()[t]+"px":n})}),w.each({Height:"height",Width:"width"},function(e,t){w.each({padding:"inner"+e,content:t,"":"outer"+e},function(n,r){w.fn[r]=function(i,o){var a=arguments.length&&(n||"boolean"!=typeof i),s=n||(!0===i||!0===o?"margin":"border");return z(this,function(t,n,i){var o;return y(t)?0===r.indexOf("outer")?t["inner"+e]:t.document.documentElement["client"+e]:9===t.nodeType?(o=t.documentElement,Math.max(t.body["scroll"+e],o["scroll"+e],t.body["offset"+e],o["offset"+e],o["client"+e])):void 0===i?w.css(t,n,s):w.style(t,n,i,s)},t,a?i:void 0,a)}})}),w.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(e,t){w.fn[t]=function(e,n){return arguments.length>0?this.on(t,null,e,n):this.trigger(t)}}),w.fn.extend({hover:function(e,t){return this.mouseenter(e).mouseleave(t||e)}}),w.fn.extend({bind:function(e,t,n){return this.on(e,null,t,n)},unbind:function(e,t){return this.off(e,null,t)},delegate:function(e,t,n,r){return this.on(t,e,n,r)},undelegate:function(e,t,n){return 1===arguments.length?this.off(e,"**"):this.off(t,e||"**",n)}}),w.proxy=function(e,t){var n,r,i;if("string"==typeof t&&(n=e[t],t=e,e=n),g(e))return r=o.call(arguments,2),i=function(){return e.apply(t||this,r.concat(o.call(arguments)))},i.guid=e.guid=e.guid||w.guid++,i},w.holdReady=function(e){e?w.readyWait++:w.ready(!0)},w.isArray=Array.isArray,w.parseJSON=JSON.parse,w.nodeName=N,w.isFunction=g,w.isWindow=y,w.camelCase=G,w.type=x,w.now=Date.now,w.isNumeric=function(e){var t=w.type(e);return("number"===t||"string"===t)&&!isNaN(e-parseFloat(e))},"function"==typeof define&&define.amd&&define("jquery",[],function(){return w});var Jt=e.jQuery,Kt=e.$;return w.noConflict=function(t){return e.$===w&&(e.$=Kt),t&&e.jQuery===w&&(e.jQuery=Jt),w},t||(e.jQuery=e.$=w),w});
</script>
  <script type="text/javascript">/**
 * StyleFix 1.0.3 & PrefixFree 1.0.7
 * @author Lea Verou
 * MIT license
 */(function(){function t(e,t){return[].slice.call((t||document).querySelectorAll(e))}if(!window.addEventListener)return;var e=window.StyleFix={link:function(t){try{if(t.rel!=="stylesheet"||t.hasAttribute("data-noprefix"))return}catch(n){return}var r=t.href||t.getAttribute("data-href"),i=r.replace(/[^\/]+$/,""),s=t.parentNode,o=new XMLHttpRequest,u;o.onreadystatechange=function(){o.readyState===4&&u()};u=function(){var n=o.responseText;if(n&&t.parentNode&&(!o.status||o.status<400||o.status>600)){n=e.fix(n,!0,t);if(i){n=n.replace(/url\(\s*?((?:"|')?)(.+?)\1\s*?\)/gi,function(e,t,n){return/^([a-z]{3,10}:|\/|#)/i.test(n)?e:'url("'+i+n+'")'});var r=i.replace(/([\\\^\$*+[\]?{}.=!:(|)])/g,"\\$1");n=n.replace(RegExp("\\b(behavior:\\s*?url\\('?\"?)"+r,"gi"),"$1")}var u=document.createElement("style");u.textContent=n;u.media=t.media;u.disabled=t.disabled;u.setAttribute("data-href",t.getAttribute("href"));s.insertBefore(u,t);s.removeChild(t);u.media=t.media}};try{o.open("GET",r);o.send(null)}catch(n){if(typeof XDomainRequest!="undefined"){o=new XDomainRequest;o.onerror=o.onprogress=function(){};o.onload=u;o.open("GET",r);o.send(null)}}t.setAttribute("data-inprogress","")},styleElement:function(t){if(t.hasAttribute("data-noprefix"))return;var n=t.disabled;t.textContent=e.fix(t.textContent,!0,t);t.disabled=n},styleAttribute:function(t){var n=t.getAttribute("style");n=e.fix(n,!1,t);t.setAttribute("style",n)},process:function(){t('link[rel="stylesheet"]:not([data-inprogress])').forEach(StyleFix.link);t("style").forEach(StyleFix.styleElement);t("[style]").forEach(StyleFix.styleAttribute)},register:function(t,n){(e.fixers=e.fixers||[]).splice(n===undefined?e.fixers.length:n,0,t)},fix:function(t,n,r){for(var i=0;i<e.fixers.length;i++)t=e.fixers[i](t,n,r)||t;return t},camelCase:function(e){return e.replace(/-([a-z])/g,function(e,t){return t.toUpperCase()}).replace("-","")},deCamelCase:function(e){return e.replace(/[A-Z]/g,function(e){return"-"+e.toLowerCase()})}};(function(){setTimeout(function(){t('link[rel="stylesheet"]').forEach(StyleFix.link)},10);document.addEventListener("DOMContentLoaded",StyleFix.process,!1)})()})();(function(e){function t(e,t,r,i,s){e=n[e];if(e.length){var o=RegExp(t+"("+e.join("|")+")"+r,"gi");s=s.replace(o,i)}return s}if(!window.StyleFix||!window.getComputedStyle)return;var n=window.PrefixFree={prefixCSS:function(e,r,i){var s=n.prefix;n.functions.indexOf("linear-gradient")>-1&&(e=e.replace(/(\s|:|,)(repeating-)?linear-gradient\(\s*(-?\d*\.?\d*)deg/ig,function(e,t,n,r){return t+(n||"")+"linear-gradient("+(90-r)+"deg"}));e=t("functions","(\\s|:|,)","\\s*\\(","$1"+s+"$2(",e);e=t("keywords","(\\s|:)","(\\s|;|\\}|$)","$1"+s+"$2$3",e);e=t("properties","(^|\\{|\\s|;)","\\s*:","$1"+s+"$2:",e);if(n.properties.length){var o=RegExp("\\b("+n.properties.join("|")+")(?!:)","gi");e=t("valueProperties","\\b",":(.+?);",function(e){return e.replace(o,s+"$1")},e)}if(r){e=t("selectors","","\\b",n.prefixSelector,e);e=t("atrules","@","\\b","@"+s+"$1",e)}e=e.replace(RegExp("-"+s,"g"),"-");e=e.replace(/-\*-(?=[a-z]+)/gi,n.prefix);return e},property:function(e){return(n.properties.indexOf(e)?n.prefix:"")+e},value:function(e,r){e=t("functions","(^|\\s|,)","\\s*\\(","$1"+n.prefix+"$2(",e);e=t("keywords","(^|\\s)","(\\s|$)","$1"+n.prefix+"$2$3",e);return e},prefixSelector:function(e){return e.replace(/^:{1,2}/,function(e){return e+n.prefix})},prefixProperty:function(e,t){var r=n.prefix+e;return t?StyleFix.camelCase(r):r}};(function(){var e={},t=[],r={},i=getComputedStyle(document.documentElement,null),s=document.createElement("div").style,o=function(n){if(n.charAt(0)==="-"){t.push(n);var r=n.split("-"),i=r[1];e[i]=++e[i]||1;while(r.length>3){r.pop();var s=r.join("-");u(s)&&t.indexOf(s)===-1&&t.push(s)}}},u=function(e){return StyleFix.camelCase(e)in s};if(i.length>0)for(var a=0;a<i.length;a++)o(i[a]);else for(var f in i)o(StyleFix.deCamelCase(f));var l={uses:0};for(var c in e){var h=e[c];l.uses<h&&(l={prefix:c,uses:h})}n.prefix="-"+l.prefix+"-";n.Prefix=StyleFix.camelCase(n.prefix);n.properties=[];for(var a=0;a<t.length;a++){var f=t[a];if(f.indexOf(n.prefix)===0){var p=f.slice(n.prefix.length);u(p)||n.properties.push(p)}}n.Prefix=="Ms"&&!("transform"in s)&&!("MsTransform"in s)&&"msTransform"in s&&n.properties.push("transform","transform-origin");n.properties.sort()})();(function(){function i(e,t){r[t]="";r[t]=e;return!!r[t]}var e={"linear-gradient":{property:"backgroundImage",params:"red, teal"},calc:{property:"width",params:"1px + 5%"},element:{property:"backgroundImage",params:"#foo"},"cross-fade":{property:"backgroundImage",params:"url(a.png), url(b.png), 50%"}};e["repeating-linear-gradient"]=e["repeating-radial-gradient"]=e["radial-gradient"]=e["linear-gradient"];var t={initial:"color","zoom-in":"cursor","zoom-out":"cursor",box:"display",flexbox:"display","inline-flexbox":"display",flex:"display","inline-flex":"display"};n.functions=[];n.keywords=[];var r=document.createElement("div").style;for(var s in e){var o=e[s],u=o.property,a=s+"("+o.params+")";!i(a,u)&&i(n.prefix+a,u)&&n.functions.push(s)}for(var f in t){var u=t[f];!i(f,u)&&i(n.prefix+f,u)&&n.keywords.push(f)}})();(function(){function s(e){i.textContent=e+"{}";return!!i.sheet.cssRules.length}var t={":read-only":null,":read-write":null,":any-link":null,"::selection":null},r={keyframes:"name",viewport:null,document:'regexp(".")'};n.selectors=[];n.atrules=[];var i=e.appendChild(document.createElement("style"));for(var o in t){var u=o+(t[o]?"("+t[o]+")":"");!s(u)&&s(n.prefixSelector(u))&&n.selectors.push(o)}for(var a in r){var u=a+" "+(r[a]||"");!s("@"+u)&&s("@"+n.prefix+u)&&n.atrules.push(a)}e.removeChild(i)})();n.valueProperties=["transition","transition-property"];e.className+=" "+n.prefix;StyleFix.register(n.prefixCSS)})(document.documentElement);
 </script>
  <script type="text/javascript">/**
 * Minified by jsDelivr using UglifyJS v3.3.21.
 * Original file: /npm/conic-gradient@1.0.0/conic-gradient.js
 *
 * Do NOT use SRI with dynamically generated files! More information: https://www.jsdelivr.com/using-sri-with-dynamic-files
 */
!function(){var t=Math.PI,e=2*t,d=t/180,o=document.createElement("div");document.head.appendChild(o);var p=self.ConicGradient=function(t){p.all.push(this),t=t||{},this.canvas=document.createElement("canvas"),this.context=this.canvas.getContext("2d"),this.repeating=!!t.repeating,this.size=t.size||Math.max(innerWidth,innerHeight),this.canvas.width=this.canvas.height=this.size;var s=t.stops;this.stops=(s||"").split(/\s*,(?![^(]*\))\s*/);for(var o=this.from=0;o<this.stops.length;o++)if(this.stops[o]){var i=this.stops[o]=new p.ColorStop(this,this.stops[o]);i.next&&(this.stops.splice(o+1,0,i.next),o++)}else this.stops.splice(o,1),o--;if(0==this.stops[0].color.indexOf("from")&&(this.from=360*this.stops[0].pos,this.stops.shift()),void 0===this.stops[0].pos)this.stops[0].pos=0;else if(0<this.stops[0].pos){var e=this.stops[0].clone();e.pos=0,this.stops.unshift(e)}if(void 0===this.stops[this.stops.length-1].pos)this.stops[this.stops.length-1].pos=1;else if(!this.repeating&&this.stops[this.stops.length-1].pos<1){var r=this.stops[this.stops.length-1].clone();r.pos=1,this.stops.push(r)}if(this.stops.forEach(function(t,s){if(void 0===t.pos){for(var o=s+1;this[o];o++)if(void 0!==this[o].pos){t.pos=this[s-1].pos+(this[o].pos-this[s-1].pos)/(o-s+1);break}}else 0<s&&(t.pos=Math.max(t.pos,this[s-1].pos))},this.stops),this.repeating){var n=(s=this.stops.slice())[s.length-1].pos-s[0].pos;for(o=0;this.stops[this.stops.length-1].pos<1&&o<1e4;o++)for(var h=0;h<s.length;h++){var a=s[h].clone();a.pos+=(o+1)*n,this.stops.push(a)}}this.paint()};p.all=[],p.prototype={toString:function(){return"url('"+this.dataURL+"')"},get dataURL(){return"data:image/svg+xml,"+encodeURIComponent(this.svg)},get blobURL(){return URL.createObjectURL(new Blob([this.svg],{type:"image/svg+xml"}))},get svg(){return'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none"><svg viewBox="0 0 100 100" preserveAspectRatio="xMidYMid slice"><image width="100" height="100%" xlink:href="'+this.png+'" /></svg></svg>'},get png(){return this.canvas.toDataURL()},get r(){return Math.sqrt(2)*this.size/2},paint:function(){var i,t,e,s=this.context,o=this.r,r=this.size/2,n=0,h=this.stops[n];s.translate(this.size/2,this.size/2),s.rotate(-90*d),s.rotate(this.from*d),s.translate(-this.size/2,-this.size/2);for(var a=0;a<360;){if(a/360+1e-5>=h.pos){for(;i=h,n++,(h=this.stops[n])&&h!=i&&h.pos===i.pos;);if(!h)break;var p=i.color+""==h.color+""&&i!=h;t=i.color.map(function(t,s){return h.color[s]-t})}e=(a/360-i.pos)/(h.pos-i.pos);var l=p?h.color:t.map(function(t,s){var o=t*e+i.color[s];return s<3?255&o:o});if(s.fillStyle="rgba("+l.join(",")+")",s.beginPath(),s.moveTo(r,r),p)var c=360*(h.pos-i.pos);else c=.5;var g=a*d,f=(g=Math.min(360*d,g))+c*d;f=Math.min(360*d,f+.02),s.arc(r,r,o,g,f),s.closePath(),s.fill(),a+=c}}},p.ColorStop=function(t,s){if(this.gradient=t,s){var o=s.match(/^(.+?)(?:\s+([\d.]+)(%|deg|turn|grad|rad)?)?(?:\s+([\d.]+)(%|deg|turn|grad|rad)?)?\s*$/);if(this.color=p.ColorStop.colorToRGBA(o[1]),o[2]){var i=o[3];"%"==i||"0"===o[2]&&!i?this.pos=o[2]/100:"turn"==i?this.pos=+o[2]:"deg"==i?this.pos=o[2]/360:"grad"==i?this.pos=o[2]/400:"rad"==i&&(this.pos=o[2]/e)}o[4]&&(this.next=new p.ColorStop(t,o[1]+" "+o[4]+o[5]))}},p.ColorStop.prototype={clone:function(){var t=new p.ColorStop(this.gradient);return t.color=this.color,t.pos=this.pos,t},toString:function(){return"rgba("+this.color.join(", ")+") "+100*this.pos+"%"}},p.ColorStop.colorToRGBA=function(t){if(!Array.isArray(t)&&-1==t.indexOf("from")){o.style.color=t;var s=getComputedStyle(o).color.match(/rgba?\(([\d.]+), ([\d.]+), ([\d.]+)(?:, ([\d.]+))?\)/);return s&&(s.shift(),(s=s.map(function(t){return+t}))[3]=isNaN(s[3])?1:s[3]),s||[0,0,0,0]}return t}}(),self.StyleFix&&function(){var t=document.createElement("p");t.style.backgroundImage="conic-gradient(white, black)",t.style.backgroundImage=PrefixFree.prefix+"conic-gradient(white, black)",t.style.backgroundImage||StyleFix.register(function(t,s){return-1<t.indexOf("conic-gradient")&&(t=t.replace(/(?:repeating-)?conic-gradient\(\s*((?:\([^()]+\)|[^;()}])+?)\)/g,function(t,s){return new ConicGradient({stops:s,repeating:-1<t.indexOf("repeating-")})})),t})}();
//# sourceMappingURL=/sm/9e0139ba8e1ddb934dfa3689c3207f552762686bca69b4d77270e61d5bcd733c.map</script>
  <script type="text/javascript">
    $(window).on('load', function () {
  let topColHeight = 'auto';
  let sideColWidth = 'auto';
  const csvData = {"rows":[[{"span":6,"isColumn":true,"id":10310049,"title":"Continent"}],[{"span":1,"isColumn":true,"id":10310050,"title":"Africa"},{"span":1,"isColumn":true,"id":10310051,"title":"Asia"},{"span":1,"isColumn":true,"id":10310062,"title":"Australia"},{"span":1,"isColumn":true,"id":10310052,"title":"Europe"},{"span":1,"isColumn":true,"id":10310053,"title":"North America"},{"span":1,"isColumn":true,"id":10310061,"title":"South America"}],[{"span":6,"isColumn":false,"id":10310054,"title":"Crisis"},{"span":1,"isColumn":false,"id":10310055,"title":"Political"}],[{"span":1,"isColumn":false,"id":10310057,"title":"Economic"}],[{"span":1,"isColumn":false,"id":10310056,"title":"Social"}],[{"span":1,"isColumn":false,"id":10310058,"title":"Technological"}],[{"span":1,"isColumn":false,"id":10310059,"title":"Legal"}],[{"span":1,"isColumn":false,"id":10310060,"title":"Environmental"}]],"totalColBreadth":6,"totalColDepth":2,"totalRowBreadth":6,"totalRowDepth":1};
  const filters = [{"id":10310063,"label":"Study design","checked":false,"children":[{"id":10310065,"label":"Experimental non-RCT","checked":false,"children":[]},{"id":10310064,"label":"Experimental RCT","checked":false,"children":[]},{"id":10310066,"label":"Observational analytical","checked":false,"children":[]},{"id":10310067,"label":"Observational descriptive","checked":false,"children":[]}]}];
  const autoOpenFilter = false;
  const externalURLedAttributes = [{"AttributeSetId":10309905,"AttributeId":10310049,"AttributeName":"Continent","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309906,"AttributeId":10310050,"AttributeName":"Continent - Africa","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309907,"AttributeId":10310051,"AttributeName":"Continent - Asia","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309918,"AttributeId":10310062,"AttributeName":"Continent - Australia","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309908,"AttributeId":10310052,"AttributeName":"Continent - Europe","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309909,"AttributeId":10310053,"AttributeName":"Continent - North America","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309917,"AttributeId":10310061,"AttributeName":"Continent - South America","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309902,"AttributeId":10310046,"AttributeName":"Counter","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309904,"AttributeId":10310048,"AttributeName":"Counter - No","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309903,"AttributeId":10310047,"AttributeName":"Counter - Yes","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309910,"AttributeId":10310054,"AttributeName":"Crisis","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309913,"AttributeId":10310057,"AttributeName":"Crisis - Economic","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309916,"AttributeId":10310060,"AttributeName":"Crisis - Environmental","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309915,"AttributeId":10310059,"AttributeName":"Crisis - Legal","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309911,"AttributeId":10310055,"AttributeName":"Crisis - Political","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309912,"AttributeId":10310056,"AttributeName":"Crisis - Social","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309914,"AttributeId":10310058,"AttributeName":"Crisis - Technological","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309896,"AttributeId":10310040,"AttributeName":"Resilience indicator","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309898,"AttributeId":10310042,"AttributeName":"Resilience indicator - Adaptiveness","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309897,"AttributeId":10310041,"AttributeName":"Resilience indicator - Awareness","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309899,"AttributeId":10310043,"AttributeName":"Resilience indicator - Integration","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309900,"AttributeId":10310044,"AttributeName":"Resilience indicator - Resource availability and access","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309901,"AttributeId":10310045,"AttributeName":"Resilience indicator - Self-regulation","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309919,"AttributeId":10310063,"AttributeName":"Study design","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309920,"AttributeId":10310064,"AttributeName":"Study design - Experimental RCT","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309921,"AttributeId":10310065,"AttributeName":"Study design - Experimental non-RCT","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309922,"AttributeId":10310066,"AttributeName":"Study design - Observational analytical","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309923,"AttributeId":10310067,"AttributeName":"Study design - Observational descriptive","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309905,"AttributeId":10310049,"AttributeName":"Continent","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309906,"AttributeId":10310050,"AttributeName":"Continent - Africa","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309907,"AttributeId":10310051,"AttributeName":"Continent - Asia","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309918,"AttributeId":10310062,"AttributeName":"Continent - Australia","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309908,"AttributeId":10310052,"AttributeName":"Continent - Europe","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309909,"AttributeId":10310053,"AttributeName":"Continent - North America","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309917,"AttributeId":10310061,"AttributeName":"Continent - South America","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309902,"AttributeId":10310046,"AttributeName":"Counter","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309904,"AttributeId":10310048,"AttributeName":"Counter - No","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309903,"AttributeId":10310047,"AttributeName":"Counter - Yes","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309910,"AttributeId":10310054,"AttributeName":"Crisis","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309913,"AttributeId":10310057,"AttributeName":"Crisis - Economic","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309916,"AttributeId":10310060,"AttributeName":"Crisis - Environmental","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309915,"AttributeId":10310059,"AttributeName":"Crisis - Legal","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309911,"AttributeId":10310055,"AttributeName":"Crisis - Political","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309912,"AttributeId":10310056,"AttributeName":"Crisis - Social","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309914,"AttributeId":10310058,"AttributeName":"Crisis - Technological","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309896,"AttributeId":10310040,"AttributeName":"Resilience indicator","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309898,"AttributeId":10310042,"AttributeName":"Resilience indicator - Adaptiveness","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309897,"AttributeId":10310041,"AttributeName":"Resilience indicator - Awareness","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309899,"AttributeId":10310043,"AttributeName":"Resilience indicator - Integration","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309900,"AttributeId":10310044,"AttributeName":"Resilience indicator - Resource availability and access","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309901,"AttributeId":10310045,"AttributeName":"Resilience indicator - Self-regulation","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309919,"AttributeId":10310063,"AttributeName":"Study design","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309920,"AttributeId":10310064,"AttributeName":"Study design - Experimental RCT","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309921,"AttributeId":10310065,"AttributeName":"Study design - Experimental non-RCT","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309922,"AttributeId":10310066,"AttributeName":"Study design - Observational analytical","AttributeDescription":"","ExtURL":"","ExtType":""},{"AttributeSetId":10309923,"AttributeId":10310067,"AttributeName":"Study design - Observational descriptive","AttributeDescription":"","ExtURL":"","ExtType":""}];
  const metaProperties = ["Authors","Journal","Title","Year","DOI","Volume","Issue"];
  const aboutContent = "<p>An interactive evidence gap map to accompany the article '<strong>A systematic scoping review to identify factors that characterize resilient immunization programs</strong>'</p>";
  const studySubmissionContent = "";
  const segmentAttributes = [{"attribute":{"AttributeDescription":"","ExtURL":"","ExtType":"","OriginalAttributeID":0,"AttributeSetId":10309923,"AttributeId":10310067,"AttributeSetDescription":"","AttributeType":"Selectable (show checkbox)","AttributeName":"Observational descriptive"},"color":"#0275D8"},{"attribute":{"AttributeDescription":"","ExtURL":"","ExtType":"","OriginalAttributeID":0,"AttributeSetId":10309922,"AttributeId":10310066,"AttributeSetDescription":"","AttributeType":"Selectable (show checkbox)","AttributeName":"Observational analytical"},"color":"#F0AD4E"},{"attribute":{"AttributeDescription":"","ExtURL":"","ExtType":"","OriginalAttributeID":0,"AttributeSetId":10309920,"AttributeId":10310064,"AttributeSetDescription":"","AttributeType":"Selectable (show checkbox)","AttributeName":"Experimental RCT"},"color":"#5CB85C"},{"attribute":{"AttributeDescription":"","ExtURL":"","ExtType":"","OriginalAttributeID":0,"AttributeSetId":10309921,"AttributeId":10310065,"AttributeSetDescription":"","AttributeType":"Selectable (show checkbox)","AttributeName":"Experimental non-RCT"},"color":"#D9534F"}];
  const referenceData = [{"Codes":[{"AttributeId":10310052,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":68454299,"Title":"Decline in HPV-vaccination uptake in Denmark - the association between HPV-related media coverage and HPV-vaccination.","ParentTitle":"BMC public health","ShortTitle":"Suppli (2018)","DateCreated":"07/02/2022","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2018","Month":"December","StandardNumber":"1471-2458 (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"18","Pages":"1360","Edition":"","Issue":"1","Availability":"","URL":"","OldItemId":"30526589","Comments":"","TypeName":"Journal, Article","Authors":"Suppli CH ; Hansen ND ; Rasmussen M ; Valentiner-Branth P ; Krause TG ; Mølbak K ; ","ParentAuthors":"","DOI":"https://doi.org/10.1186/s12889-018-6268-x ","Keywords":"Child\r\nCohort Studies\r\nDenmark\r\nFemale\r\nHumans\r\nMass Media/*statistics & numerical data\r\nPapillomavirus Vaccines/*administration & dosage\r\nVaccination/*statistics & numerical data\r\nHPV-vaccine\r\nHuman papilloma virus vaccine\r\nImmunization\r\nMedia monitoring\r\nOn-line searches\r\nVaccination coverage","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Suppli CH, Hansen ND, Rasmussen M, Valentiner-Branth P, Krause TG, and Mølbak K (2018) Decline in HPV-vaccination uptake in Denmark - the association between HPV-related media coverage and HPV-vaccination.. BMC public health 18(1), 1360 DOI: https://doi.org/10.1186/s12889-018-6268-x "},{"Codes":[{"AttributeId":10310045,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310050,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65365931,"Title":"Impact of the Ebola outbreak on routine immunization in western area, Sierra Leone - a field survey from an Ebola epidemic area.","ParentTitle":"BMC public health","ShortTitle":"Sun (2017)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2017","Month":"April","StandardNumber":"1471-2458 (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"17","Pages":"363","Edition":"","Issue":"1","Availability":"","URL":"","OldItemId":"28446173","Comments":"","TypeName":"Journal, Article","Authors":"Sun X ; Samba TT ; Yao J ; Yin W ; Xiao L ; Liu F ; Liu X ; Zhou J ; Kou Z ; Fan H ; Zhang H ; Williams A ; Lansana PM ; Yin Z ; ","ParentAuthors":"","DOI":"https://doi.org/10.1186/s12889-017-4242-7 ","Keywords":"Child, Preschool\r\nDisease Outbreaks/*statistics & numerical data\r\nHemorrhagic Fever, Ebola/*epidemiology\r\nHumans\r\nImmunization Programs/*statistics & numerical data\r\nSierra Leone/epidemiology\r\nViral Vaccines/administration & dosage\r\n*Ebola virus disease\r\n*Field survey\r\n*Vaccination coverage","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Sun X, Samba TT, Yao J, Yin W, Xiao L, Liu F, Liu X, Zhou J, Kou Z, Fan H, Zhang H, Williams A, Lansana PM, and Yin Z (2017) Impact of the Ebola outbreak on routine immunization in western area, Sierra Leone - a field survey from an Ebola epidemic area.. BMC public health 17(1), 363 DOI: https://doi.org/10.1186/s12889-017-4242-7 "},{"Codes":[{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310051,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":68454298,"Title":"Newspaper coverage before and after the HPV vaccination crisis began in Japan: a text mining analysis.","ParentTitle":"BMC public health","ShortTitle":"Okuhara (2019)","DateCreated":"07/02/2022","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2019","Month":"June","StandardNumber":"1471-2458 (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"19","Pages":"770","Edition":"","Issue":"1","Availability":"","URL":"","OldItemId":"31208394","Comments":"","TypeName":"Journal, Article","Authors":"Okuhara T ; Ishikawa H ; Okada M ; Kato M ; Kiuchi T ; ","ParentAuthors":"","DOI":"https://doi.org/10.1186/s12889-019-7097-2 ","Keywords":"Data Mining\r\nFemale\r\nHumans\r\nJapan\r\nNewspapers as Topic/*statistics & numerical data\r\nPapillomavirus Vaccines/administration & dosage/*adverse effects\r\nUterine Cervical Neoplasms/prevention & control\r\nVaccination Coverage/statistics & numerical data\r\nAnti-vaccination movement\r\nCervical cancer\r\nContent analysis\r\nHPV vaccine\r\nHuman papillomavirus vaccine\r\nNewspaper\r\nText mining","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Okuhara T, Ishikawa H, Okada M, Kato M, and Kiuchi T (2019) Newspaper coverage before and after the HPV vaccination crisis began in Japan: a text mining analysis.. BMC public health 19(1), 770 DOI: https://doi.org/10.1186/s12889-019-7097-2 "},{"Codes":[{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310050,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310066,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65368067,"Title":"Performance-based financing contributes to the resilience of health services affected by the liberian ebola outbreak","ParentTitle":"Public Health Action","ShortTitle":"Mussah (2017)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2017","Month":"","StandardNumber":"2220-8372","City":"France","Country":"","Publisher":"International Union Against Tuberculosis and Lung Disease (The Union) (68 boulevard Saint Michel, Paris 75006, France)","Institution":"V.G. Mussah, PBF Unit, Ministry of Health, Oldest Congo Town, Monrovia, Montserrado 100010. E-mail: vmussah@gmail.com","Volume":"7","Pages":"S100-S105","Edition":"","Issue":"Supplement 1","Availability":"","URL":"","OldItemId":"617731855","Comments":"","TypeName":"Journal, Article","Authors":"Mussah Vera G; Mapleh L ; Ade S ; Harries A D; Bhat P ; Kateh F ; ","ParentAuthors":"","DOI":"https://doi.org/10.5588/pha.16.0096","Keywords":"article\r\n*child health care\r\ncross-sectional study\r\n*Ebolavirus\r\nfemale\r\n*financial management\r\nhealth care personnel\r\n*health service\r\nhuman\r\nHuman immunodeficiency virus\r\nLiberian\r\nmaternal care\r\nnonhuman\r\npregnancy outcome\r\npregnant woman\r\nprenatal care\r\nprimary health care\r\nvirus infection\r\nvirus vaccine","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Mussah Vera G, Mapleh L, Ade S, Harries A D, Bhat P, and Kateh F (2017) Performance-based financing contributes to the resilience of health services affected by the liberian ebola outbreak. Public Health Action 7(Supplement 1), S100-S105 DOI: https://doi.org/10.5588/pha.16.0096"},{"Codes":[{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310053,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65365633,"Title":"Restoring Immunization Services Provided by the Vaccines for Children Program in Puerto Rico After Hurricanes Irma and Maria, 2017-2019.","ParentTitle":"Journal of public health management and practice : JPHMP","ShortTitle":"Luna-Pinto (2021)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2021","Month":"November","StandardNumber":"1078-4659 (Linking)","City":"United States","Country":"","Publisher":"","Institution":"","Volume":"27","Pages":"E228-E235","Edition":"","Issue":"6","Availability":"","URL":"","OldItemId":"32810076","Comments":"","TypeName":"Journal, Article","Authors":"Luna-Pinto SC ; Rivera A ; Cardona I ; Rijo C ; Alvarez V ; Rodriguez J ; Yoerg B ; Shapiro CN ; Patel A ; ","ParentAuthors":"","DOI":"https://doi.org/10.1097/PHH.0000000000001193 ","Keywords":"Child\r\n*Cyclonic Storms\r\n*Disasters\r\nHumans\r\nImmunization\r\nImmunization Programs\r\nPuerto Rico","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Luna-Pinto SC, Rivera A, Cardona I, Rijo C, Alvarez V, Rodriguez J, Yoerg B, Shapiro CN, and Patel A (2021) Restoring Immunization Services Provided by the Vaccines for Children Program in Puerto Rico After Hurricanes Irma and Maria, 2017-2019.. Journal of public health management and practice : JPHMP 27(6), E228-E235 DOI: https://doi.org/10.1097/PHH.0000000000001193 "},{"Codes":[{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310051,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65367762,"Title":"Impact of the first phase of COVID-19 pandemic on childhood routine immunisation services in Nepal: a qualitative study on the perspectives of service providers and users","ParentTitle":"Journal of Pharmaceutical Policy and Practice","ShortTitle":"Khatiwada (2021)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2021","Month":"","StandardNumber":"2052-3211","City":"United Kingdom","Country":"","Publisher":"BioMed Central Ltd","Institution":"A.P. Khatiwada, Department of Pharmaceutical and Health Service Research, Nepal Health Research and Innovation Foundation, Lalitpur, Nepal. E-mail: asmitapriyadarshinikhatiwada@gmail.com, S. Shrestha, Department of Pharmaceutical and Health Service Research, Nepal Health Research and Innovation Foundation, Lalitpur, Nepal. E-mail: sunilcresta@gmail.com, S. Maskey, University of North Texas Health Science Center, Fort Worth, TX, United States. E-mail: smritimaskey@gmail.com","Volume":"14","Pages":"79","Edition":"","Issue":"1","Availability":"","URL":"","OldItemId":"2013829439","Comments":"","TypeName":"Journal, Article","Authors":"Khatiwada Asmita Priyadarshini; Shrestha Sunil ; Maskey Smriti ; Shrestha Nistha ; Khanal Saval ; Kc Bhuvan ; Paudyal Vibhu ; ","ParentAuthors":"","DOI":"https://doi.org/10.1186/s40545-021-00366-z","Keywords":"article\r\nbiosafety\r\nchild\r\n*childhood\r\nclinical article\r\ncontent analysis\r\ncontrolled study\r\n*coronavirus disease 2019\r\nfemale\r\ngenetic transcription\r\nhuman\r\nInternet\r\ninterview\r\nlockdown\r\nmale\r\nmorality\r\nmotivation\r\n*Nepal\r\n*pandemic\r\npreventive health service\r\nprotective equipment\r\n*qualitative research\r\nrural area\r\nsocial media\r\n*vaccination\r\nvaccine","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Khatiwada Asmita Priyadarshini, Shrestha Sunil, Maskey Smriti, Shrestha Nistha, Khanal Saval, Kc Bhuvan, and Paudyal Vibhu (2021) Impact of the first phase of COVID-19 pandemic on childhood routine immunisation services in Nepal: a qualitative study on the perspectives of service providers and users. Journal of Pharmaceutical Policy and Practice 14(1), 79 DOI: https://doi.org/10.1186/s40545-021-00366-z"},{"Codes":[{"AttributeId":10310051,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310066,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":68563464,"Title":"COVID-19 related immunization disruptions in Rajasthan, India: A retrospective observational study.","ParentTitle":"Vaccine","ShortTitle":"Jain (2021)","DateCreated":"08/02/2022","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2021","Month":"July","StandardNumber":"0264-410X (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"39","Pages":"4343-4350","Edition":"","Issue":"31","Availability":"","URL":"","OldItemId":"34154863","Comments":"","TypeName":"Journal, Article","Authors":"Jain R ; Chopra A ; Falézan C ; Patel M ; Dupas P ; ","ParentAuthors":"","DOI":"https://doi.org/10.1016/j.vaccine.2021.06.022 ","Keywords":"*COVID-19\r\nChild\r\nCommunicable Disease Control\r\nHumans\r\nImmunization\r\n*Immunization Programs\r\nImmunization Schedule\r\nIndia\r\nInfant\r\nSARS-CoV-2\r\nVaccination\r\n*COVID-19\r\n*Catch-up vaccination\r\n*Immunization coverage\r\n*India\r\n*Pandemics","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Jain R, Chopra A, Falézan C, Patel M, and Dupas P (2021) COVID-19 related immunization disruptions in Rajasthan, India: A retrospective observational study.. Vaccine 39(31), 4343-4350 DOI: https://doi.org/10.1016/j.vaccine.2021.06.022 "},{"Codes":[{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310051,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310062,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":68563463,"Title":"Impact of COVID-19 on routine immunisation in South-East Asia and Western Pacific: Disruptions and solutions.","ParentTitle":"The Lancet regional health. Western Pacific","ShortTitle":"Harris (2021)","DateCreated":"08/02/2022","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2021","Month":"May","StandardNumber":"2666-6065 (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"10","Pages":"100140","Edition":"","Issue":"","Availability":"","URL":"","OldItemId":"33899040","Comments":"","TypeName":"Journal, Article","Authors":"Harris RC ; Chen Y ; Côte P ; Ardillon A ; Nievera MC ; Ong-Lim A ; Aiyamperumal S ; Chong CP ; Kandasamy KV ; Mahenthiran K ; Yu TW ; Huang C ; El Guerche-Séblain C ; Vargas-Zambrano JC ; Chit A ; Nageshwaran G ; ","ParentAuthors":"","DOI":"https://doi.org/10.1016/j.lanwpc.2021.100140 ","Keywords":"Asia\r\nCOVID-19\r\nDisruption\r\nImmunisation\r\nPreventive care\r\nPublic health\r\nRoutine vaccination\r\nVaccination coverage rates\r\nVaccines","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Harris RC, Chen Y, Côte P, Ardillon A, Nievera MC, Ong-Lim A, Aiyamperumal S, Chong CP, Kandasamy KV, Mahenthiran K, Yu TW, Huang C, El Guerche-Séblain C, Vargas-Zambrano JC, Chit A, and Nageshwaran G (2021) Impact of COVID-19 on routine immunisation in South-East Asia and Western Pacific: Disruptions and solutions.. The Lancet regional health. Western Pacific 10, 100140 DOI: https://doi.org/10.1016/j.lanwpc.2021.100140 "},{"Codes":[{"AttributeId":10310052,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":68454324,"Title":"A Dynamic Model of Vaccine Compliance: How Fake News Undermined the Danish HPV Vaccine Program","ParentTitle":"JOURNAL OF BUSINESS & ECONOMIC STATISTICS","ShortTitle":"Hansen (2021)","DateCreated":"07/02/2022","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2021","Month":"","StandardNumber":"1537-2707","City":"","Country":"","Publisher":"","Institution":"QuantCo Inc, Via Fontanelle 18, Fiesole, FI, Italy","Volume":"39","Pages":"259-271","Edition":"","Issue":"1","Availability":"","URL":"","OldItemId":"","Comments":"","TypeName":"Journal, Article","Authors":"Hansen PR ; Schmidtblaicher M ; ","ParentAuthors":"","DOI":"https://doi.org/10.1080/07350015.2019.1623045","Keywords":"Fake news\r\nMedia\r\nPublic health\r\nScore-driven model\r\nVaccine uptake","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Hansen PR, and Schmidtblaicher M (2021) A Dynamic Model of Vaccine Compliance: How Fake News Undermined the Danish HPV Vaccine Program. JOURNAL OF BUSINESS & ECONOMIC STATISTICS 39(1), 259-271 DOI: https://doi.org/10.1080/07350015.2019.1623045"},{"Codes":[{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310052,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65365512,"Title":"Resilience of HPV vaccine uptake in Denmark: Decline and recovery.","ParentTitle":"Vaccine","ShortTitle":"Hansen (2020)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2020","Month":"February","StandardNumber":"0264-410X (Linking)","City":"Netherlands","Country":"","Publisher":"","Institution":"","Volume":"38","Pages":"1842-1848","Edition":"","Issue":"7","Availability":"","URL":"","OldItemId":"31918860","Comments":"","TypeName":"Journal, Article","Authors":"Hansen PR ; Schmidtblaicher M ; Brewer NT ; ","ParentAuthors":"","DOI":"https://doi.org/10.1016/j.vaccine.2019.12.019 ","Keywords":"Adolescent\r\nChild\r\nDenmark\r\nFemale\r\nHumans\r\n*Papillomavirus Infections/prevention & control\r\nPapillomavirus Vaccines/*administration & dosage\r\nRetrospective Studies\r\nVaccination/*statistics & numerical data\r\n*HPV vaccination\r\n*Resilience\r\n*Safety\r\n*Vaccine hesitancy","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Hansen PR, Schmidtblaicher M, and Brewer NT (2020) Resilience of HPV vaccine uptake in Denmark: Decline and recovery.. Vaccine 38(7), 1842-1848 DOI: https://doi.org/10.1016/j.vaccine.2019.12.019 "},{"Codes":[{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310051,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310064,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310045,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65365711,"Title":"Community engagement and integrated health and polio immunisation campaigns in conflict-affected areas of Pakistan: a cluster randomised controlled trial.","ParentTitle":"The Lancet. Global health","ShortTitle":"Habib (2017)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2017","Month":"June","StandardNumber":"2214-109X (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"5","Pages":"e593-e603","Edition":"","Issue":"6","Availability":"","URL":"","OldItemId":"28495264","Comments":"","TypeName":"Journal, Article","Authors":"Habib MA ; Soofi S ; Cousens S ; Anwar S ; Haque NU ; Ahmed I ; Ali N ; Tahir R ; Bhutta ZA ; ","ParentAuthors":"","DOI":"https://doi.org/10.1016/S2214-109X(17)30184-5 ","Keywords":"Child, Preschool\r\nCluster Analysis\r\n*Community Participation\r\nFemale\r\nHealth Promotion/*organization & administration\r\nHumans\r\nImmunization/*statistics & numerical data\r\nInfant\r\nInfant, Newborn\r\nMale\r\nPakistan\r\nPoliomyelitis/*prevention & control\r\nPoliovirus Vaccine, Inactivated/*administration & dosage\r\nPoliovirus Vaccine, Oral/*administration & dosage\r\n*Warfare","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Habib MA, Soofi S, Cousens S, Anwar S, Haque NU, Ahmed I, Ali N, Tahir R, and Bhutta ZA (2017) Community engagement and integrated health and polio immunisation campaigns in conflict-affected areas of Pakistan: a cluster randomised controlled trial.. The Lancet. Global health 5(6), e593-e603 DOI: https://doi.org/10.1016/S2214-109X(17)30184-5 "},{"Codes":[{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310052,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310066,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65365574,"Title":"The contagious nature of a vaccine scare: How the introduction of HPV vaccination lifted and eroded MMR vaccination in Denmark.","ParentTitle":"Vaccine","ShortTitle":"Gørtz (2020)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2020","Month":"June","StandardNumber":"0264-410X (Linking)","City":"Netherlands","Country":"","Publisher":"","Institution":"","Volume":"38","Pages":"4432-4439","Edition":"","Issue":"28","Availability":"","URL":"","OldItemId":"32418796","Comments":"","TypeName":"Journal, Article","Authors":"Gørtz M ; Brewer NT ; Hansen PR ; Ejrnæs M ; ","ParentAuthors":"","DOI":"https://doi.org/10.1016/j.vaccine.2020.04.055 ","Keywords":"Adolescent\r\nChild\r\nDenmark\r\nFemale\r\nHumans\r\nImmunization Programs\r\nMale\r\n*Papillomavirus Infections/prevention & control\r\n*Papillomavirus Vaccines\r\nVaccination\r\n*HPV\r\n*MMR\r\n*Media coverage\r\n*Spillover\r\n*Vaccination uptake","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Gørtz M, Brewer NT, Hansen PR, and Ejrnæs M (2020) The contagious nature of a vaccine scare: How the introduction of HPV vaccination lifted and eroded MMR vaccination in Denmark.. Vaccine 38(28), 4432-4439 DOI: https://doi.org/10.1016/j.vaccine.2020.04.055 "},{"Codes":[{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310052,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65365955,"Title":"National Swedish survey showed that child health services and routine immunisation programmes were resilient during the early COVID-19 pandemic.","ParentTitle":"Acta paediatrica (Oslo, Norway : 1992)","ShortTitle":"Falkenstein (2021)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2021","Month":"September","StandardNumber":"0803-5253 (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"110","Pages":"2559-2566","Edition":"","Issue":"9","Availability":"","URL":"","OldItemId":"33973264","Comments":"","TypeName":"Journal, Article","Authors":"Falkenstein Hagander K; Aronsson B ; Danielsson M ; Lepp T ; Kulane A ; Schollin Ask L; ","ParentAuthors":"","DOI":"https://doi.org/10.1111/apa.15912","Keywords":"*COVID-19\r\nChild\r\n*Child Health Services\r\nChild, Preschool\r\nCross-Sectional Studies\r\nHumans\r\nImmunization Programs\r\nPandemics/prevention & control\r\nSARS-CoV-2\r\nSweden/epidemiology\r\n*COVID-19\r\n*child health services\r\n*immunisation programme\r\n*resilience\r\n*vaccination","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Falkenstein Hagander K, Aronsson B, Danielsson M, Lepp T, Kulane A, and Schollin Ask L (2021) National Swedish survey showed that child health services and routine immunisation programmes were resilient during the early COVID-19 pandemic.. Acta paediatrica (Oslo, and Norway : 1992) 110(9), 2559-2566 DOI: https://doi.org/10.1111/apa.15912"},{"Codes":[{"AttributeId":10310050,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":68454301,"Title":"Effect of Ebola virus disease on maternal and child health services in Guinea: a retrospective observational cohort study.","ParentTitle":"The Lancet. Global health","ShortTitle":"Delamou (2017)","DateCreated":"07/02/2022","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2017","Month":"April","StandardNumber":"2214-109X (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"5","Pages":"e448-e457","Edition":"","Issue":"4","Availability":"","URL":"","OldItemId":"28237252","Comments":"","TypeName":"Journal, Article","Authors":"Delamou A ; Ayadi AME ; Sidibe S ; Delvaux T ; Camara BS ; Sandouno SD ; Beavogui AH ; Rutherford GW ; Okumura J ; Zhang WH ; De Brouwere V ; ","ParentAuthors":"","DOI":"https://doi.org/10.1016/S2214-109X(17)30078-5 ","Keywords":"Adult\r\nChild\r\nChild Health/*statistics & numerical data\r\nChild, Preschool\r\nFemale\r\nGuinea\r\nHemorrhagic Fever, Ebola/*prevention & control\r\nHumans\r\n*Immunization Programs\r\nInfant\r\nInfant, Newborn\r\nMaternal Health/*statistics & numerical data\r\nMaternal-Child Health Services/*organization & administration\r\nPoverty\r\nRetrospective Studies\r\nYoung Adult","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Delamou A, Ayadi AME, Sidibe S, Delvaux T, Camara BS, Sandouno SD, Beavogui AH, Rutherford GW, Okumura J, Zhang WH, and De Brouwere V (2017) Effect of Ebola virus disease on maternal and child health services in Guinea: a retrospective observational cohort study.. The Lancet. Global health 5(4), e448-e457 DOI: https://doi.org/10.1016/S2214-109X(17)30078-5 "},{"Codes":[{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310051,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310055,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65365614,"Title":"The importance of effective risk communication and transparency: lessons from the dengue vaccine controversy in the Philippines.","ParentTitle":"Journal of public health policy","ShortTitle":"Dayrit (2020)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2020","Month":"September","StandardNumber":"0197-5897 (Linking)","City":"England","Country":"","Publisher":"","Institution":"","Volume":"41","Pages":"252-267","Edition":"","Issue":"3","Availability":"","URL":"","OldItemId":"32518285","Comments":"","TypeName":"Journal, Article","Authors":"Dayrit MM ; Mendoza RU ; Valenzuela SA ; ","ParentAuthors":"","DOI":"https://doi.org/10.1057/s41271-020-00232-3 ","Keywords":"*Communication\r\nDengue/prevention & control\r\nDengue Vaccines/*adverse effects\r\nHealth Personnel\r\nHumans\r\nImmunization Programs/*organization & administration/standards\r\nOrganizational Case Studies\r\nParents\r\nPhilippines\r\nPublic Health Administration/*standards\r\nCrisis of confidence\r\nDengue vaccine\r\nDengvaxia\r\nRisk communication\r\nVaccine controversy","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Dayrit MM, Mendoza RU, and Valenzuela SA (2020) The importance of effective risk communication and transparency: lessons from the dengue vaccine controversy in the Philippines.. Journal of public health policy 41(3), 252-267 DOI: https://doi.org/10.1057/s41271-020-00232-3 "},{"Codes":[{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310052,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310045,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310059,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65366312,"Title":"Is partnership the answer? Delivering the national immunisation programme in the new English health system: a mixed methods study.","ParentTitle":"BMC public health","ShortTitle":"Chantler (2019)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2019","Month":"January","StandardNumber":"1471-2458 (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"19","Pages":"83","Edition":"","Issue":"1","Availability":"","URL":"","OldItemId":"30654788","Comments":"","TypeName":"Journal, Article","Authors":"Chantler T ; Bell S ; Saliba V ; Heffernan C ; Raj T ; Ramsay M ; Mounier-Jack S ; ","ParentAuthors":"","DOI":"https://doi.org/10.1186/s12889-019-6400-6 ","Keywords":"*Cooperative Behavior\r\nCross-Sectional Studies\r\nEngland\r\nHumans\r\nImmunization Programs/*organization & administration\r\nNational Health Programs/*organization & administration\r\nProgram Evaluation\r\nQualitative Research\r\nSurveys and Questionnaires\r\nCollaboration\r\nHealth services\r\nHealth system reorganisation\r\nHealth systems\r\nImmunisation\r\nMixed methods\r\nPartnership\r\nPublic health\r\nQualitative research\r\nSurvey","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Chantler T, Bell S, Saliba V, Heffernan C, Raj T, Ramsay M, and Mounier-Jack S (2019) Is partnership the answer? Delivering the national immunisation programme in the new English health system: a mixed methods study.. BMC public health 19(1), 83 DOI: https://doi.org/10.1186/s12889-019-6400-6 "},{"Codes":[{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310052,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310059,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310045,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65366242,"Title":"\"It's a complex mesh\"- how large-scale health system reorganisation affected the delivery of the immunisation programme in England: a qualitative study.","ParentTitle":"BMC health services research","ShortTitle":"Chantler (2016)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2016","Month":"September","StandardNumber":"1472-6963 (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"16","Pages":"489","Edition":"","Issue":"","Availability":"","URL":"","OldItemId":"27633653","Comments":"","TypeName":"Journal, Article","Authors":"Chantler T ; Lwembe S ; Saliba V ; Raj T ; Mays N ; Ramsay M ; Mounier-Jack S ; ","ParentAuthors":"","DOI":"https://doi.org/10.1186/s12913-016-1711-0 ","Keywords":"Cooperative Behavior\r\nDelivery of Health Care/*organization & administration/standards\r\nEngland\r\nGovernment Programs\r\nHealth Care Reform/*organization & administration\r\nHumans\r\nImmunization Programs/*organization & administration/standards\r\nInterinstitutional Relations\r\nOrganizational Innovation\r\nProgram Evaluation\r\nPublic Health/standards\r\nQualitative Research\r\nQuality of Health Care\r\nState Medicine/*organization & administration/standards\r\n*Delivery of health services\r\n*Health reforms\r\n*Immunisation\r\n*Organisational change\r\n*Public health\r\n*Qualitative research","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Chantler T, Lwembe S, Saliba V, Raj T, Mays N, Ramsay M, and Mounier-Jack S (2016) \"It's a complex mesh\"- how large-scale health system reorganisation affected the delivery of the immunisation programme in England: a qualitative study.. BMC health services research 16, 489 DOI: https://doi.org/10.1186/s12913-016-1711-0 "},{"Codes":[{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310050,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310045,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65368068,"Title":"Influence of the 2014-2015 ebola outbreak on the vaccination of children in a rural district of Guinea","ParentTitle":"Public Health Action","ShortTitle":"Camara (2017)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2017","Month":"","StandardNumber":"2220-8372","City":"France","Country":"","Publisher":"International Union Against Tuberculosis and Lung Disease (The Union) (68 boulevard Saint Michel, Paris 75006, France)","Institution":"B.S. Camara, Commune of Ratoma, ConakryGuinea. E-mail: bienvenusalimcamara@gmail.com","Volume":"7","Pages":"161-167","Edition":"","Issue":"2","Availability":"","URL":"","OldItemId":"617383876","Comments":"","TypeName":"Journal, Article","Authors":"Camara Bienvenu Salim; Sidibe S ; Sandouno S D; Delamou A M; Diro E ; El Ayadi A ; Beavogui A H; Grovogui F M; Kolie D ; Takarinda K C; Okumura J ; Balde M D; Van Griensven J ; Zachariah R ; ","ParentAuthors":"","DOI":"https://doi.org/10.5588/pha.16.0120","Keywords":"article\r\nchild\r\ndiphtheria\r\n*Ebola hemorrhagic fever/ep [Epidemiology]\r\nGuinea\r\nHaemophilus infection\r\nHaemophilus influenzae type b\r\nhealth care delivery\r\nhepatitis B\r\nhuman\r\nmeasles\r\nmulticenter study\r\nnonhuman\r\npertussis\r\n*rural population\r\nstock assessment\r\ntetanus\r\ntrend study\r\ntuberculosis\r\n*vaccination\r\nyellow fever\r\nBCG vaccine\r\nmeasles vaccine\r\noral poliomyelitis vaccine\r\npoliomyelitis vaccine\r\nRotavirus vaccine\r\nyellow fever vaccine","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Camara Bienvenu Salim, Sidibe S, Sandouno S D, Delamou A M, Diro E, El Ayadi A, Beavogui A H, Grovogui F M, Kolie D, Takarinda K C, Okumura J, Balde M D, Van Griensven J, and Zachariah R (2017) Influence of the 2014-2015 ebola outbreak on the vaccination of children in a rural district of Guinea. Public Health Action 7(2), 161-167 DOI: https://doi.org/10.5588/pha.16.0120"},{"Codes":[{"AttributeId":10310052,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":68454302,"Title":"Anatomy of a health scare: education, income and the MMR controversy in the UK.","ParentTitle":"Journal of health economics","ShortTitle":"Anderberg (2011)","DateCreated":"07/02/2022","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2011","Month":"May","StandardNumber":"0167-6296 (Linking)","City":"Netherlands","Country":"","Publisher":"","Institution":"","Volume":"30","Pages":"515-30","Edition":"","Issue":"3","Availability":"","URL":"","OldItemId":"21439663","Comments":"","TypeName":"Journal, Article","Authors":"Anderberg D ; Chevalier A ; Wadsworth J ; ","ParentAuthors":"","DOI":"https://doi.org/10.1016/j.jhealeco.2011.01.009 ","Keywords":"Adolescent\r\nAdult\r\nAutistic Disorder/etiology\r\nEducational Status\r\nFemale\r\nHealth Knowledge, Attitudes, Practice\r\nHealth Status Disparities\r\nHumans\r\nImmunization/*statistics & numerical data\r\nIncome/*statistics & numerical data\r\nInfant\r\nMale\r\nMeasles-Mumps-Rubella Vaccine/*administration & dosage/adverse effects\r\nMiddle Aged\r\nParent-Child Relations\r\nParents/education/*psychology\r\nPatient Acceptance of Health Care/psychology/*statistics & numerical data\r\n*Public Opinion\r\nUnited Kingdom\r\nYoung Adult","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Anderberg D, Chevalier A, and Wadsworth J (2011) Anatomy of a health scare: education, income and the MMR controversy in the UK.. Journal of health economics 30(3), 515-30 DOI: https://doi.org/10.1016/j.jhealeco.2011.01.009 "},{"Codes":[{"AttributeId":10310051,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310060,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310042,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":68454297,"Title":"Impact of the COVID-19 Pandemic on Routine Childhood Immunization in Saudi Arabia.","ParentTitle":"Vaccines","ShortTitle":"Alsuhaibani (2020)","DateCreated":"07/02/2022","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2020","Month":"October","StandardNumber":"2076-393X (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"8","Pages":"","Edition":"","Issue":"4","Availability":"","URL":"","OldItemId":"33022916","Comments":"","TypeName":"Journal, Article","Authors":"Alsuhaibani M ; Alaqeel A ; ","ParentAuthors":"","DOI":"https://doi.org/10.3390/vaccines8040581 ","Keywords":"COVID-19\r\nchildhood\r\nchildren\r\ncoronavirus\r\nimmunizations\r\nvaccinations\r\nvaccine-preventable disease, SARS-CoV-2","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Alsuhaibani M, and Alaqeel A (2020) Impact of the COVID-19 Pandemic on Routine Childhood Immunization in Saudi Arabia.. Vaccines 8(4),  DOI: https://doi.org/10.3390/vaccines8040581 "},{"Codes":[{"AttributeId":10310043,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310044,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310041,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310047,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310051,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310056,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]},{"AttributeId":10310067,"AdditionalText":"","ArmId":0,"ArmTitle":"","ItemAttributeFullTextDetails":[]}],"Outcomes":[],"ItemId":65366478,"Title":"Association of Exposure to Civil Conflict With Maternal Resilience and Maternal and Child Health and Health System Performance in Afghanistan.","ParentTitle":"JAMA network open","ShortTitle":"Akseer (2019)","DateCreated":"07/11/2021","CreatedBy":"Luke Baxter","DateEdited":"30/05/2022","EditedBy":"Luke Baxter","Year":"2019","Month":"November","StandardNumber":"2574-3805 (Linking)","City":"","Country":"","Publisher":"","Institution":"","Volume":"2","Pages":"e1914819","Edition":"","Issue":"11","Availability":"","URL":"","OldItemId":"31702799","Comments":"","TypeName":"Journal, Article","Authors":"Akseer N ; Rizvi A ; Bhatti Z ; Das JK ; Everett K ; Arur A ; Chopra M ; Bhutta ZA ; ","ParentAuthors":"","DOI":"https://doi.org/10.1001/jamanetworkopen.2019.14819 ","Keywords":"Adolescent\r\nAdult\r\nAfghanistan\r\nChild\r\nChild Health/standards/statistics & numerical data\r\nChild, Preschool\r\nCross-Sectional Studies\r\nDelivery of Health Care/*standards/statistics & numerical data\r\nExposure to Violence/*psychology/statistics & numerical data\r\nFemale\r\nHumans\r\nInfant\r\nMaternal Health/standards/statistics & numerical data\r\nMothers/*psychology/statistics & numerical data\r\n*Resilience, Psychological\r\nSurveys and Questionnaires\r\nWarfare/psychology/statistics & numerical data","ItemStatus":"I","ItemStatusTooltip":"Included in review","QuickCitation":"Akseer N, Rizvi A, Bhatti Z, Das JK, Everett K, Arur A, Chopra M, and Bhutta ZA (2019) Association of Exposure to Civil Conflict With Maternal Resilience and Maternal and Child Health and Health System Performance in Afghanistan.. JAMA network open 2(11), e1914819 DOI: https://doi.org/10.1001/jamanetworkopen.2019.14819 "}];
  const summaryAttribute = "Authors";
  const checkboxCheckedSvg = '<svg id="checked" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">' +
    '<path d="M0 0h24v24H0z" fill="none"/>' +
    '<path d="M19 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.11 0 2-.9 2-2V5c0-1.1-.89-2-2-2zm-9 14l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"/>' +
    '</svg>';
  const checkboxUncheckedSvg = '<svg id="unchecked" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">' +
    '<path d="M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"/>' +
    '<path d="M0 0h24v24H0z" fill="none"/>' +
    '</svg>';
  const checkboxIndeterminateSvg = '<svg id="indeterminate" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' +
    '<defs>' +
    '<path d="M0 0h24v24H0z" id="a"/>' +
    '</defs>' +
    '<clipPath id="b">' +
    '<use overflow="visible" xlink:href="#a"/>' +
    '</clipPath>' +
    '<path clip-path="url(#b)" d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-2 10H7v-2h10v2z"/>' +
    '</svg>';
  const radioCheckedSvg = '<svg id="checked" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">' +
    '<path d="M12 7c-2.76 0-5 2.24-5 5s2.24 5 5 5 5-2.24 5-5-2.24-5-5-5zm0-5C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/>' +
    '<path d="M0 0h24v24H0z" fill="none"/>' +
    '</svg>';
  const radioUncheckedSvg = '<svg id="unchecked" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">' +
    '<path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/>' +
    '<path d="M0 0h24v24H0z" fill="none"/>' +
    '</svg>';
  const arrowUpSvg = '<svg class="btnRowCollapse" id="arrowUp" fill="#000000" width="18" height="18" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">' +
    '<path d="M20.24 18.804c0 0.116-0.058 0.247-0.145 0.334l-0.725 0.725c-0.087 0.087-0.203 0.145-0.334 0.145-0.116 0-0.247-0.058-0.334-0.145l-5.702-5.702-5.702 5.702c-0.087 0.087-0.218 0.145-0.334 0.145s-0.247-0.058-0.334-0.145l-0.725-0.725c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l6.761-6.761c0.087-0.087 0.218-0.145 0.334-0.145s0.247 0.058 0.334 0.145l6.761 6.761c0.087 0.087 0.145 0.218 0.145 0.334zM20.24 13.232c0 0.116-0.058 0.247-0.145 0.334l-0.725 0.725c-0.087 0.087-0.203 0.145-0.334 0.145-0.116 0-0.247-0.058-0.334-0.145l-5.702-5.702-5.702 5.702c-0.087 0.087-0.218 0.145-0.334 0.145s-0.247-0.058-0.334-0.145l-0.725-0.725c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l6.761-6.761c0.087-0.087 0.218-0.145 0.334-0.145s0.247 0.058 0.334 0.145l6.761 6.761c0.087 0.087 0.145 0.218 0.145 0.334z"/>' +
    '<path d="M0 0h24v24H0z" fill="none"/>' +
    '</svg>';
  const arrowDownSvg = '<svg class="btnRowCollapse" id="arrowDown" fill="#000000" width="18" height="18" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">' +
    '<path d="M20.24 12.768c0 0.116-0.058 0.247-0.145 0.334l-6.761 6.761c-0.087 0.087-0.218 0.145-0.334 0.145s-0.247-0.058-0.334-0.145l-6.761-6.761c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l0.725-0.725c0.087-0.087 0.203-0.145 0.334-0.145 0.116 0 0.247 0.058 0.334 0.145l5.702 5.702 5.702-5.702c0.087-0.087 0.218-0.145 0.334-0.145s0.247 0.058 0.334 0.145l0.725 0.725c0.087 0.087 0.145 0.218 0.145 0.334zM20.24 7.196c0 0.116-0.058 0.247-0.145 0.334l-6.761 6.761c-0.087 0.087-0.218 0.145-0.334 0.145s-0.247-0.058-0.334-0.145l-6.761-6.761c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l0.725-0.725c0.087-0.087 0.203-0.145 0.334-0.145 0.116 0 0.247 0.058 0.334 0.145l5.702 5.702 5.702-5.702c0.087-0.087 0.218-0.145 0.334-0.145s0.247 0.058 0.334 0.145l0.725 0.725c0.087 0.087 0.145 0.218 0.145 0.334z"/>' +
    '<path fill="none" d="M0 0h24v24H0V0z"/>' +
    '</svg>';
  const arrowLeftSvg = '<svg class="btnColCollapse" id="arrowLeft" fill="#000000" width="18" height="18" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">' +
    '<path d="M14.436 19.036c0 0.116-0.058 0.247-0.145 0.334l-0.725 0.725c-0.087 0.087-0.218 0.145-0.334 0.145s-0.247-0.058-0.334-0.145l-6.761-6.761c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l6.761-6.761c0.087-0.087 0.218-0.145 0.334-0.145s0.247 0.058 0.334 0.145l0.725 0.725c0.087 0.087 0.145 0.218 0.145 0.334s-0.058 0.247-0.145 0.334l-5.702 5.702 5.702 5.702c0.087 0.087 0.145 0.218 0.145 0.334zM20.008 19.036c0 0.116-0.058 0.247-0.145 0.334l-0.725 0.725c-0.087 0.087-0.218 0.145-0.334 0.145s-0.247-0.058-0.334-0.145l-6.761-6.761c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l6.761-6.761c0.087-0.087 0.218-0.145 0.334-0.145s0.247 0.058 0.334 0.145l0.725 0.725c0.087 0.087 0.145 0.218 0.145 0.334s-0.058 0.247-0.145 0.334l-5.702 5.702 5.702 5.702c0.087 0.087 0.145 0.218 0.145 0.334z"/>' +
    '<path fill="none" d="M0 0h24v24H0V0z"/>' +
    '</svg>';
  const arrowRightSvg = '<svg class="btnColCollapse" id="arrowRight" fill="#000000" width="18" height="18" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">' +
    '<path d="M14.436 13c0 0.116-0.058 0.247-0.145 0.334l-6.761 6.761c-0.087 0.087-0.218 0.145-0.334 0.145s-0.247-0.058-0.334-0.145l-0.725-0.725c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l5.702-5.702-5.702-5.702c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l0.725-0.725c0.087-0.087 0.218-0.145 0.334-0.145s0.247 0.058 0.334 0.145l6.761 6.761c0.087 0.087 0.145 0.218 0.145 0.334zM20.008 13c0 0.116-0.058 0.247-0.145 0.334l-6.761 6.761c-0.087 0.087-0.218 0.145-0.334 0.145s-0.247-0.058-0.334-0.145l-0.725-0.725c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l5.702-5.702-5.702-5.702c-0.087-0.087-0.145-0.218-0.145-0.334s0.058-0.247 0.145-0.334l0.725-0.725c0.087-0.087 0.218-0.145 0.334-0.145s0.247 0.058 0.334 0.145l6.761 6.761c0.087 0.087 0.145 0.218 0.145 0.334z"/>' +
    '<path fill="none" d="M0 0h24v24H0V0z"/>' +
    '</svg>';
  const refreshSvg = '<svg class="btnRefresh" id="refresh" fill="#000000" width="18" height="18" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M17.65 6.35C16.2 4.9 14.21 4 12 4c-4.42 0-7.99 3.58-7.99 8s3.57 8 7.99 8c3.73 0 6.84-2.55 7.73-6h-2.08c-.82 2.33-3.04 4-5.65 4-3.31 0-6-2.69-6-6s2.69-6 6-6c1.66 0 3.14.69 4.22 1.78L13 11h7V4l-2.35 2.35z"/></svg>';
  const topHeaderColors = ["#00695C","#00796B","#00897B","#009688"];
  const topHeaderFontColor = "#FFFFFF";
  const sideHeaderColors = ["#00695C","#00796B","#00897B","#009688"];
  const sideHeaderFontColor = "#FFFFFF";
  const dataColor = "#0275d8";
  const collapseHeaders = false;
  const allowRISDownload = true;

  const $window = $(window);
  const $pivotTable = $('.pivot-table');
  const $pivotBody = $('.body');
  const $topHead = $('.top-head');
  const $topHeadTable = $('table', $topHead);
  const $topHeadWrapper = $('.top-head-wrapper');
  const $sideHead = $('.side-head');
  const $sideHeadTable = $('table', $sideHead);
  const $footer = $('.footer');
  const $menu = $('.menu');
  const $veil = $('.veil');
  const $reader = $('.reader');
  const $attributeTooltip = $('.attribute-tooltip');
  const $attributeTooltipContent = $('.attribute-tooltip .content')

  let $header = $('.header');
  let chartType = 'bubble';
  let filterMode = 'default';

  /**
   * @param {String} value
   */
  function safeParseInt(value) {
    const parsed = parseInt(value, 10);
    if (isNaN(parsed)) {
      return 0;
    }
    return parsed;
  }

  /**
   * Convert reference object from the eppi mapper to a RIS entry
   * @param referenceItem
   */
  function exportItemToRIS(it) {
    const calend = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    const newLine = '\r\n';
    let res = 'TY  - ';
    let tmp = '';
    switch (it.TypeId) {
      case 14:
        res += 'JOUR' + newLine;
        break;
      case 1:
        res += 'RPRT' + newLine;
        break;
      case 2:
        res += 'BOOK' + newLine;
        break;
      case 3:
        res += 'CHAP' + newLine;
        break;
      case 4:
        res += 'THES' + newLine;
        break;
      case 5:
        res += 'CONF' + newLine;
        break;
      case 6:
        res += 'ELEC' + newLine;
        break;
      case 7:
        res += 'ELEC' + newLine;
        break;
      case 8:
        res += 'ADVS' + newLine;
        break;
      case 10:
        res += 'MGZN' + newLine;
        break;
      default:
        res += 'GEN' + newLine;
        break;
    }
    res += 'T1  - ' + it.Title + newLine;
    if (it.TypeId == 10 || it.TypeId == 14)
      res += 'JF  - ' + it.ParentTitle + newLine;
    else res += 'T2  - ' + it.ParentTitle + newLine;
    for (let au of it.Authors.split(';')) {
      tmp = au.trim();
      if (tmp != '') res += 'A1  - ' + tmp + newLine;
    }
    for (let au of it.ParentAuthors.split(';')) {
      tmp = au.trim();
      if (tmp != '') res += 'A2  - ' + tmp + newLine;
    }
    res +=
      'KW  - eppi-reviewer4' +
      newLine +
      (it.Keywords != null && it.Keywords.length > 2
        ? it.Keywords.trim() + newLine
        : '');
    let tmpDate = '';
    let Month = safeParseInt(it.Month);

    if (!Month || Month < 1 || Month > 12) {
      Month =
        1 + it.Month.length > 2
          ? calend.indexOf(it.Month.substring(0, 3)) + 1
          : 0;
    }
    let yr = safeParseInt(it.Year);
    if (it.Year !== '' && yr) {
      if (yr > 0) {
        if (yr < 20) yr += 1900;
        else if (yr < 100) yr += 2000;
        if (yr.toString().length == 4) {
          res += 'py  - ' + yr.toString() + newLine;
          if (Month != 0) {
            tmpDate +=
              it.Year +
              '/' +
              (Month.toString().length == 1
                ? '0' + Month.toString()
                : Month.toString()) +
              '//';
          } else {
            tmpDate += it.Year + '///' + it.Month; //"y1  - "
          }
        }
      }
    }
    if (tmpDate.length > 0) {
      res += 'DA  - ' + tmpDate + newLine;
      res += 'Y1  - ' + tmpDate;

      //little trick: edition information is supposed to be the additional info at the end of the
      //Y1 filed. For Thesis pubtype (4) we use the edition field to hold the thesys type,
      //the following finishes up the Y1 field keeping all this into account

      if (it.TypeId == 4 && it.Edition.length > 0)
        res += newLine + 'KW  - ' + it.Edition + newLine;
      else if (it.Edition.length > 0) res += ' ' + it.Edition + newLine;
      else res += newLine;
    } else if (it.TypeId == 4 && it.Edition.length > 0) {
      res += newLine + 'KW  - ' + it.Edition + newLine;
    } //end of little trick

    res += 'AB  - ' + it.Abstract + newLine;
    if (it.DOI.length > 0) res += 'DO  - ' + it.DOI + newLine;
    res += 'VL  - ' + it.Volume + newLine;
    res += 'IS  - ' + it.Issue + newLine;
    let split = '-';
    Yr = it.Pages.indexOf(split);
    if (Yr > 0) {
      let pgs = it.Pages.split(split);
      res += 'SP  - ' + pgs[0] + newLine;
      res += 'EP  - ' + pgs[1] + newLine;
    } else if (it.Pages.length > 0) res += 'SP  - ' + it.Pages + newLine;
    res +=
      'CY  - ' +
      it.City +
      (it.Country.length > 0 ? ' ' + it.Country : '') +
      newLine;
    if (it.URL.length > 0) res += 'UR  - ' + it.URL + newLine;
    if (it.Availability.length > 0) res += 'AV  - ' + it.Availability + newLine;
    if (it.Publisher.length > 0) res += 'PB  - ' + it.Publisher + newLine;
    if (it.StandardNumber.length > 0)
      res += 'SN  - ' + it.StandardNumber + newLine;
    res += 'U1  - ' + it.ItemId.toString() + newLine;
    if (it.OldItemId.length > 0) res += 'U2  - ' + it.OldItemId + newLine;

    res += 'N1  - ' + it.Comments + newLine;

    res += 'ER  - ' + newLine + newLine;

    res = res.replace('     ', ' ');
    res = res.replace('    ', ' ');
    res = res.replace('   ', ' ');
    res = res.replace('   ', ' ');
    return res;
  }

  /**
   *
   * @param {Array} references - array of eppi reference objects
   */
  function handleRisDownloadButtonClick(references){
    const $downloadButton = $('#risDownload');
    // remove existing onclick handlers
    $downloadButton.unbind();
    $downloadButton.on('click', (e) => {
      const fileBlob = createRISFile(references);
      downloadRISFile(fileBlob);
    });
  }

  /**
   * Create a RIS file blcb from the given references.
   * @param selectedReferences - list of references to create a RIS file from
   * @returns {Blob}
   */
  function createRISFile(selectedReferences){
    const len = selectedReferences.length;
    let risData = '';
    for(let i = 0; i < len; i++){
      risData += exportItemToRIS(selectedReferences[i]);
    }
    return new Blob([risData], { type: 'text/plain' });

  }

  /**
   * Download the File blob of the references.
   * @param {Blob} file
  */
  function downloadRISFile(file){
    const blobUrl = URL.createObjectURL(file);
    // Save the need for file-save.js
    const link = document.createElement("a");
    link.href = blobUrl;
    link.download = `references.ris`;
    link.click();
  }

  /**
   * Gets the colour for the headings.
   * @param stepNumber
   * @returns {number}
   */
  function getColor(side, stepNumber) {
    // let base = 24;
    // let steps = 5;
    // let step = 8;
    //
    // // stops a step being greater than 5
    // stepNumber = stepNumber % steps;
    // return base + (step * stepNumber);
    stepNumber = stepNumber % 4;
    if (side)
      return sideHeaderColors[stepNumber];
    else
      return topHeaderColors[stepNumber];
  }

  /**
   * Builds the table used as the column headers of the pivot table.
   */
  function buildTableColHead() {
    let tableHeadHtml = '';
    let rowClass = '';
    let styles = [];

    for (let i = 0; i < csvData.totalColDepth; i++) {
      let cellClass = '';

      if (i > 0 && (i + 1) < csvData.totalColDepth) {
        rowClass = 'header-can-hide'
      } else if ((i + 1) === csvData.totalColDepth) {
        cellClass = 'clickable-col'
        rowClass = ''
      } else {
        rowClass = ''
      }

      const row = csvData.rows[i];
      tableHeadHtml += '<tr class="' + rowClass + '">';

      for (let j = 0; j < row.length; j++) {
        const col = row[j];
        const colSpan = i === 0 && j === 0 ? csvData.totalColBreadth : col.span;

        let style = '';

        if (i === 0) {
          style = 'style="background-color:' + getColor(false, 0) + ';color:' + topHeaderFontColor + ';"';
        }

        if (i === 1) {
          style = 'style="background-color:' + getColor(false, j) + ';color:' + topHeaderFontColor + ';"';

          for (let k = 0; k < colSpan; k++)
          {
            styles.push(style);
          }
        }

        if (i > 1) {
          style = styles[j];
        }

        tableHeadHtml += '<th class="level-' + i + ' ' + cellClass + '" colspan="' + colSpan + '"' + style + ' data-id="' + col.id + '">';
        tableHeadHtml += '<div>';

        if (i === 1) {
          tableHeadHtml += arrowLeftSvg;
          tableHeadHtml += arrowRightSvg;
          tableHeadHtml += refreshSvg;
        }

        tableHeadHtml += '<span>';
        tableHeadHtml += col.title;
        tableHeadHtml += '</span></div>';
        tableHeadHtml += '</th>'
      }

      tableHeadHtml += '</tr>';
    }

    $('table thead', $topHead).append(tableHeadHtml);
    topColHeight = $('table', $topHead).height();
    $('table', $topHead).height(topColHeight);
  }

  /**
   * Builds the table used as the row headers of the pivot table.
   */
  function buildTableRowHead() {
    let tableHeadHtml = '';
    let rowClass = '';
    let lightnessLevel = 0;
    let backgroundStyle = '';

    for (let i = csvData.totalColDepth; i < csvData.rows.length; i++) {
      const row = csvData.rows[i];
      tableHeadHtml += '<tr>';

      for (let j = 0; j < row.length; j++) {
        const col = row[j];
        const rowSpan = i === csvData.totalColDepth && j === 0 ? csvData.totalRowBreadth : col.span;
        const level = csvData.totalColDepth - row.length + j;

        if (level == 0) {
          backgroundStyle = 'background-color:' + getColor(true, 0) + ';color:' + sideHeaderFontColor + ';';
        }

        if (level === 1) {
          backgroundStyle = 'background-color:' + getColor(true, lightnessLevel) + ';color:' + sideHeaderFontColor + ';';
          lightnessLevel += 1;
        }

        if (level > 0 && (level + 1) < csvData.totalColDepth) {
          rowClass = 'header-can-hide'
        } else if ((level + 1) === csvData.totalColDepth) {
          rowClass = 'clickable-row'
        }

        tableHeadHtml += '<th class="level-' + level + ' ' + rowClass + '" rowspan="' + rowSpan + '" style="' + backgroundStyle + '" data-id="' + col.id + '">';
        tableHeadHtml += '<div>';

        if (level === 1) {
          tableHeadHtml += arrowUpSvg;
          tableHeadHtml += arrowDownSvg;
          tableHeadHtml += refreshSvg;
        }

        tableHeadHtml += '<span>';
        tableHeadHtml += col.title;
        tableHeadHtml += '</span></div>';
        tableHeadHtml += '</th>'
      }

      tableHeadHtml += '</tr>';
    }

    $('table tbody', $sideHead).append(tableHeadHtml);
    sideColWidth = $('table', $sideHead).width();
    $('table', $sideHead).width(sideColWidth);
  }

  /**
   * Creates the table columns of a row for the pivot table.
   */
  function buildTableColumns(cols, $colHeaders, rowIds) {
    let colIndex = 0;
    let tableColHtml = '';

    $colHeaders.each(function(colHeaderIndex, colHeader) {
      const $colHeader = $(colHeader);
      const colSpan = parseInt($colHeader.attr('colspan'));

      if ($colHeader.hasClass('collapsed')) {
        const colIds = [];

        for (let i = 0; i < colSpan; i++) {
          colIds.push(cols[colIndex].id);
          colIndex++;
        }

        tableColHtml += '<td class="cell" ' +
          'data-colid="' + colIds.join(',') + '" ' +
          'data-rowid="' + rowIds.join(',') + '">' +
          '</td>';
      } else {
        for (let i = 0; i < colSpan; i++) {
          tableColHtml += '<td class="cell" ' +
            'data-colid="' + cols[colIndex].id + '" ' +
            'data-rowid="' + rowIds.join(',') + '">' +
            '</td>';
          colIndex++;
        }
      }
    });

    return tableColHtml;
  }

  /**
   * Creates the table rows of the pivot table.
   */
  function buildTableRows() {
    let tableRowHtml = '';
    let rowIndex = csvData.totalColDepth;

    const cols = csvData.rows[csvData.totalColDepth - 1];
    const $colHeaders = $('.top-head .level-1');
    const $rowHeaders = $('.side-head .level-1');

    $rowHeaders.each(function(rowHeaderIndex, rowHeader) {
      const rowIds = [];
      const $rowHeader = $(rowHeader);
      const rowSpan = parseInt($rowHeader.attr('rowspan'));


      if ($rowHeader.hasClass('collapsed')) {
        tableRowHtml += '<tr>';
        for (let i = 0; i < rowSpan; i++) {
          const row = csvData.rows[rowIndex];
          rowIds.push(row[row.length - 1].id);
          rowIndex++;
        }
        tableRowHtml += buildTableColumns(cols, $colHeaders, rowIds);
        tableRowHtml += '</tr>';
      } else {
        tableRowHtml += '<tr>';
        for (let i = 0; i < rowSpan; i++) {
          const row = csvData.rows[rowIndex];
          tableRowHtml += buildTableColumns(cols, $colHeaders, [row[row.length - 1].id]);
          rowIndex++;
          tableRowHtml += '</tr>';
        }
      }
    });

    $('table tbody', $pivotBody).html(tableRowHtml);
  }

  /**
   * Gets references that match the given row and column lists.
   * @param rowIdList
   * @param colIdList
   * @returns {Array}
   * @constructor
   */
  function getFilteredReferences(rowIdList, colIdList) {
    const references = [];
    const refMatches = referenceData
      .filter((reference) => {
        if (!reference.hasOwnProperty('Codes')) return false;

        //#region This Code Does All The Reader Filtering
        if (rowIdList.length > 0 && colIdList.length > 0) { // if we have selection in both column and row....
          for (const rowId of rowIdList) {  // for each row in the list of rows
            for (const colId of colIdList) {  // get a column in the list of columns
              if (reference.Codes // of the references that have codes,, does this particular reference have at least 2 codes that match rowid and colid
                .filter((code) => code.AttributeId === rowId || code.AttributeId === colId).length >= 2){
                  return true;
                }
            }
          }
        } else {
          for (const code of reference.Codes) {
            if (rowIdList.length === 0) {  // No row intersections to check for.
              if (colIdList.indexOf(code.AttributeId) >= 0) return true;
            } else if (colIdList.length === 0) {  // No column intersections to check for.
              if (rowIdList.indexOf(code.AttributeId) >= 0) return true;
            }
          }
        }
        //#endregion

        return false;
      });

    for (const reference of refMatches) {
      //#region does filtering in the settings menu (affects map display)
      if (filterMode === 'default'){
        //#region Check all the filters. (Parent AND > Child OR)
        const parentFilters = filters.filter((item) => item.checked);
        let refMatchesFilter = parentFilters.length === 0;

        for (const parentFilter of parentFilters) {
          refMatchesFilter = false;

          for (const childFilter of parentFilter.children.filter((item) => item.checked)) {
            const childFilterMatch = reference.Codes.filter((code) => code.AttributeId === childFilter.id).length > 0;

            if (childFilterMatch) {
              refMatchesFilter = true;
              break; // exit this loop --> immediately goes into execution of the parentFilter loop; leaves childfilter loops
                      // result is we end up not checking each selected child item for a match,, resulting in an "OR" condition; "AND" condition would include all selected child items
            }
          }

          // if we do not match in the child filters, then we know it's not AND and we
          // can break.
          if (!refMatchesFilter) break;
        }

        // if we do not match all active parent filters then we do not count this.
        if (!refMatchesFilter) continue;

        references.push(reference);
        //#endregion
      } else if (filterMode === 'and') {
        //#region Check all the filters. (Parent AND == Child AND)
        const parentFilters = filters.filter((item) => item.checked);
        let refMatchesFilter = parentFilters.length === 0;

        for (const parentFilter of parentFilters) { // when checking each parent filter item,
          refMatchesFilter = false; // reset match to false
          const childFilters = parentFilter.children.filter((item) => item.checked);  // filter out child filter items

          for (const childFilter of childFilters) { // each child filter item
            refMatchesFilter = false; // reset match to false
            const childFilterMatch = reference.Codes.filter((code) => code.AttributeId === childFilter.id).length > 0;  // check if child filter item's code matches any reference code

            if (childFilterMatch) { // if child filter item matches at least 1 reference code,
              refMatchesFilter = true;  // set match to true
              // continue this loop --> we do not immediately go into execution of the parentFilter loop; stay in childfilter loops
              // result is we check each selected child item for a match, returning only references which do match, resulting in an "AND" condition
            } else {
              // refMatchesFilter = false;
              break;
            }
          }

          if (!refMatchesFilter) break;
        }

        if (!refMatchesFilter) continue;

        references.push(reference);
        //#endregion
      } else if (filterMode === 'or') {
        //#region Check all the filters. (Parent OR == Child OR)
        const parentFilters = filters.filter((item) => item.checked);
        let refMatchesFilter = parentFilters.length === 0;

        for (const parentFilter of parentFilters) {
          const childFilters = parentFilter.children.filter((item) => item.checked);

          for (const childFilter of childFilters) {
            const childFilterMatch = reference.Codes.filter((code) => code.AttributeId === childFilter.id).length > 0;

            if (childFilterMatch) {
              refMatchesFilter = true;
              break;
            }
          }

          if (!refMatchesFilter) continue;
        }

        if (!refMatchesFilter){
        } else {
          references.push(reference);
        }
        //#endregion
      }
      //#endregion
    }

    return references;
  }

  /**
   * Builds the legend.
   */
  function buildLegend() {
    if (segmentAttributes === null) return;

    let legendHtml = '';

    segmentAttributes.forEach((segment) => {
      legendHtml += '<span class="legend-item" data-description="' + segment.attribute.AttributeSetDescription + '">' +
          '<span class="dot" style="background-color: ' + segment.color + '"></span>' +
          '<span class="label">' + segment.attribute.AttributeName + '</span></span>'
    })

    $('.legend').html(legendHtml);

    $('.legend-item').hover(
        function () {  // hover on
          const text = $(this).data('description');

          if (text.length === 0) {
            return;
          }

          $(this).css('cursor', 'pointer');
          const filterTooltipHtml = '<div class="legend-tooltip">' + text + '</div>';
          $('body').append(filterTooltipHtml);
        },
        function () {  // hover off
          $('.legend-tooltip').remove();
        },
    ).mousemove((e) => {
      const $legendTooltip = $('.legend-tooltip');
      $legendTooltip
          .css({
            top: e.pageY - 24 - $legendTooltip.height(),
            left: e.pageX + 10
          });
    });;
  }

  /**
   * Creates a pie png image as a data url.
   */
  function createPiePng(counts) {
    let currentPoint = 0;
    let conicGrad = '';

    for (const count of counts) {
      currentPoint += count.width;
      if (conicGrad.length > 0) conicGrad += ', ';
      conicGrad += count.color + ' 0 ' + currentPoint + '%';
    }

    if (currentPoint < 100) {
      if (conicGrad.length > 0) conicGrad += ', ';
      conicGrad += '#eeeeee 0';
    }

    const gradient = new ConicGradient({
      stops: conicGrad,
      size: 97
    });

    const png = gradient.png;

    // Clean up memory issues once we have our PNG.
    $(gradient.canvas).empty().remove();
    delete gradient.canvas;

    return png;
  }

  /**
   * Create the mosiac svg
   *
   * @param {Number} dimension
   * @param {Array} references
   */
  function createMosaic(dimension, references) {
    let x = 0;
    let y = 0;

    let html = `<svg width="100" height="100" xmlns="http://www.w3.org/2000/svg">
   <defs>
   <pattern id="grid" width="${dimension}" height="${dimension}" patternUnits="userSpaceOnUse">
   <path d="M ${dimension} 0 L 0 0 0 ${dimension}" fill="none" stroke="white" stroke-width="0.5" />
   </pattern>
   </defs >
    <rect width="100%" height="100%" fill="url(#grid)" />`;

    for (const ref of references) {
      html += `<rect width="${dimension}" height="${dimension}" fill="${ref}" x="${x}" y="${y}" stroke="white" stroke-width="0.5" />`;
      x += dimension;

      if (x > 90) {
        x = 0;
        y += dimension
      }
    }

    html += '</svg >';


    return html;
  }

  /**
   * Creates the initial zero counts for the block.
   * @param rowIds
   * @param colIds
   */
  function createInitialCounts(rowIds, colIds) {
    let counts = [];

    if (segmentAttributes.length === 0) {
      counts.push({
        id: rowIds.join('_') + '-' + colIds.join('_'),
        attribute: null,
        color: dataColor,
        count: 0,
        width: 0,
        size: 0
      })
    } else {
      for (const segment of segmentAttributes) {
        counts.push({
          id: segment.attribute.AttributeId,
          attribute: segment.attribute,
          color: segment.color,
          count: 0,
          width: 0,
          size: 0
        })
      }
    }

    return counts
  }

  /**
   * Calculate the dimensions of the blocks that will go in the svg grid
   *
   * @param {Number} minRequiredBlocks minimum required mosiac tiles (equal to max number of references)
   * @param {Number} containerDimension dimensions of the container
   */
  function getBlockSize(minRequiredBlocks, containerDimension) {
    let number = Math.round(Math.sqrt(minRequiredBlocks));
    while (containerDimension % number != 0 && number < containerDimension) {
      number = number + 1;
    }
    return containerDimension / number;
  }

  /**
   * Updates all the reference counts and creates the visuals.
   */
  function updateReferenceCounts() {
    let maxCount = 0;

    // Remove old html and detach events.
    $('.cell', $pivotBody).off();
    $('.pie-wrapper').off().detach().remove();
    $('.data-wrapper').off().detach().remove();

    $('.cell', $pivotBody).each((index, cell) => {
      const $cell = $(cell);
      let colIds = $cell.data('colid')
      let rowIds = $cell.data('rowid');

      if (isNaN(colIds)) {
        colIds = colIds.split(',').map(p => parseInt(p));
      } else {
        colIds = [colIds]
      }

      if (isNaN(rowIds)) {
        rowIds = rowIds.split(',').map(p => parseInt(p));
      } else {
        rowIds = [rowIds]
      }

      const references = getFilteredReferences(rowIds, colIds);
      const counts = createInitialCounts(rowIds, colIds);

      let totalCount = 0;

      for (const reference of references) {
        for (const count of counts) {
          if (count.attribute === null) {
            totalCount += 1;
            count.count += 1;
            continue;
          }

          const countMatched = reference.Codes
            .filter((code) => code.AttributeId === count.id).length > 0

          if (countMatched) {
            count.count += 1;
            totalCount += 1;
          }
        }
      }

      if (totalCount === 0) {
        $cell.addClass('none');
      } else {
        $cell.removeClass('none');
      }

      if (totalCount > maxCount) {
        maxCount = totalCount
      }

      for (const count of counts) {
        count.width = totalCount === 0 || count.count === 0 ? 0 : Math.round(count.count / totalCount * 100);

        // Check if the count is a sane amount to see, if not, make it the minimum.
        if (count.width < 8 && count.count !== 0) count.width = 8
      }

      $cell.data('totalSize', 0);
      $cell.data('totalCount', totalCount);
      $cell.data('counts', counts);

      let piePngStyle = '';
      if (counts.length > 0 && chartType === 'donut') {
        const png = createPiePng(counts);

        if (png.length > 0) {
          piePngStyle = ' style="background-image: url(' + png + ')"';
        }
      }

      let dataWrapper = '<div class="pie-wrapper">' +
        '<div class="pie"' + piePngStyle + '></div>' +
        '<div class="pie-hole"></div>' +
        '</div>' +
        '<div class="mosaic-wrapper"></div>' +
        '<div class="data-wrapper">';

      for (const count of counts) {
        dataWrapper += '<div id="' + count.id + '" ' +
          'class="data" ' +
          'style="background-color: ' + count.color + ';"></div>';
      }

      dataWrapper += '</div>';

      $cell.html(dataWrapper);
    });

    // Update the size of the chart elements.
    $('.cell', $pivotBody).each((index, cell) => {
      const $cell = $(cell);
      const totalCount = $cell.data('totalCount');
      const counts = $cell.data('counts');

      for (const count of counts) {
        count.size = count.count === 0 ? 0 : Math.round(count.count / maxCount * 100);
        // Check if the count is a sane amount to see, if not, make it the minimum.
        if (count.size < 8 && count.count !== 0) count.size = 8
      }

      let totalSize = totalCount === 0 ? 0 : Math.round(totalCount / maxCount * 100);
      // Check if the count is a sane amount to see, if not, make it the minimum.
      if (totalSize < 8 && totalCount !== 0) totalSize = 8

      $cell.data('counts', counts);
      $cell.data('totalSize', totalSize);
      $cell.data('maxCount', maxCount);
    });

    // Setup events.
    setupTooltips();
    handleTableClick();
  }

  /**
   * Switches between the bubble and heat-map views.
   */
  function updateTableDataView() {
    $('.cell', $pivotBody).each((index, cell) => {
      const $cell = $(cell);
      const counts = $cell.data('counts');
      const totalSize = $cell.data('totalSize');
      const totalWidth = $cell.data('totalWidth');
      const maxCount = $cell.data('maxCount');
      const blockSize = 100;
      const halfBlockSize = blockSize / 2;

      if (chartType === 'donut') {
        const circleSize = (blockSize * totalSize / 100);
        let holeSize = totalSize - (circleSize / blockSize * 24);
        if (holeSize < 0) holeSize = 0;

        $('.pie-wrapper', $cell).css({
          'display': 'block'
        });

        $('.pie', $cell).css({
          'width': circleSize + '%',
          'height': circleSize + '%'
        });

        $('.pie-hole', $cell).css({
          'width': holeSize + '%',
          'height': holeSize + '%'
        });

        $('.data-wrapper', $cell).css({
          'display': 'none'
        });

        $('.mosaic-wrapper', $cell).css({
          'display': 'none'
        });

        return;
      } else {
        $('.pie-wrapper', $cell).css({
          'display': 'none'
        });

        $('.mosaic-wrapper', $cell).css({
          'display': 'none'
        });

        $('.data-wrapper', $cell).css({
          'display': 'block'
        });
      }

      $('.data-wrapper', $cell).css({
        'display': chartType === 'heat' ? 'flex' : 'block',
        'opacity': chartType === 'heat' ? (totalSize / 100) : 1
      });

      if (chartType === 'mosaic') {
        $('.pie-wrapper', $cell).css({
          'display': 'none'
        });

        $('.data-wrapper', $cell).css({
          'display': 'none'
        });

        $('.mosaic-wrapper', $cell).css({
          'display': 'block'
        });

        const dimension = getBlockSize(maxCount, 100);

        // Get colours from segments
        const referenceColours = counts.map(x => new Array(x.count).fill(x.color));
        const references = [].concat.apply([], referenceColours);
        let mosaic = createMosaic(dimension, references);
        $('.mosaic-wrapper', $cell).html(mosaic)
      }

      $('.data', $cell).css({
        'border-radius': chartType === 'heat' ? '0%' : '100%',
        'position': chartType === 'bubble' ? 'absolute' : 'relative'
      });

      for (const [index, count] of counts.entries()) {
        let size = blockSize * count.size / 100;
        // Check if the count is a sane amount to see, if not, make it the minimum.
        if (size < 8 && count.count !== 0) size = 8
        const halfSize = size / 2

        // calculate the middle.
        let transX = blockSize - halfBlockSize - halfSize;
        let transY = blockSize - halfBlockSize - halfSize;
        let diff = 0;

        if (size > halfBlockSize) {
          diff = size - halfBlockSize;
        }

        // move count to it's quadrant.
        if (count.attribute !== null) {
          if (index === 0) {  // move top left
            transX -= halfSize - diff;
            transY -= halfSize - diff;
          } else if (index === 1) {  // move top right
            transX += halfSize - diff;
            transY -= halfSize - diff;
          } else if (index === 2) {  // move bottom left
            transX -= halfSize - diff;
            transY += halfSize - diff;
          } else if (index === 3) {  // move bottom right
            transX += halfSize - diff;
            transY += halfSize - diff;
          }
        }

        $('#' + count.id, $cell).css({
          'height': chartType === 'bubble' ? size + '%' : '100%',
          'width': chartType === 'bubble' ? size + '%' : count.width + '%',
          'transform': chartType === 'bubble' ? 'translate(' + transX + 'px, ' + transY + 'px)' : 'translate(0, 0)'
        });
      }

      setTimeout(function () {
        $('.data-wrapper', $cell).css({
          'opacity': chartType === 'heat' ? totalWidth / 100 : 1
        });
      }, 500);
    });
  }

  /**
   * Cross browser functionality to request full screen access.
   * https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API#Toggling_fullscreen_mode
   */
  function toggleFullScreen() {
    if (!document.fullscreenElement &&    // alternative standard method
      !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
      } else if (document.documentElement.msRequestFullscreen) {
        document.documentElement.msRequestFullscreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }
  }

  /**
   * Helper function to build the full table.
   */
  function buildTable() {
    buildTableColHead();
    buildTableRowHead();
    buildTableRows();

    // Update data in the table.
    updateReferenceCounts();
    updateTableDataView();
  }

  /**
   * Sets the filter state of the the given filter ID by either adding it or
   * removing it from a list of unchecked filter IDs.
   * @param checkFilters
   * @param filterId
   * @param klass
   */
  function setFilterState(checkFilters, filterId, klass) {
    for (const filter of checkFilters) {
      if (filterId === filter.id) {
        filter.checked = klass === 'checked' || klass === 'indeterminate';
      }

      if (filter.children.length > 0) {
        setFilterState(filter.children, filterId, klass);
      }
    }
  }

  /**
   * Updates the filter's parent node's state.
   * @param $target
   */
  function updateFilterParentNodeCheckedState($target) {
    let hasChecked = false;
    let hasUnchecked = false;
    let hasIndeterminate = false;

    $target.next().children().each(function () {
      const klass = $(this).attr('class');

      if (klass === 'checked') {
        hasChecked = true;
      } else if (klass === 'unchecked') {
        hasUnchecked = true;
      } else {
        hasIndeterminate = true;
      }
    })

    let klass = '';

    if (hasChecked && !hasUnchecked && !hasIndeterminate) {
      klass = 'checked'
    } else if (!hasChecked && hasUnchecked && !hasIndeterminate) {
      klass = 'unchecked'
    } else if ((hasChecked && hasUnchecked && !hasIndeterminate) ||
      (hasChecked && !hasUnchecked && hasIndeterminate) ||
      (!hasChecked && hasUnchecked && hasIndeterminate) ||
      (hasChecked && hasUnchecked && hasIndeterminate)) {
      klass = 'indeterminate'
    }

    if (klass.length > 0) {
      const filterId = $target.data('id');
      setFilterState(filters, filterId, klass);

      $target
        .removeClass($target.attr('class'))
        .addClass(klass);
    }
  }

  /**
   * Updates the filter's node state and calls it's children r
   * recursively.
   * @param $target
   * @param newKlass
   */
  function updateFilterNodeCheckedState($target, newKlass = '') {
    const klass = $target.attr('class');
    $('#filterClearButton').attr('disabled',false);

    if (newKlass.length === 0) {
      if (klass === 'checked') {
        newKlass = 'unchecked';
      } else {
        newKlass = 'checked';
      }
    }

    const $next = $target.next();

    if ($next.is('ul')) {
      $next.children().each(function () {
        updateFilterNodeCheckedState($(this), newKlass);
      })
    }

    if ($target.is('li')) {
      const filterId = $target.data('id');
      setFilterState(filters, filterId, newKlass);

      $target
        .removeClass(klass)
        .addClass(newKlass);

      const $parent = $target.parent().prev();
      updateFilterParentNodeCheckedState($parent);
    }
  }

  /**
   * Update's the filter type and the checked state of the radios.
   * @param $filters
   * @param $target
   */
  function updateFilterMode($filters, $target) {
    const klass = $target.attr('class');
    if (klass === 'checked') return;

    filterMode = $target.data('id');

    $('.filter-type-wrapper li', $filters)
        .removeClass('checked')
        .addClass('unchecked');
    $target
        .removeClass('unchecked')
        .addClass('checked');
  }

  /**
   * Update's the chartType and the checked state of the radios.
   * @param $filters
   * @param $target
   */
  function updateViewStyle($filters, $target) {
    const klass = $target.attr('class');
    if (klass === 'checked') return;

    chartType = $target.data('id');

    $('.style-wrapper li', $filters)
      .removeClass('checked')
      .addClass('unchecked');
    $target
      .removeClass('unchecked')
      .addClass('checked');
  }

  /**
   * Creates a filter node recursively.
   * @param filters
   * @returns {string}
   */
  function createFilterNode(filters) {
    let filterHtml = '<ul>';

    for (const filter of filters) {
      filterHtml += '<li class="unchecked" data-id="' + filter.id + '">' +
        checkboxCheckedSvg +
        checkboxUncheckedSvg +
        checkboxIndeterminateSvg +
        ' <span>' + filter.label + '</span></li>';

      if (filter.children.length > 0) filterHtml += createFilterNode(filter.children);
    }

    filterHtml += '</ul>';
    return filterHtml;
  }

  /**
   * Creates the elements for the settings panel and hooks up
   * the events.
   */
  function createSettingsPanel() {
    const $filters = $('.settings');

    // Open filter if it should be auto open.
    if (autoOpenFilter) {
      $filters.addClass('open');
      $veil.addClass('open');
    }

    let settingsHtml = '<div class="title clearfix">' +
      '<span>Settings</span>' +
      '<a class="btnSettings right" id="close">close</a>' +
      '<a class="btnSettings left disabled" id="update">update</a>' +
      '</div>';
    settingsHtml += createFiltersPanel();
    settingsHtml += createStylesPanel();

    $filters.html(settingsHtml);

    $('.filter-type-wrapper li', $filters).on('click', (e) => {
      updateFilterMode($filters, $(e.currentTarget));
      $('.disabled').removeClass('disabled');
      $('#update', $filters).on('click', handleUpdate);
    });

    $('.filter-wrapper li', $filters).on('click', (e) => {
      updateFilterNodeCheckedState($(e.currentTarget));
      $('.disabled').removeClass('disabled');
      $('#update', $filters).on('click', handleUpdate);
    });

    $('.style-wrapper li', $filters).on('click', (e) => {
      updateViewStyle($filters, $(e.currentTarget));
      $('.disabled').removeClass('disabled');
      $('#update', $filters).on('click', handleUpdate);
    });

    $('.menu-settings').on('click', function () {
      $filters.addClass('open');
      $veil.addClass('open');
    });

    $('#filterClearButton').on('click', function () {
      const $filterButton = $('#filterClearButton');
      const $filteritems = $('.filter-wrapper ul > li');

      for (const item of $filteritems) {
        const $item = $(item);
        updateFilterNodeCheckedState($item, 'unchecked');
      }
      $filterButton.attr('disabled', true);
      $('.disabled').removeClass('disabled');
      $('#update', $filters).on('click', handleUpdate);
    })

    $('#close', $filters).on('click', function () {
      if (!$('#update', $filters).hasClass('disabled')) {
        if (!confirm('You have not updated the map with your changes. Are you sure you want to close?')) {
          return;
        }
      }

      $filters.removeClass('open');
      $veil.removeClass('open');
    });

    function handleUpdate() {
      const $update = $(this);
      $update.addClass('busy');

      setTimeout(() => {
        updateReferenceCounts();
        updateTableDataView();

        $filters.removeClass('open');
        $veil.removeClass('open');
        $update.removeClass('busy')
          .addClass('disabled');
        $update.off('click');
      }, 300);
    }
  }

  /**
   * Creates the filter section elements for the settings panel.
   * @returns {string}
   */
  function createFiltersPanel() {
    let filterHtml = `<div class="filter-type-wrapper">
        <h2>Filter mode</h2>
        <ul>
          <li class="${filterMode === 'default' ? 'checked': 'unchecked'}" data-id="default">
            ${radioCheckedSvg}
            ${radioUncheckedSvg}
            <span>Default <small>(OR within sections, AND across sections)</small></span>
          </li>
          <li class="${filterMode === 'and' ? 'checked': 'unchecked'}" data-id="and">
            ${radioCheckedSvg}
            ${radioUncheckedSvg}
            <span>And</span>
          </li>
          <li class="${filterMode === 'or' ? 'checked': 'unchecked'}" data-id="or">
            ${radioCheckedSvg}
            ${radioUncheckedSvg}
            <span>Or</span>
          </li>
        </ul>
      </div>
      <div class="filter-wrapper">
        <h2>Filters</h2>`;
      filterHtml +=
      '<button id="filterClearButton">Clear Filter</button>';
    filterHtml += createFilterNode(filters);
    filterHtml +=
    '</div>';

    return filterHtml;
  }

  /**
   * Creates the style section elements for the settings panel.
   * @returns {string}
   */
  function createStylesPanel() {
    let styleHtml = `
    <div class="style-wrapper">
    <h2>Style</h2>
      <ul>
      <li class="${chartType === 'bubble' ? 'checked': 'unchecked'}" data-id="bubble">
      ${radioCheckedSvg}
      ${radioUncheckedSvg}
      <span>Bubble-map</span>
      </li>
      <li class="${chartType === 'heat' ? 'checked': 'unchecked'}" data-id="heat">
      ${radioCheckedSvg}
      ${radioUncheckedSvg}
      <span>Heat-map</span>
      </li>
      <li class="${chartType === 'mosaic' ? 'checked': 'unchecked'}" data-id="mosaic">
      ${radioCheckedSvg}
      ${radioUncheckedSvg}
      <span>Mosaic</span>
      </li>`;

    if(segmentAttributes.length !== 0){
        styleHtml+=`
        <li class="${chartType === 'donut' ? 'checked': 'unchecked'}" data-id="donut">
        ${radioCheckedSvg}
        ${radioUncheckedSvg}
        <span>Donut-map</span>
        </li>`;
    }
    styleHtml += '</ul>';

    return styleHtml;
  }

  /**
   * Creates the tooltip for the cell as it's hovered over.
   */
  function setupTooltips() {
    $('.cell').hover((e) => {
      // Hover.
      const $target = $(e.currentTarget);
      const totalCount = $target.data('totalCount');

      if (totalCount <= 0) {
        return
      }

      const counts = $target.data('counts');

      let tooltipHtml = '<div class="tooltip">';

      for (const count of counts) {
        if (count.count === 0) continue;

        let title = 'Record';

        if (count.count > 1) {
          title = 'Records';
        }

        if (count.attribute !== null) {
          title = count.attribute.AttributeName
        }

        tooltipHtml += '<div class="count">' +
          '<span style="background-color: ' + count.color + ';">' + count.count + '</span> ' +
          title +
          '</div>';
      }

      tooltipHtml += '</div>';

      $(tooltipHtml)
        .appendTo('body')
        .fadeIn('fast');
    }, (e) => {
      // Hover out.
      $('.tooltip').remove();
      delete $('.tooltip');
    }).mousemove((e) => {
      $('.tooltip')
        .css({
          top: e.pageY + 10,
          left: e.pageX + 10
        });
    });
  }

  /**
   * Creates the meta item in the reader for the reference key
   * passed in.
   * @param key
   * @param reference
   * @returns {string}
   */
  function createMetaItem(key, reference) {
    let metaItem = '';
    const renameKeys = {
      'Journal': 'ParentTitle'
    }
    let tempKey = renameKeys[key];
    if (reference[tempKey] == undefined) {
      tempKey = key
    }

    if (tempKey !== summaryAttribute && reference[tempKey] !== undefined && reference[tempKey].length > 0) {

      let label = '';
      label = key

      metaItem += '<div class="meta-data-item clearfix ' + tempKey + '">';
      metaItem += '<label>' + label.replace(/([^A-Z])([A-Z])/g, '$1 $2') + '</label>';
      metaItem += '<span>' + reference[tempKey] + '</span>';
      metaItem += '</div>'
    }

    return metaItem;
  }

  /**
   * Creates the elements to read a review.
   * @param refId
   */
  function selectReference(refId) {
    const references = referenceData.filter((reference) => {
      return reference.ItemId === refId;
    });

    if (references.length === 0) {
      $('.read').html('');
      return;
    }

    const reference = references[0];
    let refHtml = '<h2>' + reference.Title + '</h2>';
    refHtml += '<hr>';

    if (reference.Abstract !== undefined && reference.Abstract.length > 0) {
      refHtml += '<p>' + reference.Abstract.replace(/\r/g, '<br>') + '</p>';
      refHtml += '<hr>';
    } else if (summaryAttribute.length > 0 && reference[summaryAttribute] !== undefined && reference[summaryAttribute].length > 0) {
      refHtml += '<p>' + reference[summaryAttribute].replace(/\r/g, '<br>') + '</p>';
      refHtml += '<hr>';
    }

    if (reference.URL !== undefined && reference.URL.length > 0) {
      refHtml += '<ol class="refs">';
      refHtml += '<li><a class="small" href="' + reference.URL + '" target="_blank">' + reference.URL + '</a></li>';

      if (reference.DOI === undefined || reference.DOI.length === 0) {
        refHtml += '</ol>';
        refHtml += '<hr>';
      }
    }

    if (reference.DOI !== undefined && reference.DOI.length > 0) {
      if (reference.URL === undefined || reference.URL.length === 0) {
        refHtml += '<ol class="refs">';
      }

      refHtml += '<li><a class="small" href="' + reference.DOI + '" target="_blank">' + reference.DOI + '</a></li>';
      refHtml += '</ol>';
      refHtml += '<hr>';
    }

    refHtml += '<div class="meta-data">';
    for (const key of metaProperties) {
      refHtml += createMetaItem(key, reference);
    }

    var matchedExturlAttributes = [];
    refHtml += "<div> <hr>";
    // checking for matches with external Attribute URLs to display in reader
    for (const attribute of externalURLedAttributes) {
      matchedExturlAttributes = reference.Codes.filter(code =>
        code.AttributeId === attribute.AttributeId);
        if (matchedExturlAttributes.length > 0 && attribute.ExtURL !== "" && attribute.ExtURL !== undefined) {
          refHtml +=
          '<span>' + attribute.AttributeName + '<br>' +
            '<a href="' + attribute.ExtURL + '" target="_blank">' + attribute.ExtURL + '</a>' +
          '</span><br>';
        }
    }
    refHtml +='</div>';

    refHtml += '</div>';

    $('.read').html(refHtml);
  }

  /**
   * Updates the reader to show the newly filtered/changed references.
   * @param references
   */
  function updateReferenceReader(references) {
    $('.refMenuItem').off();

    const readerOrder = $('#RefSortOrder').val();

    if (readerOrder === 'title') {
      references.sort(function (a, b) {
        if (a.Title > b.Title) return 1;
        else if (a.Title < b.Title) return -1;
        else return 0;
      });
    } else if (readerOrder === 'author') {
      references.sort(function (a, b) {
        if (a.Authors > b.Authors) return 1;
        else if (a.Authors < b.Authors) return -1;
        else return 0;
      });
    } else if (readerOrder === 'date') {
      references.sort(function (a, b) {

        let aDateStr = '';
        let bDateStr = '';

        if (a.Month.length !== 0) {
          aDateStr += a.Month + ' 01, ';
        }
        aDateStr += a.Year;

        if (b.Month.length !== 0) {
          bDateStr += b.Month + ' 01, ';
        }
        bDateStr += b.Year;

        const aDate = Date.parse(aDateStr);
        const bDate = Date.parse(bDateStr);

        if (aDate > bDate) return 1;
        else if (aDate < bDate) return -1;
        else return 0;
      });
    }

    let title = '';

    if (references.length === 1) {
      title = references.length + ' Record'
    } else {
      title = references.length + ' Records'
    }

    $groupingSelect = $('.grouping-opts');

    let refsHtml = '';

    if ($groupingSelect.val() === 'none') {   // no grouping
      //#region ADDS ALL THE STUDIES INTO AN UNSEGMENTED LIST
      refsHtml = '<ul>';

      for (const ref of references) {
        let colors = [];

        for (const code of ref.Codes) {
          for (const segment of segmentAttributes) {
            if (code.AttributeId === segment.attribute.AttributeId) {
              colors.push(segment.color);
            }
          }
        }

        refsHtml += '<li class="refMenuItem" data-refid="' + ref.ItemId + '" segmented="no">' +
          '<div class="title">' + ref.Title + '</div>' +
          '<div class="auth">' + ref.Authors + '</div>';

        refsHtml += '<div class="date">';

        if (ref.Month.length !== 0) {
          refsHtml += ref.Month + ', ';
        }

        refsHtml += ref.Year;

        for (let color of colors) {
          refsHtml += '<span class="refMenuItemLegend" style="background-color: ' + color + '" />';
        }

        refsHtml += '</div>';
        refsHtml += '</li>';
      }

      refsHtml += '</ul>';
      // #endregion
    } else if ($groupingSelect.val() === 'segment') {  // group by segments
      //#region ADD STUDIES INTO INDIVUDUAL UNORDERED LISTS
      refsHtml = '<ul class="segmented">';

      for (const segment of segmentAttributes){
        refsHtml +=
        '<li style="border-left:2px solid ' + segment.color + ';">' +
          '<div class="segment-title" style="border-bottom: 3px solid ' + segment.color + ';border-left: 5px solid ' + segment.color + '">'
            + segment.attribute.AttributeName +
          '</div>' +
          '<ul>';
            for (const ref of references) {
              let colors = [];

              for (const code of ref.Codes) {
                for (const segment of segmentAttributes){
                  if (code.AttributeId === segment.attribute.AttributeId) {
                    colors.push(segment.color);
                  }
                }
              }
              for (const code of ref.Codes) {
                if (code.AttributeId === segment.attribute.AttributeId) {
                  refsHtml +=
                  '<li class="refMenuItem" data-refid="' + ref.ItemId + '" segmented="yes">' +
                    '<div class="title">' + ref.Title + '</div>' +
                    '<div class="auth">' + ref.Authors + '</div>';

                    refsHtml +=
                    '<div class="date">';
                    if (ref.Month.length !== 0) {
                      refsHtml += ref.Month + ', ';
                    }
                    refsHtml += ref.Year;

                    for (let color of colors) {
                      refsHtml +=
                      '<span class="refMenuItemLegend" style="background-color: ' + color + '" />';
                    }
                    refsHtml +=
                    '</div>';
                  refsHtml +=
                  '</li>';
                }
              }
            }
          refsHtml +=
          '</ul>' +
        '</li>'
      }
      refsHtml +=
      '</ul>'
      //#endregion
    }

    $('.title > span', $reader).html(title);
    $('.nav', $reader).html(refsHtml);

    $('.refMenuItem').on('click', (e) => {
      $('.refMenuItem').removeClass('selected');

      const $target = $(e.currentTarget);
      const refId = $target.data('refid');

      $target.addClass('selected');
      selectReference(refId);
    });

    if (references.length > 0) {
      $($('.refMenuItem')[0]).trigger('click');
    } else {
      selectReference(0);
    }

    handleRisDownloadButtonClick(references);
  }

  /**
   * Gets the reference code id's from the checked items in the reader
   * filter.
   * @returns {{rows: Array, cols: Array}}
   */
  function getReaderFilterSelection() {
    const colIdList = [];
    const rowIdList = [];

    $('.reader-filter .cols li').each((index, col) => {
      const $col = $(col);
      if ($col.attr('class') === 'checked') {
        colIdList.push($col.data('id'))
      }
    });

    $('.reader-filter .rows li').each((index, row) => {
      const $row = $(row);
      if ($row.attr('class') === 'checked') {
        rowIdList.push($row.data('id'))
      }
    });

    return {
      cols: colIdList,
      rows: rowIdList
    };
  }

  /**
   * Creates the reader selector HTML
   * @returns {string}
   */
  function buildReaderFilter(rowIdList, colIdList) {
    const readerTopFilter = csvData.rows[csvData.totalColDepth - 1]
    const readerSideFilter = []

    for (let i = csvData.totalColDepth; i < csvData.rows.length; i++) {
      const row = csvData.rows[i]
      readerSideFilter.push(row[row.length - 1])
    }

    let colParent = readerTopFilter.length === colIdList.length ? 'checked' : 'indeterminate';
    if (colIdList.length === 0) colParent = 'unchecked';

    let html = '<div class="reader-filter">' +
      '<button id="codeFilterClearButton" class="btn">Clear Filters</button>' +
      '<ul>' +
      '<li class="' + colParent + '" data-parent="true" title="' + csvData.rows[0][0].title + '">' +
      checkboxCheckedSvg +
      checkboxUncheckedSvg +
      checkboxIndeterminateSvg +
      ' <span>' + csvData.rows[0][0].title + '</span>' +
      '</li>' +
      '<ul class="cols">';

    for (const item of readerTopFilter)
    {
      const state = colIdList.indexOf(item.id) >= 0 ? 'checked' : 'unchecked';
      html += '<li class="' + state + '" data-id="' + item.id + '" data-parent="false" title="' + item.title + '">' +
        checkboxCheckedSvg +
        checkboxUncheckedSvg +
        checkboxIndeterminateSvg +
        ' <span>' + item.title + '</span>' +
        '</li>';
    }

    let rowParent = readerSideFilter.length === rowIdList.length ? 'checked' : 'indeterminate';
    if (rowIdList.length === 0) rowParent = 'unchecked';

    html += '</ul>';
    html += '<li class="' + rowParent + '" data-parent="true" title="' + csvData.rows[csvData.totalColDepth][0].title + '">' +
      checkboxCheckedSvg +
      checkboxUncheckedSvg +
      checkboxIndeterminateSvg +
      ' <span>' + csvData.rows[csvData.totalColDepth][0].title + '</span>' +
      '</li>';
    html += '<ul class="rows">';

    for (const item of readerSideFilter)
    {
      const state = rowIdList.indexOf(item.id) >= 0 ? 'checked' : 'unchecked';
      html += '<li class="' + state + '" data-id="' + item.id + '" data-parent="false" title="' + item.title + '">' +
        checkboxCheckedSvg +
        checkboxUncheckedSvg +
        checkboxIndeterminateSvg +
        ' <span>' + item.title + '</span>' +
        '</li>';
    }

    html += '</ul>';
    html += '</ul>';
    html += '</div>';
    return html
  }

  /**
   * Builds the reference reader.
   * @param selectedRowIds
   * @param selectedColIds
   */
  function buildReferenceReader(selectedRowIds, selectedColIds) {
    let risDownloadButton = '';

    if (allowRISDownload) {
      risDownloadButton = '<button id="risDownload" class="btn">Download Listed References</button>';
    }

    let readerHtml =
      '<div class="title clearfix">' +
        '<a class="close">X</a> <span></span>' +
        risDownloadButton +
        '<input type="text" placeholder="Filter" class="reader-filter">' +
        '<select class="filter-opts">' +
          '<option class="opt-all" value="1"> All </option>' +
          '<option class="opt-title" value="2"> Title </option>' +
          '<option class="opt-abstract" value="3"> Abstract </option>' +
          '<option class="opt-author" value="4"> Author </option>' +
        '</select>' +
      '</div>' +
      '<div class="content">' +
        buildReaderFilter(selectedRowIds, selectedColIds) +
        '<div class="navTainer">' +
          '<div class="navGroupSelect">' +
            '<label for="sgroup" style="width:70px;display:block;float:left;">Group by: </label>' +
            '<select id="sgroup" class="grouping-opts">' +
            '<option value="none">None</option>' +
            '<option value="segment">Segment</option>' +
            '</select>' +
          '</div>' +
          '<div class="ref-sort-order">' +
            '<label for="RefSortOrder" style="width:70px;display:block;float:left;">Sort by: </label>' +
            '<select id="RefSortOrder">' +
              '<option value="title">Title</option>' +
              '<option value="author">Author</option>' +
              '<option value="date">Date</option>' +
            '</select>' +
          '</div>' +
          '<div class="nav">' +
          '</div>' +
        '</div>' +
        '<div class="read"></div>' +
      '</div>';

    $reader.html(readerHtml);

    if (segmentAttributes.length <= 0) {
      $('.navGroupSelect').hide();
    }
  }

  /**
   * Performs the search on the reader.
   * @param event
   * @param references
   * @param $readerFilter
   * @param $filterSelect
   */
  function doReaderSearch(event, references, $readerFilter, $filterSelect) {
    if (event !== null && event.which === -1) return;

    const searchTerm = $readerFilter.val().toLowerCase().trim();
    const filterOption = $filterSelect.val();
    let searchReferences = [];

    if (searchTerm.length <= 0) {
      updateReferenceReader(references);
      return;
    }
    references.forEach((ref) => {
      if (filterOption == 1){   // search across All
        if (Object.keys(ref).indexOf('Abstract') > -1){
          let titleRefs = -1;
          let abstractRefs = -1;
          let authorRefs = -1;
          titleRefs = ref.Title.toLowerCase().indexOf(searchTerm);
          abstractRefs = ref.Abstract.toLowerCase().indexOf(searchTerm);
          authorRefs = ref.Authors.toLowerCase().indexOf(searchTerm);
          if (titleRefs !== -1 || abstractRefs !== -1 || authorRefs !== -1){
            searchReferences.push(ref);
          }
        }
      } else if (filterOption == 2){   // search by Title
        if (Object.keys(ref).indexOf('Title') > -1){
          if (ref.Title.toLowerCase().indexOf(searchTerm) !== -1){
            searchReferences.push(ref);
          }
        }
      } else if (filterOption == 3){   // search by Abstract
        if (Object.keys(ref).indexOf('Abstract') > -1){
          if (ref.Abstract.toLowerCase().indexOf(searchTerm) !== -1){
            searchReferences.push(ref);
          }
        }
      } else if (filterOption == 4){   // search by Author
        if (Object.keys(ref).indexOf('Authors') > -1){
          if (ref.Authors.toLowerCase().indexOf(searchTerm) !== -1){
            searchReferences.push(ref);
          }
        }
      }
    });

    // final line: always do the update
    updateReferenceReader(searchReferences);
  }

  /**
   * Shows the reader for the given row and column id lists.
   * @param rowIdList
   * @param colIdList
   */
  function toggleReader(rowIdList, colIdList) {
    const references = getFilteredReferences(rowIdList, colIdList);
    buildReferenceReader(rowIdList, colIdList);
    updateReferenceReader(references);
    const $readerFilter = $('.reader-filter');
    const $filterSelect = $('.filter-opts');

    $readerFilter.on('keydown', (e) => {
      doReaderSearch(e, references, $readerFilter, $filterSelect);
    });

    $filterSelect.on('change', (e) => {
      doReaderSearch(null, references, $readerFilter, $filterSelect);
    });

    $('.grouping-opts').on('change', (e) => {
      updateReferenceReader(references);
    });

    $('#RefSortOrder').on('change', (e) => {
      updateReferenceReader(references);
    });

    $('.reader-filter li').on('click', (e) => {
      const $target = $(e.currentTarget);
      const klass = $target.attr('class');
      const isParent = $target.data('parent');
      const newKlass = klass === 'checked' ? 'unchecked' : 'checked';
      $('#codeFilterClearButton').attr('disabled', false);

      $target
        .removeClass(klass)
        .addClass(newKlass);

      if (isParent) {
        $target.next().children().each((index, child) => {
          $(child)
            .removeClass('checked')
            .removeClass('unchecked')
            .addClass(newKlass);
        });
      } else {
        const parent = $target.parent().prev();
        $(parent)
          .removeClass('checked')
          .removeClass('unchecked')
          .addClass('indeterminate');
      }

      const newSelection = getReaderFilterSelection();
      const references = getFilteredReferences(newSelection.rows, newSelection.cols);

      updateReferenceReader(references);
    });

    $('#codeFilterClearButton').on('click', function() {
      const $filterItems = $('.reader-filter li');
      for (const item of $filterItems){
        const $item = $(item);
        if ($item.hasClass('unchecked')){
          // do nothing; already in target state
        } else if ($item.hasClass('checked')){
          $item.removeClass('checked');
          $item.addClass('unchecked');
        } else if ($item.hasClass('indeterminate')){
          $item.removeClass('indeterminate');
          $item.addClass('unchecked');
        }
      }

      $('#codeFilterClearButton').attr('disabled', true);
      const newSelection = getReaderFilterSelection();
      const references = getFilteredReferences(newSelection.rows, newSelection.cols);
      updateReferenceReader(references);
    })

    $('.close', $reader).on('click', () => {
      $('.refMenuItem').off();
      $('.reader-filter li').off();
      $reader.removeClass('open');
      $veil.removeClass('open');
    });

    $veil.addClass('open');
    $reader.addClass('open');
  }

  /**
   * Creates the external url popup.
   * @param dataAxis
   * @param headerId
   * @param pageX
   * @param pageY
   */
  function createExtUrlPopup(dataAxis, headerId, pageX, pageY) {
    const filteredExtUrlAttr = externalURLedAttributes.filter(attr =>
      attr.AttributeId === headerId &&
      attr.AttributeName !== null &&
      attr.AttributeName !== undefined &&
      attr.AttributeName !== '' &&
      ((attr.AttributeDescription !== null &&
        attr.AttributeDescription !== undefined &&
        attr.AttributeDescription !== '') ||
        (attr.ExtURL !== undefined &&
        attr.ExtURL !== null &&
        attr.ExtURL !== '' &&
        attr.ExtType !== undefined &&
        attr.ExtType !== null &&
        attr.ExtType !== '')));
    const popupHasAttributeData = filteredExtUrlAttr.length > 0;

    if (!popupHasAttributeData) return false;

    let popUpHtml = '<div class="overlay-text" data-header-id="' + headerId + '" data-axis="' + dataAxis + '">';

    for (const attribute of filteredExtUrlAttr) {
      popUpHtml += '<h4>' + attribute.AttributeName + '</h4>';

      if (attribute.AttributeDescription !== null &&
        attribute.AttributeDescription !== undefined &&
        attribute.AttributeDescription.length > 0) {
        popUpHtml += '<p>' + attribute.AttributeDescription + '</p>';
      }

      if (attribute.ExtURL !== undefined &&
        attribute.ExtURL !== null &&
        attribute.ExtURL !== '' &&
        attribute.ExtType !== undefined &&
        attribute.ExtType !== null &&
        attribute.ExtType !== '') {
        popUpHtml += '<a class="overlay-link" href="' + attribute.ExtURL + '" target="_blank">' + attribute.ExtType + '</a>';
      }
    }

    popUpHtml += '<p class="text-center text-muted">(click to view records)</p></div>'

    $attributeTooltipContent.html(popUpHtml);
    $attributeTooltip.addClass('show');

    $('.close-tooltip').on('click', (e) => {
      $attributeTooltip.removeClass('show');
    });

    $attributeTooltipContent.on('click', (e) => {
      $attributeTooltip.removeClass('show');

      const $target = $('.overlay-text');
      const headerId = parseInt($target.data('header-id'));
      const axis = $target.attr('data-axis');

      if (axis === 'col') {
        toggleReader([], [headerId]);
      } else if (axis === 'row') {
        toggleReader([headerId], []);
      }
    });

    // dynamically calculate and adjust the elements position to always draw it within the window's bounds
    const overlayHeight = $attributeTooltip.height();
    const overlayWidth = $attributeTooltip.width();
    const pageHeight = $window.height();
    const pageWidth = $window.width();

    let top = pageY;
    let left = pageX;

    if ((overlayWidth + left) > pageWidth) {
      left -= overlayWidth - 10;
    } else {
      left += 10;
    }

    if ((overlayHeight + top) > pageHeight) {
      top -= overlayHeight - 10;
    } else {
      top += 10;
    }

    $attributeTooltip.css({
      top: top,
      left: left
    });

    return true;
  }

  /**
   * Handles the table click to show the reader dialog.
   */
  function handleTableClick() {
    //#region Header Click shows popup
    // when clicking the row (i.e - side headers)
    $('.clickable-row').on('click', (e) => {
      const $target = $(e.currentTarget);
      const headerId = $target.data('id');
      const createdPopup = createExtUrlPopup('row', headerId, e.pageX, e.pageY);

      if (!createdPopup) {
        toggleReader([headerId], []);
      }
    });

    // when clicking the column (i.e - top headers)
    $('.clickable-col').on('click', (e) => {
      const $target = $(e.currentTarget);
      const headerId = $target.data('id');
      const createdPopup = createExtUrlPopup('col', headerId, e.pageX, e.pageY);

      if (!createdPopup){
        toggleReader([], [headerId]);
      }
    });
    //#endregion

    $('.cell').on('click', (e) => {
      const $target = $(e.currentTarget);
      const totalCount = $target.data('totalCount');

      if (totalCount === 0) {
        return;
      }

      let colIds = $target.data('colid');
      let rowIds = $target.data('rowid');

      if (isNaN(colIds)) {
        colIds = colIds.split(',').map(p => parseInt(p));
      } else {
        colIds = [colIds]
      }

      if (isNaN(rowIds)) {
        rowIds = rowIds.split(',').map(p => parseInt(p));
      } else {
        rowIds = [rowIds]
      }

      toggleReader(rowIds, colIds);
    });
  }

  /**
   *  handle the about button click to show the about map reader dialog
   */
  function handleAboutClick() {
    $('.menu-about').on('click', e => {
      let aboutHtml = '<div class="title clearfix">' +
        '<a class="close">X</a> <span>About This Map</span>' +
        '</div>' +
        '<div class="content">' +
        '<div class="read">' + aboutContent + '</div>' +
        '</div>';

      $reader.html(aboutHtml);

      $('.close', $reader).on('click', () => {
        $reader.removeClass('open');
        $veil.removeClass('open');
      });

      $veil.addClass('open');
      $reader.addClass('open');
    });
  }

  /**
   * Handles the study submission click to show the information on how to
   * submit a new study to the map.
   */
  function handleStudySubmissionClick() {
    $('.menu-studysubmit').on('click', e => {
      let studySubmitHtml =
      '<div class="title clearfix">' +
        '<a class="close">X</a> <span> Submit a Study </span>' +
      '</div>' +
      '<div class="content">' +
        '<div class="read">' + studySubmissionContent + '</div>' +
      '</div>';

      $reader.html(studySubmitHtml);

      $('.close', $reader).on('click',() => {
        $reader.removeClass('open');
        $veil.removeClass('open');
      });

      $veil.addClass('open');
      $reader.addClass('open');
    });
  }

  /**
   * Makes the headers hide/show.
   */
  function handleExpandClick() {
    const $menuExpand = $('.menu-expand');
    const $topTable = $('table', $topHead);
    const $sideTable = $('table', $sideHead);

    $menuExpand.on('click', e => {

      if ($topTable.find('.collapsed').length > 0 || $sideTable.find('.collapsed').length > 0) {
        alert('You cannot hide the headers when they are collapsed.');
        return;
      }

      $menuExpand.toggleClass('active');

      if ($menuExpand.hasClass('active')) {
        $topTable.height('auto');
        $sideTable.width('auto');
      } else {
        $topTable.height(topColHeight);
        $sideTable.width(sideColWidth);
      }

      $('.header, .header-can-hide').slideToggle(200, function () {
          adjustTable();
      });
    });
  }

  /**
   * Toggles fullscreen mode.
   */
  function handleFullscreenClick() {
    const $menuFullscreen = $('.menu-fullscreen');

    $menuFullscreen.on('click', e => {
      toggleFullScreen();
      $menuFullscreen.toggleClass('active');
    });
  }

  /**
   * Toggles fullscreen mode.
   */
  function handleReaderClick() {
    const $menuReader = $('.menu-reader');

    const colIdList = csvData.rows[csvData.totalColDepth - 1].map(item => item.id)
    const rowIdList = []

    for (let i = csvData.totalColDepth; i < csvData.rows.length; i++) {
      const row = csvData.rows[i]
      rowIdList.push(row[row.length - 1].id)
    }

    $menuReader.on('click', e => {
      toggleReader(rowIdList, colIdList);
      $menuReader.toggleClass('active');
    });
  }

  /**
   * Toggles the column collapse.
   * @param miss
   * @param count
   */
  function toggleTopColCollapse(miss, count) {
    const max = miss + count;

    $('.top-head tr').each((rowIndex, row) => {
      if (rowIndex <= 1) return;
      let total = 0;

      $(row).children().each((colIndex, cell) => {
        const $cell = $(cell);
        const colSpan = parseInt($cell.attr('colspan'));
        total += colSpan;

        if (total > miss && total <= max) {
          if (total - 1 === miss) {
            $cell.toggleClass('first');
          }

          if (total - 1 === max) {
            $cell.toggleClass('last');
          }

          $cell.toggleClass('collapsed');
        } else if (total > max) {
          return false;
        }
      });
    });

    buildTableRows();
    adjustTable();

    $pivotBody.trigger('scroll');

    // Update data in the table.
    updateReferenceCounts();
    updateTableDataView();
  }

  /**
   * Toggles the row collapse.
   * @param miss
   * @param count
   */
  function toggleSideColCollapse(miss, count) {
    const max = miss + count;
    let total = 0;

    $('.side-head .level-2').each((index, cell) => {
      const $cell = $(cell);
      const rowSpan = parseInt($cell.attr('rowspan'));
      total += rowSpan;

      if (total > miss && total <= max) {
        if (total - 1 === miss) {
          $cell.toggleClass('first');
        }

        if (total - 1 === max) {
          $cell.toggleClass('last');
        }

        $cell.toggleClass('collapsed')
      } else if (total > max) {
        return false;
      }
    });

    total = 0;

    $('.side-head .level-3').each((index, cell) => {
      const $cell = $(cell);
      const rowSpan = parseInt($cell.attr('rowspan'));
      total += rowSpan;

      if (total > miss && total <= max) {
        if (total - 1 === miss) {
          $cell.toggleClass('first');
        }

        if (total - 1 === max) {
          $cell.toggleClass('last');
        }

        $cell.toggleClass('collapsed')
      } else if (total > max) {
        return false;
      }
    });

    buildTableRows();
    adjustTable();

    $pivotBody.trigger('scroll');

    // Update data in the table.
    updateReferenceCounts();
    updateTableDataView();
  }

  /**
   * Collapse all the rows and columns in the map.
   */
  function collapseAll(){
    const $colHeaders =  $('.level-1')
    .filter(function() {
      return this.colSpan > 1;
    });
    $colHeaders.each((index, cell) => {
      const $cell = $(cell);
      $cell.toggleClass('collapsed');
      const count = parseInt($cell.attr('colspan'));
      let miss = 0;

      let $prev = $cell.prev();

      while ($prev.length > 0) {
        try {
          miss += parseInt($prev.attr('colspan'))
          $prev = $prev.prev();
        }
        catch (e) {
          break;
        }
      }

      if (count > 0) {
          toggleTopColCollapse(miss, count);
      }
    });

    const $rowHeaders =  $('.level-1').filter(function() {
      return this.rowSpan > 1;
    });

    $rowHeaders.each((index, cell) => {
      const $cell = $(cell);
      $cell.toggleClass('collapsed');

      const count = parseInt($cell.attr('rowspan'));
      let miss = 0;
      $('.side-head .level-1').each((index, otherCell) => {
        const $otherCell = $(otherCell);

        if ($otherCell.text() !== $cell.text()) {
          miss += parseInt($otherCell.attr('rowspan'));
        } else {
           return false;
        }
      });

      if (count > 0) {
          toggleSideColCollapse(miss, count);
      }
    });
  }

  /**
   * Toggles collapsing columns and rows.
   */
  function handleTableRowColCollapse() {
    // Manage column collapse.
    $('.btnColCollapse').on('click', e => {
      const $target = $(e.currentTarget);
      const $cell = $target.parent().parent();
      $cell.toggleClass('collapsed');
      $cell.toggleClass('busy');

      const count = parseInt($cell.attr('colspan'));
      let miss = 0;

      let $prev = $cell.prev();

      while ($prev.length > 0) {
        try {
          miss += parseInt($prev.attr('colspan'))
          $prev = $prev.prev();
        }
        catch (e) {
          break;
        }
      }

      if (count > 0) {
        setTimeout(() => {
          toggleTopColCollapse(miss, count);
          $cell.toggleClass('busy');
        }, 50);
      } else {
        $cell.toggleClass('busy');
      }
    });

    // Manage row collapse.
    $('.btnRowCollapse').on('click', e => {
      const $target = $(e.currentTarget);
      const $cell = $target.parent().parent();
      $cell.toggleClass('collapsed');
      $cell.toggleClass('busy');

      const count = parseInt($cell.attr('rowspan'));
      let miss = 0;

      $('.side-head .level-1').each((index, cell) => {
        const $otherCell = $(cell);

        if ($otherCell.text() !== $cell.text()) {
          miss += parseInt($otherCell.attr('rowspan'));
        } else {
           return false;
        }
      });

      if (count > 0) {
        setTimeout(() => {
          toggleSideColCollapse(miss, count);
          $cell.toggleClass('busy');
        }, 50);
      } else {
        $cell.toggleClass('busy');
      }
    });
  }

  /**
   * Adjusts the table as the window is resized.
   */
  function adjustTable() {
    const bodyWidth = $pivotTable.width() - $sideHead.width();
    const topHeadWrapperCssHeight = $topHead.height();
    const topHeadWrapperCssPaddingLeft = $sideHead.width();
    const topHeadCssWidth = bodyWidth;
    const bodyCssWidth = bodyWidth - 1;
    let sideHeadCssHeight = $window.height() - $topHead.height() - $footer.height() - $menu.height() - 48;
    let bodyCssHeight = sideHeadCssHeight;

    $header = $('.header')

    if ($header.length > 0 && $header.css('display') !== 'none') {
      sideHeadCssHeight = sideHeadCssHeight - $header.height() - 8;
      bodyCssHeight = sideHeadCssHeight;
    }

    $topHeadWrapper.css({
      'height': topHeadWrapperCssHeight,
      'padding-left': topHeadWrapperCssPaddingLeft
    });

    $topHead.css({
      'width': topHeadCssWidth
    });

    $topHeadTable.css({
      'margin-left': '0px'
    });

    $sideHead.css({
      'height': sideHeadCssHeight
    });

    $sideHeadTable.css({
      'margin-top': '0px'
    });

    $pivotBody.css({
      overflow: 'scroll',
      width: bodyCssWidth,
      height: bodyCssHeight
    });

    $pivotBody.scroll(function (e) {
      $topHeadTable.css({
        'margin-left': e.target.scrollLeft * -1
      });

      $sideHeadTable.css({
        'margin-top': e.target.scrollTop * -1
      });
    });
  }

  // Call all the methods to initialize the page.
  createSettingsPanel();
  buildTable();
  adjustTable();
  buildLegend();
  handleExpandClick();
  handleFullscreenClick();
  handleReaderClick();
  handleTableRowColCollapse();

  if(collapseHeaders){
    collapseAll();
  }

  if (aboutContent.trim().length === 0) {
    $('.menu-about').hide();
  }
  else {
    handleAboutClick();
  }

  if (studySubmissionContent.trim().length === 0) {
    $('.menu-studysubmit').hide();
  } else {
    handleStudySubmissionClick();
  }

  // Show the loader.
  const $loader = $('.loader');
  const windowHeight = $window.height() + 200;

  $window.resize(adjustTable);

  // Slide the up after all is done.
  $loader
    .css({
      'top': -windowHeight,
      'bottom': windowHeight
    });

  // Removes the loader after the animation is complete.
  setTimeout(() => {
    $loader.remove();
  }, 2000);
});

  </script>
</body>
</html>
