# Example GitLab Pages/Jekyll Template

This is a template for generating a basic GitLab pages site. You can find the documentation for the Jekyll static site generator at <https://jekyllrb.com>.

## Usage

To use this template, fork a copy into your own namespace and edit the forked project as follows.

### Project name

Update the project name of your fork to something meaningful for your intended page. Go to Settings > General > Project name. Your project name should contain no white spaces.

Change the path of your project to match your project name. Go to Settings > General > Advanced > Change path.

### _config.yml

This file provides several settings for your site:

    * title: This is the title for your site
    * email: A contact email address for the page author/site operator
    * description: A description of the content of the site - if you maintain the '>' at the start of the description then newlines in your text will be ignored
    * baseurl: This is used to generate links to images etc, this should be set to /pages/**group or username**/**project name**
    * url: Don't change this
    * version: If you wish to indicate that this is a new version of this site, increment this number

Note that the "project name" in the baseurl must match the name and path defined in the previous step.

### _sass/site.sass

This file defines the style of the website and is written in the SASS language (<https://sass-lang.com/guide>) a simplified, more powerful version of CSS. Do not change the .banner and .small_hidden sections as these control the header layout. When your site is built this file is converted in the CSS by Jekyll.

### Gemfile

If you wish to install additional Jekyll add-ons ('gems') then these should be added to this file as per the existing lines.

### index.md

This is the main page file. This will be processed as Markdown to generate the index.html file for your site. Unless you have a specific need this file need not be edited.

### main.md

This is a pure Markdown file that is included into index.md to generate your front page. For single page sites this and _config.yml would be all you need to edit.

### assets/images

Use this folder to hold any images for your site. They can then be referenced in your Markdown file with the following src:

    "{{ "/assets/images/imagename.png" | prepend: site.baseurl }}"

Remember to include an **alt** textual description for all images.

### _layouts/default.md

This file defines the basic structure of the page, incorporating the header, logo and footer section files. The content from your page file will be incorporated into this in place of the **{{ content }}**.

### Additional pages

To create an additional page, create a new .md file including at the start the following:

    ---
    layout: default
    sitemap: true
    ---

This will indicate to Jekyll that a .html file should be generated and apply the defualt layout template.

## Building

If you don't have your own GitLab runner configured then you can use our GitLab runner to build the site by visiting your forked projects Settings (cog) > CI/CD section, expanding the _*Runners*_ section and clicking the _*Enable Shared Runners_* button.

Once a runner is defined then subsequent pushes to this repository will trigger a build process.

If the job fails, then look in the _CI/CD / Jobs_ section (rocket symbol) - clickon the failed job button to see the output from the build process and act on the error given.

## Viewing your site

Assuming that the build of the site completes without error your site will now be visable at:

    https://open.win.ox.ac.uk/pages/<yournamespace>/<yourproject>
